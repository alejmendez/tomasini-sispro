<?php

namespace App\Modules\Empresa\Http\Controllers;

//Controlador Padre
use App\Modules\Empresa\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Empresa\Http\Requests\EmpresaCargosRequest;

//Modelos
use App\Modules\Empresa\Models\EmpresaCargos;

class EmpresaCargosController extends Controller
{
    protected $titulo = 'Empresa Cargos';

    public $js = [
        'EmpresaCargos'
    ];
    
    public $css = [
        'EmpresaCargos'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('empresa::EmpresaCargos', [
            'EmpresaCargos' => new EmpresaCargos()
        ]);
    }

    public function nuevo()
    {
        $EmpresaCargos = new EmpresaCargos();
        return $this->view('empresa::EmpresaCargos', [
            'layouts' => 'base::layouts.popup',
            'EmpresaCargos' => $EmpresaCargos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $EmpresaCargos = EmpresaCargos::find($id);
        return $this->view('empresa::EmpresaCargos', [
            'layouts' => 'base::layouts.popup',
            'EmpresaCargos' => $EmpresaCargos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $EmpresaCargos = EmpresaCargos::withTrashed()->find($id);
        } else {
            $EmpresaCargos = EmpresaCargos::find($id);
        }

        if ($EmpresaCargos) {
            return array_merge($EmpresaCargos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EmpresaCargosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $EmpresaCargos = $id == 0 ? new EmpresaCargos() : EmpresaCargos::find($id);

            $EmpresaCargos->fill($request->all());
            $EmpresaCargos->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $EmpresaCargos->id,
            'texto' => $EmpresaCargos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            EmpresaCargos::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            EmpresaCargos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            EmpresaCargos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = EmpresaCargos::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}