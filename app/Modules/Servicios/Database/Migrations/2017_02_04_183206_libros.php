<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Libros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        MÓDULO DE LIBROS LEGALES
            CONTROL DE ENTREGA LIBROS LEGALES
                Fecha de Recibido al Dpto. de Administración
                Fecha de Entrega al Personal Externo
                Fecha de Entrega de la Comunicación al Cliente
        */
        Schema::create('servicios_libros', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->date('entrega_admon')->nullable()->comment('Fecha de Entrega al Dpto. de Administración');
            $table->date('entrega')->nullable()->comment('Fecha de Entrega al Personal Externo');
            $table->date('entrega_comunicacion')->nullable()->comment('Fecha de Entrega de la Comunicación al Cliente');

            $table->string('comentario', 200)->nullable()->comment('Comentario');
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_libros');
    }
}
