<?php

namespace App\Modules\Empresa\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Modules\Empresa\Models\EmpresaEstructura;

class EstructuraSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$estructura = EmpresaEstructura::create([
			'nombre' => 'Presidencia',
			'empresa_estructura_id' => 0
		]);

		EmpresaEstructura::create([
			'nombre' => 'Administración',
			'empresa_estructura_id' => $estructura->id
		]);

		EmpresaEstructura::create([
			'nombre' => 'Servicios',
			'empresa_estructura_id' => $estructura->id
		]);

		EmpresaEstructura::create([
			'nombre' => 'Operaciones',
			'empresa_estructura_id' => $estructura->id
		]);

		EmpresaEstructura::create([
			'nombre' => 'Talento Humano',
			'empresa_estructura_id' => $estructura->id
		]);

		EmpresaEstructura::create([
			'nombre' => 'Informatica',
			'empresa_estructura_id' => $estructura->id
		]);
	}
}
