<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function(Blueprint $table){
            $table->increments('id');
            
            $table->string('nombre', 100);
            $table->string('slug', 100);
            $table->string('direccion', 200);

            $table->integer('clientes_categorias_id')->unsigned()->nullable();
            $table->integer('clientes_rutas_id')->unsigned()->nullable();
            $table->integer('clientes_condicion_contribuyente_id')->unsigned()->nullable();
            $table->integer('clientes_razon_social_id')->unsigned()->nullable();
            $table->integer('clientes_condicion_id')->unsigned()->nullable();
            
            $table->integer('supervisor')->unsigned()->nullable();
            $table->integer('jefe_departamento')->unsigned()->nullable();
            $table->integer('asistente')->unsigned()->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_categorias_id')->references('id')->on('clientes_categorias')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('clientes_rutas_id')->references('id')->on('clientes_rutas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('clientes_condicion_contribuyente_id')->references('id')->on('clientes_condicion_contribuyente')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('clientes_razon_social_id')->references('id')->on('clientes_razon_social')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('clientes_condicion_id')->references('id')->on('clientes_condicion')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('supervisor')->references('id')->on('empresa_empleados')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('jefe_departamento')->references('id')->on('empresa_empleados')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('asistente')->references('id')->on('empresa_empleados')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
