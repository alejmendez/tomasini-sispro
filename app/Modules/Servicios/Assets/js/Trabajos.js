var aplicacion, tabla, busqueda = [0, 0, 0];

$(function() {
    aplicacion = new app('formulario', {
        'limpiar_inicio': false,
        'limpiar': function() {
            busqueda = [0, 0, 0];
            tabla.ajax.reload();

            $('.tools .expand', '.portlet-lista-clientes').trigger('click');

            $("#datosCliente, #recepcion_documentos").css('display', 'none');
            $("div.portlet, div.row-servicio-condicion, div.cont-fecha-operaciones", "#cont-servicios").css('display', 'none');
        }
    });

    tabla = $('#tabla').DataTable({
        ajax: {
            url: $url + "datatable",
            data: function(d) {
                d.mes = $('#mes').val();
                d.ano = $('#ano').val();
                d.entrego_documentos = $('#entrego_documentos').val();
                d.supervisor = $('#supervisor').val();
            }
        },
        columns: [
            { "data": "nombre", "name": "clientes.nombre" },
            { "data": "razon_social", "name": "clientes_condicion_contribuyente.nombre" }
        ]
    });

    agregarFiltrosDatatable('#tabla');

    $('.tools .reload', '.portlet-lista-clientes').click(function() {
        tabla.ajax.reload();
    });

    var date_op = {
        format: "dd/mm/yyyy",
        todayBtn: "linked",
        clearBtn: true,
        language: "es",
        calendarWeeks: true,
        endDate: new Date(),
    };

    if (usuario.perfil_id == 7 || usuario.perfil_id == 8) {
        date_op.startDate = new Date();
    }

    $('.input-group.date').datepicker(date_op);


    $("#ClientesHistorico_recibido_admon").change(function() {
        var val = $(this).val();

        if (val != '') {
            $("input", ".input-group.date-recibido_admon").each(function() {
                if ($(this).val() == '' && $(this).is(':visible')) {
                    $(this).val(val);
                }
            });
            $(".input-group.date-recibido_admon").trigger("changeDate");
        }
    });

    $("#ClientesHistorico_recibido_oper").change(function() {
        var val = $(this).val();

        if (val != '') {
            $("input", ".input-group.date-recibido_oper").each(function() {
                if ($(this).val() == '' && $(this).is(':visible')) {
                    $(this).val(val);
                }
            });
            $(".input-group.date-recibido_oper").trigger("changeDate");
        }
    });

    $(".input-group.date-recibido_admon").on("changeDate", function(e) {
        var t = $(this);
        change_recibido_admon($('input', e.currentTarget));
        $('.input-group.date-recibido_oper', t.parents('.row')).datepicker('setStartDate', e.date);
    });

    $(".input-group.date-recibido_oper").on("changeDate", function(e) {
        var t = $(this);
        change_recibido_oper($('input', e.currentTarget));
        $('.input-group.date-recibido_admon', t.parents('.row')).datepicker('setEndDate', e.date);
    });

    $('.monto_cancelado, .regular_monto_cancelado, .comp_monto_cancelado, .susti_monto_cancelado').maskMoney();

    $("#buscar").click(function() {
        $("#modalTablaBusqueda").modal('show');
        $("#tabla").DataTable().search($("#nombre_cliente").val()).draw();
    });

    $('#tabla').on("click", "tbody tr", function() {
        $("#cliente_id").val(this.id);
        buscar();
    });

    $("#mes").on('change', function() {
        $('#mes_h').val($(this).val());
        recarga_tabla();
        buscar();
    });

    $("#ano").on('change', function() {
        $('#ano_h').val($(this).val());
        recarga_tabla();
        buscar();
    });

    $("#entrego_documentos").on('change', function() {
        tabla.ajax.reload();
    });

    $("#supervisor").on('change', function() {
        tabla.ajax.reload();
    });

    $('.input-group-addon', '.date').click(function() {
        $(this).parent().find('input').focus();
    });


    $('.certificado').change(function() {
        var t = $(this),
            display = t.val() == '1' ? 'none' : 'block',
            elements = [
                '.numero_planilla',
                '.pago_planilla',
                //'.bancos_id',
                //'.monto_cancelado',

                '.regular_numero_planilla',
                '.regular_pago_planilla',
                //'.regular_bancos_id',
                //'.regular_monto_cancelado',

                '.comp_numero_planilla',
                '.comp_pago_planilla',
                //'.comp_bancos_id',
                //'.comp_monto_cancelado',

                '.susti_numero_planilla',
                '.susti_pago_planilla',
                //'.susti_bancos_id',
                //'.susti_monto_cancelado'
            ],
            padre = t.parent().parent().parent();

        for (var i in elements) {
            $(elements[i], padre).parents('.form-group').css('display', display);
        }
    });

    //tabla.search($("#nombre_cliente").val(), false, true).draw();
});

function recarga_tabla() {
    if ($('#entrego_documentos').val() != '' || $('#supervisor').val() != '') {
        tabla.ajax.reload();
    }
}

function change_recibido_admon(t) {
    var admon_val = t.val(),
        $portlet = t.parents('.portlet-body'),
        oper_val = $('.recibido_oper', $portlet).val(),
        display = '';

    display = admon_val == '' ? 'none' : 'block';
    $("div.cont-fecha-operaciones", $portlet).css('display', display);

    display = (admon_val == '' || oper_val == '') ? 'none' : 'block';
    $portlet.find('.row:eq(1)').css('display', display);
}

function change_recibido_oper(t) {
    var oper_val = t.val(),
        $portlet = t.parents('.portlet-body'),
        admon_val = $('.recibido_admon', $portlet).val(),
        display = (admon_val == '' || oper_val == '') ? 'none' : 'block';

    $portlet.find('.row:eq(1)').css('display', display);
}

function buscar_cliente() {
    var id = parseInt($("#cliente_id").val());

    $.get($url + 'buscar-cliente/' + id, function(r) {
        aviso(r);

        $("input", "#cont-servicios").val('');

        $("#cliente_id").val(r.id);
        $("#nombre_cliente").val(r.nombre);

        $('.tools .collapse', '.portlet-lista-clientes').trigger('click');
        $("#datosCliente, #recepcion_documentos").css('display', 'block');
        aplicacion.rellenar(r);

        $('#mes_h').val($('#mes').val());
        $('#ano_h').val($('#ano').val());
    });
}

function buscar() {
    var id = parseInt($("#cliente_id").val()),
        mes = parseInt($("#mes").val()),
        ano = parseInt($("#ano").val());

    if (isNaN(id)) {
        id = 0;
    }

    if (isNaN(mes)) {
        mes = 0;
    }

    if (isNaN(ano)) {
        ano = 0;
    }

    if (id == 0 || mes == 0 || ano == 0 || (busqueda[0] == id && busqueda[1] == mes && busqueda[2] == ano)) {
        $('.tools .collapse', '.portlet-lista-clientes').trigger('click');
        if (busqueda[0] != id) {
            busqueda[0] = id;
            buscar_cliente();
        }
        return;
    }

    $.blockUI({
        message: "<i class='fa fa-cog fa-spin'></i> Buscando.",
        css: {
            border: 'none',
            fontSize: '26px',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

    busqueda[0] = id;
    busqueda[1] = mes;
    busqueda[2] = ano;

    $('#mes_h').val(mes);
    $('#ano_h').val(ano);

    $.get($url + 'buscar', {
        'id': id,
        'mes': mes,
        'ano': ano
    }, function(r) {
        if (r.s === 'n') {
            aviso(r);
            return;
        }

        $('.tools .collapse', '.portlet-lista-clientes').trigger('click');

        $("input", "#cont-servicios").val('');

        var serv;
        aplicacion.rellenar(r);

        $("#cliente_id").val(r.id);
        $("#nombre_cliente").val(r.nombre);

        $("#datosCliente, #recepcion_documentos").css('display', 'block');

        for (var i in r.servicios) {
            serv = $("div.portlet[data-id='" + r.servicios[i] + "']", "#cont-servicios");
            serv.css('display', 'block');

            $(".recibido_admon", serv).each(function() {
                //$(this).blur();
                change_recibido_admon($(this));
                change_recibido_oper($(this));
            });
        }
        $.unblockUI();
    }).fail(function() {
        $.unblockUI();
    });
}