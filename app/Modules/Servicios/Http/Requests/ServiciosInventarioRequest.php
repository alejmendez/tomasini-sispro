<?php

namespace App\Modules\Servicios\Http\Requests;

use App\Http\Requests\Request;

class ServiciosInventarioRequest extends Request {
    protected $reglasArr = [
		'clientes_historico_id' => ['required', 'integer'], 
		'recibido' => ['required', 'date_format:"d/m/Y"'], 
		'proceso' => ['required', 'date_format:"d/m/Y"'], 
		'retorno' => ['required', 'date_format:"d/m/Y"'], 
		'envio' => ['required', 'date_format:"d/m/Y"'], 
		'entrega_admon' => ['required', 'date_format:"d/m/Y"'], 
		'entrega' => ['required', 'date_format:"d/m/Y"'], 
		'entrega_comunicacion' => ['required', 'date_format:"d/m/Y"']
	];
}