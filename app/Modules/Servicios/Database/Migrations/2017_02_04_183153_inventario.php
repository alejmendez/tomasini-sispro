<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Inventario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        MÓDULO DE SERVICIO DE INVENTARIOS
            CONTROL DE ELABORACION DE INVENTARIOS
                fecha de entrega de la contabilidad a yamilet
                fecha de proceso del inventario
                fecha de retorno al depósito - archivo
                fecha de entrega al deparatamento de administracion
            
            MÓDULO DE CONTROL DE ENTREGA DE LOS DOCUMENTOS
                Fecha de Entrega al Dpto. de Administración
                Fecha de Entrega al Personal Externo
                Fecha de Entrega de la Comunicación al Cliente
        */
        Schema::create('servicios_inventario', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->date('recibido')->nullable()->comment('Fecha de entrega de la contabilidad a yamilet');

            $table->date('proceso')->nullable()->comment('Fecha de proceso del inventario');
            $table->date('retorno')->nullable()->comment('Fecha de retorno al depósito - archivo');
            $table->date('envio')->nullable()->comment('Fecha de entrega al deparatamento de administracion');

            $table->date('entrega_admon')->nullable()->comment('Fecha de Entrega al Dpto. de Administración');
            $table->date('entrega')->nullable()->comment('Fecha de Entrega al Personal Externo');
            $table->date('entrega_comunicacion')->nullable()->comment('Fecha de Entrega de la Comunicación al Cliente');

            $table->string('comentario', 200)->nullable()->comment('Comentario');
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_inventario');
    }
}
