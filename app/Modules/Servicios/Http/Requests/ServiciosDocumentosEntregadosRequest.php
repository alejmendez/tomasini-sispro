<?php

namespace App\Modules\Servicios\Http\Requests;

use App\Http\Requests\Request;

class ServiciosDocumentosEntregadosRequest extends Request {
    protected $reglasArr = [
		'clientes_historico_id' => ['required', 'integer'], 
		'forma_74' => ['min:3', 'max:1'], 
		'forma_30' => ['min:3', 'max:1'], 
		'forma_35' => ['min:3', 'max:1'], 
		'forma_21' => ['min:3', 'max:1'], 
		'forma_igtf' => ['min:3', 'max:1'], 
		'forma_18' => ['min:3', 'max:1'], 
		'forma_22' => ['min:3', 'max:1'], 
		'inventario' => ['min:3', 'max:1'], 
		'libros' => ['min:3', 'max:1'], 
		'libros_compras_ventas' => ['min:3', 'max:1'], 
		'contabilidad' => ['min:3', 'max:1']
	];
}