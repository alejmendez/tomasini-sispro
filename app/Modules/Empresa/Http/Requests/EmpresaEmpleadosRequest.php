<?php

namespace App\Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class EmpresaEmpleadosRequest extends Request {
    protected $reglasArr = [
		'nacionalidad' => ['required'], 
		'dni' => ['required', 'integer', 'unique:empresa_empleados,dni'], 
		'nombre' => ['nombre', 'required', 'min:3', 'max:100'], 
		'apellido' => ['nombre', 'min:3', 'max:100'], 
		'correo' => ['email', 'min:3', 'max:50', 'unique:empresa_empleados,correo'], 
		'telefono' => ['telefono', 'min:3', 'max:15'], 
		'telefono_movil' => ['telefono', 'min:3', 'max:15'], 
		'nacimiento' => ['date_format:"d/m/Y"'], 
		'genero' => ['max:1'], 
		'edo_civil' => ['max:2'], 
		'domicilio' => ['min:3', 'max:200'], 
		'empresa_estructura_id' => ['required', 'integer'],
		'empresa_cargos_id' => ['required', 'integer']
	];
}