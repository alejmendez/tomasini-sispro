<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix') . '/clientes', 'namespace' => 'App\\Modules\Clientes\Http\Controllers'], function()
{
    Route::get('/', 				'ClientesController@index');
	Route::get('nuevo', 			'ClientesController@nuevo');
	Route::get('cambiar/{id}', 		'ClientesController@cambiar');
	
	Route::get('buscar/{id}', 		'ClientesController@buscar');

	Route::post('guardar',			'ClientesController@guardar');
	Route::put('guardar/{id}', 		'ClientesController@guardar');

	Route::delete('eliminar/{id}', 	'ClientesController@eliminar');
	Route::post('restaurar/{id}', 	'ClientesController@restaurar');
	Route::delete('destruir/{id}', 	'ClientesController@destruir');

	Route::get('datatable', 		'ClientesController@datatable');

	Route::get('reparar_servicios/{id?}', 	'ClientesController@reparar_servicios');


	Route::group(['prefix' => 'categorias'], function() {
		Route::get('/', 				'ClientesCategoriasController@index');
		Route::get('nuevo', 			'ClientesCategoriasController@nuevo');
		Route::get('cambiar/{id}', 		'ClientesCategoriasController@cambiar');
		
		Route::get('buscar/{id}', 		'ClientesCategoriasController@buscar');

		Route::post('guardar',			'ClientesCategoriasController@guardar');
		Route::put('guardar/{id}', 		'ClientesCategoriasController@guardar');

		Route::delete('eliminar/{id}', 	'ClientesCategoriasController@eliminar');
		Route::post('restaurar/{id}', 	'ClientesCategoriasController@restaurar');
		Route::delete('destruir/{id}', 	'ClientesCategoriasController@destruir');

		Route::get('datatable', 		'ClientesCategoriasController@datatable');
	});


	Route::group(['prefix' => 'rutas'], function() {
		Route::get('/', 				'ClientesRutasController@index');
		Route::get('nuevo', 			'ClientesRutasController@nuevo');
		Route::get('cambiar/{id}', 		'ClientesRutasController@cambiar');
		
		Route::get('buscar/{id}', 		'ClientesRutasController@buscar');

		Route::post('guardar',			'ClientesRutasController@guardar');
		Route::put('guardar/{id}', 		'ClientesRutasController@guardar');

		Route::delete('eliminar/{id}', 	'ClientesRutasController@eliminar');
		Route::post('restaurar/{id}', 	'ClientesRutasController@restaurar');
		Route::delete('destruir/{id}', 	'ClientesRutasController@destruir');

		Route::get('datatable', 		'ClientesRutasController@datatable');
	});


	Route::group(['prefix' => 'clientes_condicion_contribuyente'], function() {
		Route::get('/', 				'ClientesCondicionContribuyenteController@index');
		Route::get('nuevo', 			'ClientesCondicionContribuyenteController@nuevo');
		Route::get('cambiar/{id}', 		'ClientesCondicionContribuyenteController@cambiar');
		
		Route::get('buscar/{id}', 		'ClientesCondicionContribuyenteController@buscar');

		Route::post('guardar',			'ClientesCondicionContribuyenteController@guardar');
		Route::put('guardar/{id}', 		'ClientesCondicionContribuyenteController@guardar');

		Route::delete('eliminar/{id}', 	'ClientesCondicionContribuyenteController@eliminar');
		Route::post('restaurar/{id}', 	'ClientesCondicionContribuyenteController@restaurar');
		Route::delete('destruir/{id}', 	'ClientesCondicionContribuyenteController@destruir');

		Route::get('datatable', 		'ClientesCondicionContribuyenteController@datatable');
	});


	Route::group(['prefix' => 'clientes_razon_social'], function() {
		Route::get('/', 				'ClientesRazonSocialController@index');
		Route::get('nuevo', 			'ClientesRazonSocialController@nuevo');
		Route::get('cambiar/{id}', 		'ClientesRazonSocialController@cambiar');
		
		Route::get('buscar/{id}', 		'ClientesRazonSocialController@buscar');

		Route::post('guardar',			'ClientesRazonSocialController@guardar');
		Route::put('guardar/{id}', 		'ClientesRazonSocialController@guardar');

		Route::delete('eliminar/{id}', 	'ClientesRazonSocialController@eliminar');
		Route::post('restaurar/{id}', 	'ClientesRazonSocialController@restaurar');
		Route::delete('destruir/{id}', 	'ClientesRazonSocialController@destruir');

		Route::get('datatable', 		'ClientesRazonSocialController@datatable');
	});

    //{{route}}
});
