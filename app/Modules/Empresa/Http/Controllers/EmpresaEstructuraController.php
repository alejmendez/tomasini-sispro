<?php

namespace App\Modules\Empresa\Http\Controllers;

//Controlador Padre
use App\Modules\Empresa\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Empresa\Http\Requests\EmpresaEstructuraRequest;

//Modelos
use App\Modules\Empresa\Models\EmpresaEstructura;

class EmpresaEstructuraController extends Controller
{
    protected $titulo = 'Empresa Estructura';

    public $js = [
        'EmpresaEstructura'
    ];
    
    public $css = [
        'EmpresaEstructura'
    ];

    public $librerias = [
        'datatables',
        'jstree'
    ];

    public function index()
    {
        return $this->view('empresa::EmpresaEstructura', [
            'EmpresaEstructura' => new EmpresaEstructura()
        ]);
    }

    public function nuevo()
    {
        $EmpresaEstructura = new EmpresaEstructura();
        return $this->view('empresa::EmpresaEstructura', [
            'layouts' => 'base::layouts.popup',
            'EmpresaEstructura' => $EmpresaEstructura
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $EmpresaEstructura = EmpresaEstructura::find($id);
        return $this->view('empresa::EmpresaEstructura', [
            'layouts' => 'base::layouts.popup',
            'EmpresaEstructura' => $EmpresaEstructura
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $EmpresaEstructura = EmpresaEstructura::withTrashed()->find($id);
        } else {
            $EmpresaEstructura = EmpresaEstructura::find($id);
        }

        if ($EmpresaEstructura) {
            return array_merge($EmpresaEstructura->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EmpresaEstructuraRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $EmpresaEstructura = $id == 0 ? new EmpresaEstructura() : EmpresaEstructura::find($id);

            $EmpresaEstructura->fill($request->all());
            $EmpresaEstructura->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $EmpresaEstructura->id,
            'texto' => $EmpresaEstructura->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            EmpresaEstructura::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            EmpresaEstructura::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            EmpresaEstructura::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function arbol()
    {
        return EmpresaEstructura::arbol();
    }

    public function datatable(Request $request)
    {
        $sql = EmpresaEstructura::select([
            'id', 'nombre', 'empresa_estructura_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}