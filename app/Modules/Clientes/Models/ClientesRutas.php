<?php

namespace App\Modules\Clientes\Models;

use App\Modules\Base\Models\Modelo;

use App\Modules\Empresa\Models\EmpresaEmpleados;


class ClientesRutas extends modelo
{
    protected $table = 'clientes_rutas';
    protected $fillable = ["nombre", "responsable_ruta"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre de Ruta'
        ],
        'responsable_ruta' => [
            'type' => 'text',
            'label' => 'Responsable',
            'placeholder' => 'Responsable de la Ruta'
        ]
    ];

    public function empleado(){
        return $this->hasOne('App\Modules\Empresa\Models\EmpresaEmpleados', 'responsable_ruta');
    }
}