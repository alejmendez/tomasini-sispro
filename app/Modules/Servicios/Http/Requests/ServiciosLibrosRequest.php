<?php

namespace App\Modules\Servicios\Http\Requests;

use App\Http\Requests\Request;

class ServiciosLibrosRequest extends Request {
    protected $reglasArr = [
		'clientes_historico_id' => ['required', 'integer'], 
		'entrega_admon' => ['required', 'date_format:"d/m/Y"'], 
		'entrega' => ['required', 'date_format:"d/m/Y"'], 
		'entrega_comunicacion' => ['required', 'date_format:"d/m/Y"']
	];
}