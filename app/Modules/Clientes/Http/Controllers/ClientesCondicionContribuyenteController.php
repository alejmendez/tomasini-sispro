<?php

namespace App\Modules\Clientes\Http\Controllers;

//Controlador Padre
use App\Modules\Clientes\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Clientes\Http\Requests\ClientesCondicionContribuyenteRequest;

//Modelos
use App\Modules\Clientes\Models\ClientesCondicionContribuyente;

class ClientesCondicionContribuyenteController extends Controller
{
    protected $titulo = 'Clientes Condicion Contribuyente';

    public $js = [
        'ClientesCondicionContribuyente'
    ];
    
    public $css = [
        'ClientesCondicionContribuyente'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('clientes::ClientesCondicionContribuyente', [
            'ClientesCondicionContribuyente' => new ClientesCondicionContribuyente()
        ]);
    }

    public function nuevo()
    {
        $ClientesCondicionContribuyente = new ClientesCondicionContribuyente();
        return $this->view('clientes::ClientesCondicionContribuyente', [
            'layouts' => 'base::layouts.popup',
            'ClientesCondicionContribuyente' => $ClientesCondicionContribuyente
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $ClientesCondicionContribuyente = ClientesCondicionContribuyente::find($id);
        return $this->view('clientes::ClientesCondicionContribuyente', [
            'layouts' => 'base::layouts.popup',
            'ClientesCondicionContribuyente' => $ClientesCondicionContribuyente
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $ClientesCondicionContribuyente = ClientesCondicionContribuyente::withTrashed()->find($id);
        } else {
            $ClientesCondicionContribuyente = ClientesCondicionContribuyente::find($id);
        }

        if ($ClientesCondicionContribuyente) {
            return array_merge($ClientesCondicionContribuyente->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ClientesCondicionContribuyenteRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $ClientesCondicionContribuyente = $id == 0 ? new ClientesCondicionContribuyente() : ClientesCondicionContribuyente::find($id);

            $ClientesCondicionContribuyente->fill($request->all());
            $ClientesCondicionContribuyente->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $ClientesCondicionContribuyente->id,
            'texto' => $ClientesCondicionContribuyente->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            ClientesCondicionContribuyente::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            ClientesCondicionContribuyente::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            ClientesCondicionContribuyente::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = ClientesCondicionContribuyente::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}