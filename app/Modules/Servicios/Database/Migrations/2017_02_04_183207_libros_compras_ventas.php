<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LibrosComprasVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_libros_compras_ventas', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->date('recibido_admon')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Administración');
            $table->date('recibido_oper')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Operaciones');

            $table->date('proceso')->nullable()->comment('Fecha de proceso del inventario');
            $table->date('revision')->nullable()->comment('Fecha de revision por el supervisor');
            
            $table->date('entrega_admon')->nullable()->comment('Fecha de envio al departamento de Administración');
            $table->date('entrega')->nullable()->comment('Fecha de Entrega al Personal Externo');
            $table->date('entrega_comunicacion')->nullable()->comment('Fecha de Entrega de la Comunicación al Cliente');

            $table->string('comentario', 200)->nullable()->comment('Comentario');
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_libros_compras_ventas');
    }
}
