<?php

namespace App\Modules\Empresa\Models;

use App\Modules\Base\Models\Modelo;

class EmpresaCargos extends modelo
{
    protected $table = 'empresa_cargos';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Empresa Cargos'
        ]
    ];
}