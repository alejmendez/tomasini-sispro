<?php

namespace App\Modules\Empresa\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Modules\Empresa\Models\EmpresaCargos;
use App\Modules\Empresa\Models\EmpresaEmpleados;
use App\Modules\Base\Models\Usuario;

class EmpleadosSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$empleados = [
            ["Adannys Torres", "Asistente Administrativo I", 19534208],
            ["Carlos Perez", "Asistente de Contabilidad I", 16500690],
            ["Carlos Vallee", "Vicepresidente", 8898125],
            ["Carolina Tomasini", "Coordinador de Cuentas Por Cobrar", 14968680],
            ["Cristian Alvarez", "Jefe de Mercadeo", 19475560],
            ["Dariang Saavedra", "Asistente de Contribuyentes I", 23731781],
            ["Dayannery Castillo", "Asistente de Contabilidad I", 21261538],
            ["Erick Martinez", "Contador II", 19077886],
            ["Fatima Medina", "Asistente de Contabilidad I", 24542839],
            ["Freidymar Barrios", "Contador III", 21507399],
            ["Iris Hurtado", "Contador II", 23730360],
            ["Jaqueline Fuenmayor", "Asistente de Contabilidad I", 25914018],
            ["Javier Requena", "Personal Externo", 19730000],
            ["Jonathan Martinez", "Asistente de Contabilidad I", 19536582],
            ["Jorge Zapata", "Analista de Sistemas", 18450500],
            ["Maite Molina", "Asistente de Contabilidad II", 18886641],
            ["Marianny Fernandez", "Administradora", 20774860],
            ["Marianny Salazar", "Analista de Recursos Humanos I", 18476005],
            ["Michell Rodriguez", "Asistente Administrativo II", 16498056],
            ["Mileibys Marin", "Gerente de Operaciones", 12519498],
            ["Ninibeth Alvarez", "Contador II", 22800264],
            ["Raifer Rojas", "Contador III", 23731118],
            ["Rocio Medrano", "Asistente Administrativo I", 21580080],
            ["Ruben Martinez", "Asistente de Contribuyentes I", 17047380],
            ["Stephany Gutierrez", "Asistente de Contribuyentes II", 24377932],
            ["Stephany Romero", "Contador I", 24037912],
            ["Stewart Velasquez ", "Asistente de Cobranzas", 20557060],
            ["Yanitza Chacare", "Gerente de Auditoria e Impuestos", 16221021],
            ["Yeniree Zavala", "Contador I", 17162912],
            ["Ysbeth Tomasini", "Presidente", 8876489],
        ];

		$empleados_admon = [
			8876489, 
			16221021, 
			12519498,
			14968680,
			20774860,
			21580080
		];

		$empleados_ope = [
			17162912, 
			19077886,
			24037912,
			21507399,
			23731118,
			22800264,
			23730360
		];

		foreach ($empleados as $empleado) {
			list($nombre, $apellido) = explode(' ', $empleado[0]);
			
			$nombre = ucwords(strtolower($nombre));
			$apellido = ucwords(strtolower($apellido));

			$_cargo = $empleado[1];
			$cargo = EmpresaCargos::where('nombre', $_cargo)->first();
			if (!$cargo) {
				echo 'No esta el cargo "' . $_cargo . '"';
				exit;
			}

			EmpresaEmpleados::create([
				'nacionalidad'          => 'V', 
				'dni'                   => $empleado[2], 
				'nombre'                => $nombre, 
				'apellido'              => $apellido, 
				'empresa_estructura_id' => 1,
				'empresa_cargos_id'     => $cargo->id
			]);

			if (in_array($empleado[2], $empleados_admon)){
				Usuario::create([
					'usuario'       => strtolower(substr($nombre, 0, 1) . $apellido),
					'nombre'        => $nombre,
					'apellido'      => $apellido,
					'password'      => $empleado[2],
					'dni'           => $empleado[2],
					'autenticacion' => 'B',
					'perfil_id'     => $empleado[2] == 12519498 ? 6 : 7,
					'super'         => 'n'
				]);
			} elseif (in_array($empleado[2], $empleados_ope)){
				Usuario::create([
					'usuario'       => strtolower(substr($nombre, 0, 1) . $apellido),
					'nombre'        => $nombre,
					'apellido'      => $apellido,
					'password'      => $empleado[2],
					'dni'           => $empleado[2],
					'autenticacion' => 'B',
					'perfil_id'     => 8,
					'super'         => 'n'
				]);
			}
		}
	}
}
