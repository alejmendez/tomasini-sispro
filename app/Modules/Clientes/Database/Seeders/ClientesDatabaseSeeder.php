<?php

namespace App\Modules\Clientes\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClientesDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call(CategoriasSeeder::class);
		$this->call(CondicionSeeder::class);
		$this->call(ClientesRazonSocialSeeder::class);
		$this->call(CondicionContribuyenteSeeder::class);
		$this->call(RutasSeeder::class);
		//$this->call(ClientesSeeder::class);

		Model::reguard();
	}
}
