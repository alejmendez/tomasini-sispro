<?php

namespace App\Modules\Empresa\Models;

use App\Modules\Base\Models\Modelo;

class EmpresaBancos extends modelo
{
    protected $table = 'empresa_bancos';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Empresa Bancos'
        ]
    ];   
}