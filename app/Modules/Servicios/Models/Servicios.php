<?php

namespace App\Modules\Servicios\Models;

use App\Modules\Base\Models\Modelo;

class Servicios extends modelo
{
    protected $table = 'servicios';
    protected $fillable = ['nombre'];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Servicios'
        ]
    ];

    public function clientes()
    {
        return $this->belongsToMany('App\Modules\Clientes\Models\Clientes', 'servicios_clientes');
    }
}