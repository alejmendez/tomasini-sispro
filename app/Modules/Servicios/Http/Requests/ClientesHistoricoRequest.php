<?php

namespace App\Modules\Servicios\Http\Requests;

use App\Http\Requests\Request;

class ClientesHistoricoRequest extends Request {
    protected $reglasArr = [
		'mes' => ['required', 'integer'], 
		'ano' => ['required', 'integer'], 
		'clientes_id' => ['required', 'integer'], 
		'clientes_categorias_id' => ['required', 'integer'], 
		'clientes_rutas_id' => ['required', 'integer'], 
		'responsable_ruta' => ['required', 'integer'], 
		'supervisor' => ['required', 'integer'], 
		'jefe_departamento' => ['required', 'integer'], 
		'asistente' => ['required', 'integer']
	];
}