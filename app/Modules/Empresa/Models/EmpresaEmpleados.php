<?php

namespace App\Modules\Empresa\Models;

use App\Modules\Base\Models\Modelo;
use App\Modules\Empresa\Models\EmpresaCargos;

class EmpresaEmpleados extends modelo
{
    protected $table = 'empresa_empleados';
    protected $fillable = ["nacionalidad", "dni", "nombre", "apellido", "empresa_cargos_id", "correo", "telefono", "telefono_movil", "nacimiento", "genero", "edo_civil", "domicilio", "empresa_estructura_id"];
    protected $campos = [
        'empresa_estructura_id' => [
            'type' => 'hidden'
        ],
        'nacionalidad' => [
            'type' => 'select',
            'label' => 'Nacionalidad',
            'placeholder' => '- Nacionalidad del Empleado',
            'options' => [
                'v' => 'Venezolano',
                'e' => 'Extranjero'
            ]
        ],
        'dni' => [
            'type' => 'number',
            'label' => 'Cédula',
            'placeholder' => 'Cédula del Empleado'
        ],
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Empleado'
        ],
        'apellido' => [
            'type' => 'text',
            'label' => 'Apellido',
            'placeholder' => 'Apellido del Empleado'
        ],
        'empresa_cargos_id' => [
            'type' => 'select',
            'label' => 'Cargo',
            'placeholder' => '- Cargo del Empleado'
        ],
        'correo' => [
            'type' => 'text',
            'label' => 'Correo',
            'placeholder' => 'Correo del Empleado'
        ],
        'telefono' => [
            'type' => 'text',
            'label' => 'Telefono',
            'placeholder' => 'Telefono del Empleado'
        ],
        'telefono_movil' => [
            'type' => 'text',
            'label' => 'Telefono Movil',
            'placeholder' => 'Telefono Movil del Empleado'
        ],
        'nacimiento' => [
            'type' => 'text',
            'label' => 'Nacimiento',
            'placeholder' => 'Nacimiento del Empleado'
        ],
        'genero' => [
            'type' => 'select',
            'label' => 'Género',
            'placeholder' => '- Género del Empleado',
            'options' => [
                'm' => 'Masculino',
                'f' => 'Femenino'
            ]
        ],
        'edo_civil' => [
            'type' => 'select',
            'label' => 'Estado Civil',
            'placeholder' => '- Estado Civil del Empleado',
            'options' => [
                's' => 'Soltero',
                'c' => 'Casado',
                'd' => 'Divorsiado',
                'v' => 'Viudo',
            ]
        ],
        'domicilio' => [
            'type' => 'textarea',
            'label' => 'Domicilio',
            'cont_class' => 'col-md-12',
            'placeholder' => 'Domicilio del Empleado'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['empresa_cargos_id']['options'] = EmpresaCargos::orderBy('nombre')->pluck('nombre', 'id');
    }

    public function getNacimientoAttribute($value){
        return is_null($value) ? $value : $this->formatoFecha($value)->format('d/m/Y');
    }


    public function setNacimientoAttribute($value){
        return $this->formatoFecha($value)->format('Y-m-d');
    }

    public function setCorreoAttribute($value){
        $this->attributes['correo'] = strtolower($value);
    }

    public static function cne($c){
        return Cne::find($c);
    }

    public function cargo(){
        return $this->hasOne('App\Modules\Empresa\Models\EmpresaCargos');
    }

    public function estructura(){
        return $this->hasOne('App\Modules\Empresa\Models\EmpresaEstructura', 'empresa_estructura_id');
    }
}