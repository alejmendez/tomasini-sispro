<?php

namespace App\Modules\Clientes\Database\Seeders;

use DB;

use Illuminate\Database\Seeder;
use App\Modules\Clientes\Models\ClientesRutas;

class RutasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$rutas = [
    		['Av Maracay', 'HECTOR'],
    		['Avenida 17 de Diciembre', 'HECTOR'],
			['Avenida Andres Eloy Blanco', 'HECTOR'],
			['Avenida Jesus Soto', 'JAVIER'],
			['Avenida Libertador', 'HECTOR'],
			['Avenida Nueva Granada 1', 'HECTOR'],
			['Avenida Nueva Granada 2', 'MANUEL'],
			['Avenida República', 'ISMAEL'],
			['Centro 1', 'STEWART'],
			['Centro 2', 'MANUEL'],
			['Cruz Verde', 'STEWART'],
			['Estadio Herés', 'HECTOR'],
			['Hospital', 'HECTOR'],
			['La Paragua', 'OMAR'],
			['La Redoma', 'MANUEL'],
			['La sabanita', 'ISMAEL'],
			['Las Banderas', 'ISMAEL'],
			['Los Procesos', 'ISMAEL'],
			['Los Proceres', 'ISMAEL'],
			['Oficina', 'Oficina'],
			['Paseo Meneses 1', 'HECTOR'],
			['Paseo Meneses 2', 'MANUEL'],
			['Periferico', 'ISMAEL'],
			['Puerto Ordaz', 'CARLOS LUISIN'],
			['Terminal', 'ISMAEL'],
			['Vista Hermosa', 'ISMAEL']
    	];

    	DB::beginTransaction();
        try{
            foreach ($rutas as $ruta) {
		        ClientesRutas::create([
					'nombre' => $ruta[0],
					'responsable_ruta' => ucwords(strtolower($ruta[1]))
				]);
	    	}
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();
    }
}
