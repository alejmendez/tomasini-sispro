<?php

namespace App\Modules\Clientes\Models;

use App\Modules\Base\Models\Modelo;


class ClientesCondicionContribuyente extends modelo
{
    protected $table = 'clientes_condicion_contribuyente';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Clientes Condicion Contribuyente'
        ]
    ];

    public function clientes()
	{
		return $this->hasMany('App\Modules\Clientes\Models\Clientes');
	}
}