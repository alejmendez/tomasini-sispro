<?php

namespace App\Modules\Servicios\Http\Controllers;

//Controlador Padre
use App\Modules\Servicios\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Servicios\Http\Requests\ServiciosRequest;

//Modelos
use App\Modules\Servicios\Models\Servicios;

class ServiciosController extends Controller
{
    protected $titulo = 'Servicios';

    public $js = [
        'Servicios'
    ];
    
    public $css = [
        'Servicios'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('servicios::Servicios', [
            'Servicios' => new Servicios()
        ]);
    }

    public function nuevo()
    {
        $Servicios = new Servicios();
        return $this->view('servicios::Servicios', [
            'layouts' => 'base::layouts.popup',
            'Servicios' => $Servicios
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Servicios = Servicios::find($id);
        return $this->view('servicios::Servicios', [
            'layouts' => 'base::layouts.popup',
            'Servicios' => $Servicios
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Servicios = Servicios::withTrashed()->find($id);
        } else {
            $Servicios = Servicios::find($id);
        }

        if ($Servicios) {
            return array_merge($Servicios->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ServiciosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Servicios = $id == 0 ? new Servicios() : Servicios::find($id);

            $Servicios->fill($request->all());
            $Servicios->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Servicios->id,
            'texto' => $Servicios->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Servicios::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Servicios::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Servicios::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Servicios::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}