<?php

namespace App\Modules\Clientes\Http\Requests;

use App\Http\Requests\Request;

class ClientesRutasRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100'], 
		'responsable_ruta' => ['required', 'integer']
	];
}