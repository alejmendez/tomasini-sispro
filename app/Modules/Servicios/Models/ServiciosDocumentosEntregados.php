<?php

namespace App\Modules\Servicios\Models;

use App\Modules\Base\Models\Modelo;

class ServiciosDocumentosEntregados extends modelo
{
    protected $table = 'servicios_documentos_entregados';
    protected $fillable = [
        'clientes_historico_id',
        'entrega_admon',
        'forma_74',
        'forma_30',
        'forma_35_1',
        'forma_35_2',
        'forma_21_1',
        'forma_21_2',
        'forma_18_1',
        'forma_18_2',
        'forma_22',
        'inventario',
        'libros',
        'libros_compras_ventas',
        'contabilidad'
    ];
    
    public function clientes_historico()
	{
		return $this->hasOne('App\Modules\Servicios\Models\ClientesHistorico');
	}
}