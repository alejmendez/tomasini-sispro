<?php

namespace App\Modules\Empresa\Models;

class Cne {
	protected static $url = "http://www.cne.gov.ve/web/registro_electoral/ce.php?nacionalidad={n}&cedula={c}";

	public static function find($c){
        $c = intval($c);
		$resultado = self::_find('V', $c);
		if ($resultado){
			return $resultado;
		}

		$resultado = self::_find('E', $c);
		return $resultado;
	}

	protected static function _find($n, $c){
		$url = str_replace(['{n}', '{c}'], [$n, $c], self::$url);

		$resource = self::getUrl($url);
		$text = strip_tags($resource);
		$findme = 'SERVICIO ELECTORAL';
		$pos = strpos($text, $findme);

		$findme2 = 'ADVERTENCIA';
		$pos2 = strpos($text, $findme2);

		$findme3 = 'no corresponde a un elector';
        $pos3 = strpos($text, $findme3);

        if (!!$pos3 || ($pos == FALSE && $pos2 == TRUE)) {
            return null;
        }

		if ($pos == TRUE && $pos2 == FALSE) {
			$rempl = array('Cédula:', 'Nombre:', 'Estado:', 'Municipio:', 'Parroquia:', 'Centro:', 'Dirección:', 'SERVICIO ELECTORAL', 'Mesa:');
			$r = trim(str_replace($rempl, '|', self::clear($text)));
			$resource = explode("|", $r);
            
            $nombre = ucwords(strtolower($resource[2]));
            $nombre = trim($nombre);
            $nombre = preg_replace('/\s+/', ' ', $nombre);
            $resource = [
                'cedula' => $resource[1],
                'nombre' => $nombre,
                'estado' => str_replace('Edo. ', '', ucwords(strtolower($resource[3]))),
                'municipio' => str_replace('Ce. ', '', ucwords(strtolower($resource[4]))),
                'parroquia' => str_replace('Pq. ', '', ucwords(strtolower($resource[5]))),
            ];
		} elseif ($pos == FALSE && $pos2 == FALSE) {
			$rempl = array('Cédula:', 'Primer Nombre:', 'Segundo Nombre:', 'Primer Apellido:', 'Segundo Apellido:', 'ESTATUS','sexo:');
			$r = trim(str_replace($rempl, '|', $text));
			$resource = explode("|", $r);
		}

		return $resource;
	}

	public static function getUrl($url) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // almacene en una variable
		curl_setopt($curl, CURLOPT_HEADER, FALSE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		if (curl_exec($curl) === false) {
			echo 'Curl error: ' . curl_error($curl);
		} else {
			$return = curl_exec($curl);
		}

		curl_close($curl);

		return $return;
	}

	public static function clear($valor) {
		$rempl = array('\n', '\t');
		$r = trim(str_replace($rempl, ' ', $valor));
		return str_replace("\r", "", str_replace("\n", "", str_replace("\t", "", $r)));
	}
}