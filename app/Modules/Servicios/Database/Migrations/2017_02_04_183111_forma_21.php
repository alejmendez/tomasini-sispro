<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Forma21 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        IMPUESTO A LAS GRANDES TRANSACCIONES FINANCIERAS (I.G.T.F)
            FORMA 21 (IGTF) (quincenal)
                fecha de proceso por el asistente
                fecha revisado y declarado en el portal por el supervisor
                fecha de entrega al departamento de administracion
                
                MÓDULO DE SERVICIO BANCARIO (quincena 1) (quincena 2)
                    NUMERO DE LA PLANILLA
                    FECHA DE PAGO DE LA PLANILLA
                    ENTIDAD BANCARIA
                    MONTO CANCELADO

                MÓDULO DE CONTROL DE ENTREGA DE LOS DOCUMENTOS (1era. Quincena) (2da. Quincena)
                    Fecha de Entrega al Personal Externo
                    Fecha de Entrega de la Comunicación al Cliente
        */
        
        Schema::create('servicios_forma_21_1', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->date('recibido_admon')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Administración');
            $table->date('recibido_oper')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Operaciones');

            $table->date('proceso')->nullable()->comment('Fecha revisado y declarado en el portal por el supervisor');
            $table->date('revision_declarado')->nullable()->comment('Fecha revisado y declarado en el portal por el supervisor');
            $table->date('entrega_admon')->nullable()->comment('Fecha de envio al departamento de Administración');

            $table->string('certificado', 1)->nullable()->comment('Habilita o Desabilita la informacion del banco');
            $table->string('numero_planilla', 40)->nullable()->comment('Numero de la planilla');
            $table->date('pago_planilla')->nullable()->comment('Fecha de pago de la planilla');
            $table->integer('bancos_id')->nullable()->unsigned()->comment('Entidad bancaria');
            $table->decimal('monto_cancelado', 14, 2)->nullable()->comment('Monto cancelado');

            $table->date('entrega')->nullable()->comment('Fecha de Entrega al Personal Externo');
            $table->date('entrega_comunicacion')->nullable()->comment('Fecha de Entrega de la Comunicación al Cliente');

            $table->string('comentario', 200)->nullable()->comment('Comentario');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('bancos_id')->references('id')->on('empresa_bancos')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('servicios_forma_21_2', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->date('recibido_admon')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Administración');
            $table->date('recibido_oper')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Operaciones');

            $table->date('proceso')->nullable()->comment('Fecha revisado y declarado en el portal por el supervisor');
            $table->date('revision_declarado')->nullable()->comment('Fecha revisado y declarado en el portal por el supervisor');
            $table->date('entrega_admon')->nullable()->comment('Fecha de envio al departamento de Administración');

            $table->string('certificado', 1)->nullable()->comment('Habilita o Desabilita la informacion del banco');
            $table->string('numero_planilla', 40)->nullable()->comment('Numero de la planilla');
            $table->date('pago_planilla')->nullable()->comment('Fecha de pago de la planilla');
            $table->integer('bancos_id')->nullable()->unsigned()->comment('Entidad bancaria');
            $table->decimal('monto_cancelado', 14, 2)->nullable()->comment('Monto cancelado');

            $table->date('entrega')->nullable()->comment('Fecha de Entrega al Personal Externo');
            $table->date('entrega_comunicacion')->nullable()->comment('Fecha de Entrega de la Comunicación al Cliente');

            $table->string('comentario', 200)->nullable()->comment('Comentario');
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('bancos_id')->references('id')->on('empresa_bancos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_forma_21_1');
        Schema::dropIfExists('servicios_forma_21_2');
    }
}
