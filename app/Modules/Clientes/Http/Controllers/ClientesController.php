<?php

namespace App\Modules\Clientes\Http\Controllers;

//Controlador Padre
use App\Modules\Clientes\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Clientes\Http\Requests\ClientesRequest;

//Modelos
use App\Modules\Clientes\Models\Clientes;
use App\Modules\Servicios\Models\Servicios;
use App\Modules\Servicios\Models\ServiciosClientes;

class ClientesController extends Controller
{
    protected $titulo = 'Clientes';

    public $js = [
        'Clientes'
    ];
    
    public $css = [
        'Clientes'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('clientes::Clientes', [
            'Clientes' => new Clientes(),
            'Servicios' => Servicios::all()
        ]);
    }

    public function nuevo()
    {
        $Clientes = new Clientes();
        return $this->view('clientes::Clientes', [
            'layouts' => 'base::layouts.popup',
            'Clientes' => $Clientes,
            'Servicios' => Servicios::all()
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Clientes = Clientes::find($id);
        return $this->view('clientes::Clientes', [
            'layouts' => 'base::layouts.popup',
            'Clientes' => $Clientes,
            'Servicios' => Servicios::all()
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Clientes = Clientes::withTrashed()->find($id);
        } else {
            $Clientes = Clientes::find($id);
        }

        if ($Clientes) {
            $Clientes->servicios = ServiciosClientes::where('clientes_id', $Clientes->id)->select('servicios_id')->get();

            $Clientes->servicios = $Clientes->servicios->pluck('servicios_id');
            return array_merge($Clientes->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ClientesRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Clientes = $id == 0 ? new Clientes() : Clientes::find($id);

            $Clientes->fill($request->all());
            $Clientes->slug = str_slug($Clientes->nombre);
            $Clientes->save();

            ServiciosClientes::where('clientes_id', $Clientes->id)->delete();
            foreach ($request->servicios as $servicio_id => $valor) {
                ServiciosClientes::create([
                    'servicios_id' => $servicio_id,
                    'clientes_id' => $Clientes->id
                ]);
            }
        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Clientes->id,
            'texto' => $Clientes->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Clientes::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Clientes::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function reparar_servicios(Request $request, $id = 0)
    {
        set_time_limit(0);
        $servicios = [
            [],
            [1, 2, 3, 4, 5, 6, 7, 9],
            [4, 7, 9],
            [1, 4, 7, 8, 9],
            []
        ];
        
        if ($id == 0) {
            $Clientes = Clientes::all();
        } else {
            $Clientes = Clientes::find($id);
        }

        foreach ($Clientes as $Cliente) {
            ServiciosClientes::where('clientes_id', $Cliente->id)->delete();
            if (is_null($Cliente->clientes_condicion_contribuyente_id)){
                continue;
            }
            
            foreach ($servicios[$Cliente->clientes_condicion_contribuyente_id] as $servicio_id) {
                ServiciosClientes::create([
                    'servicios_id' => $servicio_id,
                    'clientes_id' => $Cliente->id
                ]);
            }
        }

        return 'listo! ' . \Carbon\Carbon::now();
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Clientes::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Clientes::select([
            'clientes.id', 'clientes.nombre', 'clientes_condicion_contribuyente.nombre as razon_social', 'clientes.deleted_at'
        ])
        ->leftJoin('clientes_condicion_contribuyente', 'clientes_condicion_contribuyente.id', '=', 'clientes.clientes_condicion_contribuyente_id');

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}