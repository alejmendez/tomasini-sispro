<?php

namespace App\Modules\Clientes\Models;

use App\Modules\Base\Models\Modelo;

class ClientesRazonSocial extends modelo
{
    protected $table = 'clientes_razon_social';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Clientes Razon Social'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    public function clientes()
	{
		return $this->hasMany('App\Modules\Clientes\Models\Clientes');
	}
}