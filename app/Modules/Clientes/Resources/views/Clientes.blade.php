@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Clientes']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Clientes.',
        'columnas' => [
            'Nombre' => '70',
    		'Razon Social' => '30'
        ]
    ])
@endsection

@section('content')
    {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
        <div class="row">
            {!! $Clientes->generate() !!}
        </div>

        <div class="row servicios">
            <h3>Servicios que Aplican al Cliente</h3><br />
            @foreach ($Servicios as $Servicio)
            <label>
                <input id="servicios_{{ $Servicio->id }}" name="servicios[{{ $Servicio->id }}]" value="1" type="checkbox" />
                {{ $Servicio->nombre }}
            </label>
            @endforeach
        </div>
    {!! Form::close() !!}
@endsection

@push('css')
<style type="text/css">
.servicios label {
    width: 100%;
    padding: 10px;
}

.servicios label:hover {
    background-color: #ffffff;
}
</style>
@endpush