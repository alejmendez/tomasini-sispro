<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contabilidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        MÓDULO DE SERVICIO DE CONTABILIDAD
            CONTROL DE PROCESO Y REVISION DE LA CONTABILIDAD
                fecha de proceso por el asistente
                fecha de revision por el supervisor
                fecha de envio al depósito - archivo
            
            MÓDULO DE CONTROL DE ENTREGA DE LOS DOCUMENTOS
                Fecha de Entrega al Personal Externo
                Fecha de Entrega de la Comunicación al Cliente
        */
        Schema::create('servicios_contabilidad', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->date('proceso')->nullable()->comment('Fecha de proceso por el asistente');
            $table->date('revision')->nullable()->comment('Fecha de revision por el supervisor');
            $table->date('envio')->nullable()->comment('Fecha de envio al depósito - archivo');

            $table->date('entrega')->nullable()->comment('Fecha de Entrega al Personal Externo');
            $table->date('entrega_comunicacion')->nullable()->comment('Fecha de Entrega de la Comunicación al Cliente');
            $table->string('entrega_documentos', 1)->nullable()->comment('Entrega de documentos');

            $table->string('comentario', 200)->nullable()->comment('Comentario');
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_contabilidad');
    }
}
