<?php

namespace App\Modules\Empresa\Http\Controllers;

//Controlador Padre
use App\Modules\Empresa\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Empresa\Http\Requests\EmpresaBancosRequest;

//Modelos
use App\Modules\Empresa\Models\EmpresaBancos;

class EmpresaBancosController extends Controller
{
    protected $titulo = 'Empresa Bancos';

    public $js = [
        'EmpresaBancos'
    ];
    
    public $css = [
        'EmpresaBancos'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('empresa::EmpresaBancos', [
            'EmpresaBancos' => new EmpresaBancos()
        ]);
    }

    public function nuevo()
    {
        $EmpresaBancos = new EmpresaBancos();
        return $this->view('empresa::EmpresaBancos', [
            'layouts' => 'base::layouts.popup',
            'EmpresaBancos' => $EmpresaBancos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $EmpresaBancos = EmpresaBancos::find($id);
        return $this->view('empresa::EmpresaBancos', [
            'layouts' => 'base::layouts.popup',
            'EmpresaBancos' => $EmpresaBancos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $EmpresaBancos = EmpresaBancos::withTrashed()->find($id);
        } else {
            $EmpresaBancos = EmpresaBancos::find($id);
        }

        if ($EmpresaBancos) {
            return array_merge($EmpresaBancos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EmpresaBancosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $EmpresaBancos = $id == 0 ? new EmpresaBancos() : EmpresaBancos::find($id);

            $EmpresaBancos->fill($request->all());
            $EmpresaBancos->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $EmpresaBancos->id,
            'texto' => $EmpresaBancos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            EmpresaBancos::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            EmpresaBancos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            EmpresaBancos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = EmpresaBancos::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}