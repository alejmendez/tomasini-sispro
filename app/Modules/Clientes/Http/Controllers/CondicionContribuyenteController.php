<?php

namespace App\Modules\Clientes\Http\Controllers;

//Controlador Padre
use App\Modules\Clientes\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Clientes\Http\Requests\CondicionContribuyenteRequest;

//Modelos
use App\Modules\Clientes\Models\CondicionContribuyente;

class CondicionContribuyenteController extends Controller
{
    protected $titulo = 'Condicion Contribuyente';

    public $js = [
        'CondicionContribuyente'
    ];
    
    public $css = [
        'CondicionContribuyente'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('clientes::CondicionContribuyente', [
            'CondicionContribuyente' => new CondicionContribuyente()
        ]);
    }

    public function nuevo()
    {
        $CondicionContribuyente = new CondicionContribuyente();
        return $this->view('clientes::CondicionContribuyente', [
            'layouts' => 'base::layouts.popup',
            'CondicionContribuyente' => $CondicionContribuyente
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $CondicionContribuyente = CondicionContribuyente::find($id);
        return $this->view('clientes::CondicionContribuyente', [
            'layouts' => 'base::layouts.popup',
            'CondicionContribuyente' => $CondicionContribuyente
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $CondicionContribuyente = CondicionContribuyente::withTrashed()->find($id);
        } else {
            $CondicionContribuyente = CondicionContribuyente::find($id);
        }

        if ($CondicionContribuyente) {
            return array_merge($CondicionContribuyente->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(CondicionContribuyenteRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $CondicionContribuyente = $id == 0 ? new CondicionContribuyente() : CondicionContribuyente::find($id);

            $CondicionContribuyente->fill($request->all());
            $CondicionContribuyente->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $CondicionContribuyente->id,
            'texto' => $CondicionContribuyente->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            CondicionContribuyente::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            CondicionContribuyente::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            CondicionContribuyente::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = CondicionContribuyente::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}