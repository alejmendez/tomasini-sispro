<?php

namespace App\Modules\Clientes\Http\Controllers;

//Controlador Padre
use App\Modules\Clientes\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Clientes\Http\Requests\ClientesRazonSocialRequest;

//Modelos
use App\Modules\Clientes\Models\ClientesRazonSocial;

class ClientesRazonSocialController extends Controller
{
    protected $titulo = 'Clientes Razon Social';

    public $js = [
        'ClientesRazonSocial'
    ];
    
    public $css = [
        'ClientesRazonSocial'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('clientes::ClientesRazonSocial', [
            'ClientesRazonSocial' => new ClientesRazonSocial()
        ]);
    }

    public function nuevo()
    {
        $ClientesRazonSocial = new ClientesRazonSocial();
        return $this->view('clientes::ClientesRazonSocial', [
            'layouts' => 'base::layouts.popup',
            'ClientesRazonSocial' => $ClientesRazonSocial
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $ClientesRazonSocial = ClientesRazonSocial::find($id);
        return $this->view('clientes::ClientesRazonSocial', [
            'layouts' => 'base::layouts.popup',
            'ClientesRazonSocial' => $ClientesRazonSocial
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $ClientesRazonSocial = ClientesRazonSocial::withTrashed()->find($id);
        } else {
            $ClientesRazonSocial = ClientesRazonSocial::find($id);
        }

        if ($ClientesRazonSocial) {
            return array_merge($ClientesRazonSocial->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ClientesRazonSocialRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $ClientesRazonSocial = $id == 0 ? new ClientesRazonSocial() : ClientesRazonSocial::find($id);

            $ClientesRazonSocial->fill($request->all());
            $ClientesRazonSocial->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $ClientesRazonSocial->id,
            'texto' => $ClientesRazonSocial->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            ClientesRazonSocial::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            ClientesRazonSocial::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            ClientesRazonSocial::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = ClientesRazonSocial::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}