<?php

namespace App\Modules\Clientes\Http\Controllers;

//Controlador Padre
use App\Modules\Clientes\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Clientes\Http\Requests\ClientesCategoriasRequest;

//Modelos
use App\Modules\Clientes\Models\ClientesCategorias;

class ClientesCategoriasController extends Controller
{
    protected $titulo = 'Clientes Categorias';

    public $js = [
        'ClientesCategorias'
    ];
    
    public $css = [
        'ClientesCategorias'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('clientes::ClientesCategorias', [
            'ClientesCategorias' => new ClientesCategorias()
        ]);
    }

    public function nuevo()
    {
        $ClientesCategorias = new ClientesCategorias();
        return $this->view('clientes::ClientesCategorias', [
            'layouts' => 'base::layouts.popup',
            'ClientesCategorias' => $ClientesCategorias
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $ClientesCategorias = ClientesCategorias::find($id);
        return $this->view('clientes::ClientesCategorias', [
            'layouts' => 'base::layouts.popup',
            'ClientesCategorias' => $ClientesCategorias
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $ClientesCategorias = ClientesCategorias::withTrashed()->find($id);
        } else {
            $ClientesCategorias = ClientesCategorias::find($id);
        }

        if ($ClientesCategorias) {
            return array_merge($ClientesCategorias->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ClientesCategoriasRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $ClientesCategorias = $id == 0 ? new ClientesCategorias() : ClientesCategorias::find($id);

            $ClientesCategorias->fill($request->all());
            $ClientesCategorias->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $ClientesCategorias->id,
            'texto' => $ClientesCategorias->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            ClientesCategorias::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            ClientesCategorias::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            ClientesCategorias::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = ClientesCategorias::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}