<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class import extends Command
{
    protected $signature = 'import {paso} {start_row=11}';

    protected $description = 'Import de archivos excel';

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $this->import = new importClass();
        $this->import->console = $this;

        $paso = $this->argument('paso');
        return $this->import->init($paso);
        
    }

    public function getOutput()
    {
        return $this->output;
    }
}