<?php

namespace App\Modules\Servicios\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Modules\Servicios\Models\Servicios;

class ServiciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$Servicios = [
            [1, 'Forma 74', 'forma_74'],
            [2, 'Forma 35 Segunda Quincena', 'forma_35_2'],
            [3, 'Forma 21 Segunda Quincena', 'forma_21_2'],
            [4, 'Forma 30', 'forma_30'],
            [5, 'Forma 35 Primera Quincena', 'forma_35_1'],
            [6, 'Forma 21 Primera Quincena', 'forma_21_1'],
            [7, 'Contabilidad', 'contabilidad'],
            [8, 'Inventario', 'inventario'],
            [9, 'Libros Legales', 'libros_legales'],
            [10, 'Forma 18 Segunda Quincena', 'forma_18_2'],
            [11, 'Forma 18 Primera Quincena', 'forma_18_1'],
            [12, 'Forma 22', 'forma_22']
    	];

    	foreach ($Servicios as $Servicio) {
	        Servicios::create([
                //'id' => $Servicio[0],
                'nombre' => $Servicio[1],
                'slug'   => $Servicio[2],
                'orden'  => $Servicio[0]
			]);
    	}
    }
}
