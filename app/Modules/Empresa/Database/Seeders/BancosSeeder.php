<?php

namespace App\Modules\Empresa\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Modules\Empresa\Models\EmpresaBancos;

class BancosSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$bancos = [
			'Cliente',
			'Certificado',
			'Online',
			'Correo',
			'100% Banco, Banco Comercial, C.A.',
			'100% Banco, Banco Comercial, C.A.',
			'BFC Banco Fondo Comun C.A. Banco Universal',
			'Bancamiga Banco Microfinanciero, C.A.',
			'Banco Activo, C.A. Banco Comercial',
			'Banco Agricola de Venezuela, C.A. Banco Universal',
			'Banco Caroni, C.A. Banco Universal',
			'Banco Central de Venezuela',
			'Banco Espíritu Santo',
			'Banco Exterior, C.A. Banco Universal',
			'Banco Internacional de Desarrollo, C.A.',
			'Banco Mercantil, C.A. S.A.C.A. Banco Universal',
			'Banco Nacional Crédito, C.A. Banco Universal',
			'Banco Occidental de Descuento Banco Universal, C.A. S.A.C.A',
			'Banco Plaza, C.A.',
			'Banco Provincial, S.A. Banco Universal',
			'Banco Sofitasa Banco Universal, C.A.',
			'Banco de Venezuela S.A.C.A. Banco Universal',
			'Banco de la Fuerza Armada Nacional Bolivariana, C.A.',
			'Banco de la Gente Emprendedora Bangente, c.a',
			'Banco del Caribe, C.A. Banco Universal',
			'Banco del Tesoro, C.A. Banco Universal',
			'Bancrecer S.A. Banco de Desarrollo',
			'Banesco Banco Universal S.A.C.A.',
			'Banplus Banco Comercial, C.A.',
			'Citibank N.A',
			'Del Sur Banco Universal, C.A.',
			'Instituto Municipal de Crédito Popular',
			'Mi Banco, Banco de Desarrollo, C.A.',
			'Venezolano de Crédito, S.A. Banco Universal',
		];

		foreach ($bancos as $banco) {
			EmpresaBancos::create([
				'nombre' => $banco
			]);
		}
	}
}
