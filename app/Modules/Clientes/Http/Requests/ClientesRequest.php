<?php

namespace App\Modules\Clientes\Http\Requests;

use App\Http\Requests\Request;

class ClientesRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100'], 
		'direccion' => ['required', 'min:3', 'max:200'], 
		'clientes_categorias_id' => ['required', 'integer'], 
		'clientes_rutas_id' => ['required', 'integer'],
		'clientes_condicion_contribuyente_id' => ['required', 'integer'],
		'clientes_razon_social_id' => ['required', 'integer'],
		'servicios.*' => ['required', 'integer']
	];
}