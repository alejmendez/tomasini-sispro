@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Empresa', 'Estructura']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar EmpresaEstructura.',
        'columnas' => [
            'Nombre' => '50',
		'Empresa Estructura' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $EmpresaEstructura->generate() !!}
        {!! Form::close() !!}

        <div class="col-md-12">
            <div id="arbol"></div>
        </div>
    </div>
@endsection