<?php

namespace App\Modules\Clientes\Database\Seeders;

use DB;

use Illuminate\Database\Seeder;
use App\Modules\Clientes\Models\ClientesCondicion;

class CondicionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$condiciones = [
    		'Activos',
            'Afectado',
    		'Asesorias',
            'Cesado',
            'Externo',
            'Retirado',
            'Supervisor',
            'Zulay'
    	];

        DB::beginTransaction();
        try{
            foreach ($condiciones as $condicion) {
                ClientesCondicion::create([
                    'nombre' => $condicion
                ]);
            }
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();
    }
}
