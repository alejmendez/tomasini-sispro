<?php

namespace App\Modules\Empresa\Http\Controllers;

//Controlador Padre
use App\Modules\Empresa\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Empresa\Http\Requests\EmpresaEmpleadosRequest;

//Modelos
use App\Modules\Empresa\Models\EmpresaEmpleados;
use App\Modules\Empresa\Models\EmpresaEstructura;


class EmpresaEmpleadosController extends Controller
{
    protected $titulo = 'Empresa Empleados';

    public $js = [
        'EmpresaEmpleados'
    ];
    
    public $css = [
        'EmpresaEmpleados'
    ];

    public $librerias = [
        'alphanum', 
        'bootstrap-datepicker',
        'maskedinput', 
        'datatables',
        'jstree'
    ];

    public function index()
    {
        return $this->view('empresa::EmpresaEmpleados', [
            'EmpresaEmpleados' => new EmpresaEmpleados()
        ]);
    }

    public function nuevo()
    {
        $EmpresaEmpleados = new EmpresaEmpleados();
        return $this->view('empresa::EmpresaEmpleados', [
            'layouts' => 'base::layouts.popup',
            'EmpresaEmpleados' => $EmpresaEmpleados
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $EmpresaEmpleados = EmpresaEmpleados::find($id);
        return $this->view('empresa::EmpresaEmpleados', [
            'layouts' => 'base::layouts.popup',
            'EmpresaEmpleados' => $EmpresaEmpleados
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $EmpresaEmpleados = EmpresaEmpleados::withTrashed()->find($id);
        } else {
            $EmpresaEmpleados = EmpresaEmpleados::find($id);
        }

        if ($EmpresaEmpleados) {
            return array_merge($EmpresaEmpleados->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EmpresaEmpleadosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $EmpresaEmpleados = $id == 0 ? new EmpresaEmpleados() : EmpresaEmpleados::find($id);

            $EmpresaEmpleados->fill($request->all());
            $EmpresaEmpleados->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $EmpresaEmpleados->id,
            'texto' => $EmpresaEmpleados->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            EmpresaEmpleados::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            EmpresaEmpleados::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            EmpresaEmpleados::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function arbol()
    {
        return EmpresaEstructura::arbol();
    }

    public function datatable(Request $request)
    {
        $sql = EmpresaEmpleados::select([
            'id',
            'dni',
            'nombre',
            'apellido',
            'correo',
            'telefono_movil',
            'nacimiento',
            'genero',
            'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}