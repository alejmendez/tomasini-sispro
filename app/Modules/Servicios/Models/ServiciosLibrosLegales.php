<?php

namespace App\Modules\Servicios\Models;

use Carbon\Carbon;
use App\Modules\Base\Models\Modelo;

use clientes_historico;

class ServiciosLibrosLegales extends modelo
{
    protected $table = 'servicios_libros';
    protected $fillable = [
        'clientes_historico_id',
        'entrega_admon',
        'entrega',
        'entrega_comunicacion',
        'comentario'
    ];
    
    public $campos = [
        'entrega_admon' => [
            'type'        => 'date',
            'label'       => 'Entrega a Administración',
            'placeholder' => 'Entrega a Administración',
            'title'       => 'Fecha de Documentos Recibidos Departamento de Administración'
        ],
        'entrega' => [
            'type'        => 'date',
            'label'       => 'Entrega al Personal Externo',
            'placeholder' => 'Entrega al Personal Externo',
            'title'       => 'Fecha de Entrega al Personal Externo'
        ],
        'entrega_comunicacion' => [
            'type'        => 'date',
            'label'       => 'Entrega de Comunicacion al Cliente',
            'placeholder' => 'Entrega de Comunicacion al Cliente',
            'title'       => 'Fecha de Entrega de la Comunicación al Cliente'
        ],
        'comentario' => [
            'type'        => 'textarea',
            'label'       => 'Comentario',
            'placeholder' => 'Comentario',
            'cont_class'  => 'col-sm-12'
        ]
    ];

    public function setEntregaAdmonAttribute($value)
    {
        $this->attributes['entrega_admon'] = $this->formatoFecha($value);
    }

    public function getEntregaAdmonAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }

    public function setEntregaAttribute($value)
    {
        $this->attributes['entrega'] = $this->formatoFecha($value);
    }

    public function getEntregaAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }

    public function setEntregaComunicacionAttribute($value)
    {
        $this->attributes['entrega_comunicacion'] = $this->formatoFecha($value);
    }

    public function getEntregaComunicacionAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
}