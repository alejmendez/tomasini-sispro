<?php

namespace App\Modules\Clientes\Models;

use App\Modules\Base\Models\Modelo;



class CondicionContribuyente extends modelo
{
    protected $table = 'condicion_contribuyente';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Condicion Contribuyente'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    public function clientes()
	{
		return $this->hasMany('App\Modules\Clientes\Models\Clientes');
	}

	
}