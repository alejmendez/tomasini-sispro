<?php

namespace App\Modules\Servicios\Http\Controllers;

//Controlador Padre
use App\Modules\Servicios\Http\Controllers\Controller;

//Dependencias
use DB;
use Excel;
use Carbon\Carbon;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use App\Console\Commands\importClass;

//Request
use App\Modules\Servicios\Http\Requests\TrabajosRequest;

//Modelos

use App\Modules\Empresa\Models\EmpresaEmpleados;

use App\Modules\Clientes\Models\Clientes;
use App\Modules\Clientes\Models\ClientesCategorias;
use App\Modules\Clientes\Models\ClientesCondicionContribuyente;
use App\Modules\Clientes\Models\ClientesRazonSocial;
use App\Modules\Clientes\Models\ClientesRutas;

use App\Modules\Servicios\Models\ClientesHistorico;

use App\Modules\Servicios\Models\Servicios;
use App\Modules\Servicios\Models\ServiciosClientes;

use App\Modules\Servicios\Models\ServiciosContabilidad;
use App\Modules\Servicios\Models\ServiciosForma181;
use App\Modules\Servicios\Models\ServiciosForma182;
use App\Modules\Servicios\Models\ServiciosForma211;
use App\Modules\Servicios\Models\ServiciosForma212;
use App\Modules\Servicios\Models\ServiciosForma22;
use App\Modules\Servicios\Models\ServiciosForma30;
use App\Modules\Servicios\Models\ServiciosForma351;
use App\Modules\Servicios\Models\ServiciosForma352;
use App\Modules\Servicios\Models\ServiciosForma74;
use App\Modules\Servicios\Models\ServiciosInventario;
use App\Modules\Servicios\Models\ServiciosLibrosLegales;

use App\Modules\Servicios\Models\ServiciosDocumentosEntregados;


class TrabajosController extends Controller
{
    protected $titulo = 'Trabajos';

    public $js = [
        'jquery-maskmoney/dist/jquery.maskMoney.min.js',
        'Trabajos'
    ];
    
    public $css = [
        'Trabajos'
    ];

    public $librerias = [
        'datatables',
        'bootstrap-datepicker'
    ];

    public function index()
    {
        $usuario = auth()->user();
        $Clientes = new Clientes();
        $camposClientes = ["nombre", "clientes_categorias_id", "clientes_rutas_id", "clientes_condicion_contribuyente_id", "clientes_razon_social_id"];
        $Clientes->campos['nombre']['cont_class'] = 'col-sm-12 col-xs-12';

        foreach ($camposClientes as $campo) {
            $Clientes->campos[$campo]['type'] = 'div';
        }
        
        $servicios = Servicios::orderBy('orden')->get();

        $elementos = [
            'Clientes'          => $Clientes,
            'Servicios'         => $servicios,
            'ClientesHistorico' => new ClientesHistorico(),
            'contabilidad'      => new ServiciosContabilidad(),
            'forma_74'          => new ServiciosForma74(),
            'forma_30'          => new ServiciosForma30(),
            'forma_22'          => new ServiciosForma22(),
            'forma_18_1'        => new ServiciosForma181(),
            'forma_18_2'        => new ServiciosForma182(),
            'forma_21_1'        => new ServiciosForma211(),
            'forma_21_2'        => new ServiciosForma212(),
            'forma_35_1'        => new ServiciosForma351(),
            'forma_35_2'        => new ServiciosForma352(),
            'inventario'        => new ServiciosInventario(),
            'libros_legales'    => new ServiciosLibrosLegales(),
        ];

        foreach ($elementos as $id => $elemento) {
            if ($id == 'Clientes' || $id == 'Servicios') {
                continue;
            }

            unset($elemento->campos['clientes_historico_id']);

            foreach ($elemento->campos as $idEle => $ele) {
                if ($idEle == 'mes' || $idEle == 'ano') {
                    continue;
                }
                $elemento->campos[$idEle]['id'] = $id . '_' . $idEle;
                $elemento->campos[$idEle]['name'] = $id . '[' . $idEle . ']';
                
                if ($usuario->perfil_id == 7 && $idEle == 'recibido_oper') {
                    $elemento->campos[$idEle]['type'] = 'text';
                    $elemento->campos[$idEle]['readonly'] = 'readonly';
                    $elemento->campos[$idEle]['class'] = (isset($elemento->campos[$idEle]['class']) ? $elemento->campos[$idEle]['class'] . ' ' : '') . $idEle;
                    continue;
                }

                if ($usuario->perfil_id == 8 && $idEle == 'recibido_admon') {
                    $elemento->campos[$idEle]['type'] = 'text';
                    $elemento->campos[$idEle]['readonly'] = 'readonly';
                    $elemento->campos[$idEle]['class'] = (isset($elemento->campos[$idEle]['class']) ? $elemento->campos[$idEle]['class'] . ' ' : '') . $idEle;
                    continue;
                }
                
                if ($elemento->campos[$idEle]['type'] == 'date') {
                    $elemento->campos[$idEle]['readonly'] = 'readonly';
                    $elemento->campos[$idEle]['type'] = 'text';
                    $elemento->campos[$idEle]['class'] = 'fecha';
                    $elemento->campos[$idEle]['placeholder'] = 'dd/mm/YYYY';
                    //$elemento->campos[$idEle]['data-language'] = 'es';
                    //$elemento->campos[$idEle]['data-format'] = 'dd/mm/YYYY';

                    $elemento->campos[$idEle]['content_before'] = '<div class="input-group date date-' . $idEle . '">';
                    $elemento->campos[$idEle]['content_after'] = '<div class="input-group-addon"><span class="fa fa-calendar"></span></div></div>';
                }

                $elemento->campos[$idEle]['class'] = (isset($elemento->campos[$idEle]['class']) ? $elemento->campos[$idEle]['class'] . ' ' : '') . $idEle;
            }
        }
        
        return $this->view('servicios::Trabajos', $elementos);
    }

    public function buscarCliente(Request $request, $id)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Clientes = Clientes::withTrashed()->find($request->id);
        } else {
            $Clientes = Clientes::find($request->id);
        }

        if (!$Clientes) {
            return trans('controller.nobuscar');
        }

        $Clientes->clientes_categorias_id = ClientesCategorias::find($Clientes->clientes_categorias_id)->nombre;
        $Clientes->clientes_rutas_id = ClientesRutas::find($Clientes->clientes_rutas_id)->nombre;
        $Clientes->clientes_condicion_contribuyente_id = ClientesCondicionContribuyente::find($Clientes->clientes_condicion_contribuyente_id)->nombre;
        $Clientes->clientes_razon_social_id = ClientesRazonSocial::find($Clientes->clientes_razon_social_id)->nombre;

        $Clientes->servicios = ServiciosClientes::where('clientes_id', $Clientes->id)->select('servicios_id')->get();

        $Clientes->servicios = $Clientes->servicios->pluck('servicios_id');

        return array_merge($Clientes->toArray(), [
            's' => 's'
        ]);
    }

    public function buscar(Request $request)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Clientes = Clientes::withTrashed()->find($request->id);
        } else {
            $Clientes = Clientes::find($request->id);
        }

        if (!$Clientes) {
            return trans('controller.nobuscar');
        }

        $Clientes->clientes_categorias_id = ClientesCategorias::find($Clientes->clientes_categorias_id)->nombre;
        $Clientes->clientes_rutas_id = ClientesRutas::find($Clientes->clientes_rutas_id)->nombre;
        $Clientes->clientes_condicion_contribuyente_id = ClientesCondicionContribuyente::find($Clientes->clientes_condicion_contribuyente_id)->nombre;
        $Clientes->clientes_razon_social_id = ClientesRazonSocial::find($Clientes->clientes_razon_social_id)->nombre;

        $Clientes->servicios = ServiciosClientes::where('clientes_id', $Clientes->id)->select('servicios_id')->get();

        $Clientes->servicios = $Clientes->servicios->pluck('servicios_id');
        
        $historico = ClientesHistorico::where('clientes_id', $request->id)
            ->where('mes', $request->mes)
            ->where('ano', $request->ano)
            ->first();
        
        if ($historico) {
            $Clientes->ClientesHistorico_recibido_admon = $historico->recibido_admon;
            $Clientes->ClientesHistorico_recibido_oper = $historico->recibido_oper;
            $servicios = Servicios::all();

            foreach ($servicios as $servicio) {
                unset($_servicio, $trabajo);
                $_servicio = 'App\Modules\Servicios\Models\Servicios' . str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $servicio->slug)));
                $trabajo = $_servicio::where('clientes_historico_id', $historico->id)->first();
                
                if (!$trabajo) {
                    continue;
                }
                $data = $trabajo->getAttributes();

                foreach ($trabajo->campos as $key => $value) {
                    if ($key == 'clientes_historico_id') {
                        continue;
                    }
                    
                    $columna = $servicio->slug . '_' . $key;
                    if ($value['type'] == 'date' && $data[$key] != '') {
                        $fecha = explode('-', $data[$key]);
                        $data[$key] = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
                    }

                    $Clientes->{$columna} = $data[$key];
                }
            }
        }
        
        return $Clientes;
    }

    public function guardar(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $servicios = Servicios::all();

            $Clientes = Clientes::find($data['cliente_id']);

            if (!$Clientes) {
                DB::rollback();
                return ['s' => 'n', 'msj' => 'El cliente seleccionado no existe.'];
            }

            $historico = ClientesHistorico::where('clientes_id', $data['cliente_id'])
                ->where('mes', $data['mes'])
                ->where('ano', $data['ano'])
                ->first();

            if (!$historico) {
                $historico = ClientesHistorico::create([
                    'clientes_id'            => $data['cliente_id'],
                    'mes'                    => $data['mes'],
                    'ano'                    => $data['ano'],
                    'recibido_admon'         => $data['ClientesHistorico']['recibido_admon'],
                    'recibido_oper'          => $data['ClientesHistorico']['recibido_oper'],
                    'clientes_categorias_id' => $Clientes->clientes_categorias_id,
                    'clientes_rutas_id'      => $Clientes->clientes_rutas_id,
                    'responsable_ruta'       => $Clientes->ruta->responsable_ruta,
                    'supervisor'             => $Clientes->supervisor,
                    'jefe_departamento'      => $Clientes->jefe_departamento,
                    'asistente'              => $Clientes->asistente,
                ]);
            }
            
            $datoDocumentosEntregados = [
                'entrega'               => 'n',
                'forma_74'              => 'n',
                'forma_30'              => 'n',
                'forma_22'              => 'n',
                'forma_18_1'            => 'n',
                'forma_18_2'            => 'n',
                'forma_21_1'            => 'n',
                'forma_21_2'            => 'n',
                'forma_35_1'            => 'n',
                'forma_35_2'            => 'n',
                'inventario'            => 'n',
                'libros'                => 'n',
                'libros_compras_ventas' => 'n',
                'contabilidad'          => 'n',
            ];

            foreach ($servicios as $servicio) {
                if (!isset($data[$servicio->slug])){
                    continue;
                }

                $dato = $data[$servicio->slug];
                foreach ($dato as $key => $value) {
                    if ($value == ''){
                        $dato[$key] = null;
                    }
                }
               
                $datoDocumentosEntregados[$servicio->slug] = @is_null($dato['recibido_admon']) ? 'n' : 's';

                if ($datoDocumentosEntregados[$servicio->slug] == 's') {
                    $datoDocumentosEntregados['entrega'] = 's';
                }

                $_servicio = 'App\Modules\Servicios\Models\Servicios' . str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $servicio->slug)));
                $trabajo = $_servicio::updateOrCreate(
                    ['clientes_historico_id' => $historico->id],
                    $dato
                );
            }

            ServiciosDocumentosEntregados::updateOrCreate(
                ['clientes_historico_id' => $historico->id],
                $datoDocumentosEntregados
            );
        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function supervisores()
    {
        $_empleados = EmpresaEmpleados::all();
        $empleados = [];

        foreach ($_empleados as $empleado) {
            $empleados[$empleado->id] = $empleado->nombre . ' ' . $empleado->apellido;
        }

        return $empleados;
    }

    public function datatable(Request $request)
    {
        $usuario = auth()->user();

        $sql = Clientes::select([
            'clientes.id', 
            'clientes.nombre', 
            'clientes_condicion_contribuyente.nombre as razon_social', 
            'clientes.deleted_at'
        ])
        ->leftJoin('clientes_condicion_contribuyente', 'clientes_condicion_contribuyente.id', '=', 'clientes.clientes_condicion_contribuyente_id')
        ->leftJoin('clientes_historico', [
            ['clientes.id', '=', 'clientes_historico.clientes_id'],
            ['clientes_historico.mes', '=', DB::raw($request->get('mes'))],
            ['clientes_historico.ano', '=', DB::raw($request->get('ano'))]
        ])
        ->leftJoin('servicios_documentos_entregados', 'servicios_documentos_entregados.clientes_historico_id', '=', 'clientes_historico.id');

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        if ($request->has('entrego_documentos')) {
            $sql->where('servicios_documentos_entregados.entrega', $request->get('entrego_documentos'));
        }

        if ($request->has('supervisor')) {
            $sql->where('clientes.supervisor', $request->get('supervisor'));
        }

        if ($usuario->super != 's'){
            if ($usuario->perfil_id == 8) {
                $empleado = EmpresaEmpleados::where('dni', $usuario->dni)->first();
                $empleado_id = 0;

                if ($empleado) {
                    $empleado_id = $empleado->id;
                }

                $sql->where('clientes.supervisor', $empleado_id);
            }
        }
        

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function import(Request $request)
    {
        $import = new importClass();
        $import->init(2);
        $import->procesar_operaciones();
    }
}