@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    <div id="botonera">
        <div class="msj-botonera bg-red bg-font-red text-left" style="display: none;"></div>
        <div class="btn-group btn-group-solid">
            <button id="limpiar" class="btn default tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.clean.title') }}">
                <i class="fa fa-file-o" aria-hidden="true"></i>
                <span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.clean.btn') }}</span>
            </button>

            <button id="guardar" class="btn blue tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.save.title') }}">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                <span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.save.btn') }}</span>
            </button>
        </div>
    </div>
    @include('base::partials.ubicacion', ['ubicacion' => ['Servicios', 'Trabajo']])
@endsection

@section('content')
<div class="row">
    {!! $ClientesHistorico->generate(['mes', 'ano']) !!}
</div>

<div class="portlet box blue portlet-lista-clientes">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-list"></i> Lista de clientes.
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="javascript:;" class="reload"> </a>
            <a href="javascript:;" class="fullscreen"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <label>&iquest;Entreg&oacute; documentos?</label>
                {{ Form::select('entrego_documentos', [
                    's' => 'SI',
                    'n' => 'NO',
                ], '', [
                    'id'          => 'entrego_documentos',
                    'class'       => 'form-control',
                    'placeholder' => '- Seleccione'
                ]) }} 
            </div>

            @if ($usuario->super == 's')
            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <label>Supervisor:</label>
                {{ Form::select('supervisor', $controller->supervisores(), '', [
                    'id'          => 'supervisor',
                    'class'       => 'form-control',
                    'placeholder' => '- Seleccione'
                ]) }} 
            </div>
            @endif
        </div>
        <table id="tabla" class="table table-striped table-hover table-bordered" style="width: 100%;">
            <thead>
                <tr>
                    <th style="width: 70%;">Nombre</th>
                    <th style="width: 30%;">Razon Social</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th style="width: 70%;">Nombre</th>
                    <th style="width: 30%;">Razon Social</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
    <input id="cliente_id" name="cliente_id" type="hidden" />
    <input id="ano_h" name="ano" type="hidden" value="{{ date('m') }}" />
    <input id="mes_h" name="mes" type="hidden" value="{{ date('Y') }}" />

    <div id="datosCliente" class="portlet box red-thunderbird">
        <div class="portlet-title">
            <div class="caption">
                Informaci&oacute;n del Cliente
            </div>
            <div class="tools">
                <a href="" class="collapse" data-original-title="" title=""> </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                {!! $Clientes->generate(["nombre", "clientes_categorias_id", "clientes_rutas_id", "clientes_condicion_contribuyente_id", "clientes_razon_social_id"]) !!}
            </div>
        </div>
    </div>

    <div id="recepcion_documentos" class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-book"></i> Recepci&oacute;n de documentos
            </div>
            <div class="tools">
                <a href="" class="collapse" data-original-title="" title=""> </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                 {!! $ClientesHistorico->generate(['recibido_admon', 'recibido_oper']) !!}
            </div>
        </div>
    </div>

   
    
    <div id="cont-servicios">
        @foreach($Servicios as $servicio)
        <div id="{{ $servicio->slug }}" data-id="{{ $servicio->id }}" class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-files-o"></i> {{ $servicio->nombre }}
                </div>
                <div class="tools">
                    <a href="" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                @if (isset(${$servicio->slug}->campos['recibido_admon']))
                <div class="row">
                    <h3 class="col-md-12">Fechas de Recibido</h3>
                    {!! ${$servicio->slug}->generate(['recibido_admon', 'recibido_oper']) !!}
                </div>
                <hr />
                <div class="row row-servicio-condicion">
                @else
                <div class="row">
                @endif
                    @foreach(${$servicio->slug}->campos as $campo => $prop)
                    <?php 
                    if ($campo == 'recibido_admon' || $campo == 'recibido_oper') { continue; }
                    ?>

                    @if ($usuario->perfil_id != 8)
                        @if (ends_with($campo, 'regular_numero_planilla'))
                        <h3 class="col-md-12">Regular</h3>
                        @endif

                        @if (ends_with($campo, 'comp_numero_planilla'))
                        <h3 class="col-md-12">Complementaria</h3>
                        @endif

                        @if (ends_with($campo, 'susti_numero_planilla'))
                        <h3 class="col-md-12">Sustitutiva</h3>
                        @endif
                    @endif

                    @if ($usuario->perfil_id == 8)
                        @if (!(ends_with($campo, '_planilla') || ends_with($campo, '_bancos_id') || ends_with($campo, '_cancelado') || $campo == 'entrega' || $campo == 'entrega_comunicacion'))
                            {!! ${$servicio->slug}->generate([$campo]) !!}
                        @endif
                    @elseif ($usuario->perfil_id == 7)
                        @if (ends_with($campo, '_planilla') || ends_with($campo, '_bancos_id') || ends_with($campo, '_cancelado') || $campo == 'entrega' || $campo == 'entrega_comunicacion')
                            {!! ${$servicio->slug}->generate([$campo]) !!}
                        @endif
                    @else
                        {!! ${$servicio->slug}->generate([$campo]) !!}
                    @endif

                    
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </div>
{!! Form::close() !!}
@endsection

@push('css')
<style type="text/css">
    .table-scrollable{
        border: none;
    }
</style>
@endpush

@push('js')
<script>
var usuario = {!! json_encode($usuario) !!};
</script>
@endpush