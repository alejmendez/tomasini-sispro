<?php

namespace App\Modules\Servicios\Models;

use Carbon\Carbon;
use App\Modules\Base\Models\Modelo;

use clientes_historico;

class ServiciosInventario extends modelo
{
    protected $table = 'servicios_inventario';
    protected $fillable = [
        'clientes_historico_id',
        'recibido',
        'proceso',
        'envio',
        'certificado',
        'entrega_admon',
        'entrega_comunicacion',
        'comentario'
    ];
    
    public $campos = [
        'recibido' => [
            'type'        => 'date',
            'label'       => 'Recibido',
            'placeholder' => 'Recibido'
        ],
        'proceso' => [
            'type'        => 'date',
            'label'       => 'Proceso',
            'placeholder' => 'Proceso'
        ],
        'entrega_admon' => [
            'type'        => 'date',
            'label'       => 'Envio a Administración',
            'placeholder' => 'Envio a Administración',
            'title'       => 'Fecha de envio al departamento de Administración'
        ],
        'entrega' => [
            'type'        => 'date',
            'label'       => 'Entrega al Personal Externo',
            'placeholder' => 'Entrega al Personal Externo',
            'title'       => 'Fecha de Entrega al Personal Externo'
        ],
        'entrega_comunicacion' => [
            'type'        => 'date',
            'label'       => 'Entrega de Comunicacion al Cliente',
            'placeholder' => 'Entrega de Comunicacion al Cliente',
            'title'       => 'Fecha de Entrega de la Comunicación al Cliente'
        ],
        'comentario' => [
            'type'        => 'textarea',
            'label'       => 'Comentario',
            'placeholder' => 'Comentario',
            'cont_class'  => 'col-sm-12'
        ]
    ];

    

    public function setRecibidoAttribute($value)
    {
        $this->attributes['recibido'] = $this->formatoFecha($value);
    }

    public function getRecibidoAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setProcesoAttribute($value)
    {
        $this->attributes['proceso'] = $this->formatoFecha($value);
    }

    public function getProcesoAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }

    public function setEnvioAttribute($value)
    {
        $this->attributes['envio'] = $this->formatoFecha($value);
    }

    public function getEnvioAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEntregaAdmonAttribute($value)
    {
        $this->attributes['entrega_admon'] = $this->formatoFecha($value);
    }

    public function getEntregaAdmonAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEntregaAttribute($value)
    {
        $this->attributes['entrega'] = $this->formatoFecha($value);
    }

    public function getEntregaAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEntregaComunicacionAttribute($value)
    {
        $this->attributes['entrega_comunicacion'] = $this->formatoFecha($value);
    }

    public function getEntregaComunicacionAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
}