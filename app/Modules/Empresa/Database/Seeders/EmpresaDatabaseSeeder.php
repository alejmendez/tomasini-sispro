<?php

namespace App\Modules\Empresa\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EmpresaDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call(BancosSeeder::class);
		$this->call(CargosSeeder::class);
		$this->call(EstructuraSeeder::class);
		$this->call(EmpleadosSeeder::class);

		Model::reguard();
	}
}
