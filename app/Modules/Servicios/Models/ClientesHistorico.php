<?php

namespace App\Modules\Servicios\Models;

use App\Modules\Base\Models\Modelo;

use App\Modules\Clientes\Models\Clientes;
use App\Modules\Clientes\Models\ClientesCategorias;
use App\Modules\Clientes\Models\ClientesRutas;

class ClientesHistorico extends modelo
{
	protected $table = 'clientes_historico';
	protected $fillable = [
		'mes',
		'ano',
		'recibido_admon',                //Fecha de Documentos Recibidos Departamento de Administración
        'recibido_oper',                 //Fecha de Documentos Recibidos Departamento de Operaciones 
		'clientes_id',
		'clientes_categorias_id',
		'clientes_rutas_id',
		'responsable_ruta',
		'supervisor',
		'jefe_departamento',
		'asistente'
	];

	public $campos = [
		'mes' => [
			'type'        => 'select',
			'label'       => 'Mes',
			'options'     => [
				1 => 'Enero',
				'Febrero',
				'Marzo',
				'Abril',
				'Mayo',
				'Junio',
				'Julio',
				'Agosto',
				'Septiembre',
				'Octubre',
				'Noviembre',
				'Diciembre'
			]
		],
		'ano' => [
			'type'        => 'select',
			'label'       => 'Año'
		],
		'recibido_admon' => [
            'type'        => 'date',
            'label'       => 'Recibido a Administración',
            'placeholder' => 'Recibido a Administración',
            'title'       => 'Fecha de Documentos Recibidos Departamento de Administración'
        ],
        'recibido_oper' => [
            'type'        => 'date',
            'label'       => 'Recibido a Operaciones',
            'placeholder' => 'Recibido a Operaciones',
            'cont_class'  => 'col-lg-3 col-md-4 col-sm-6 col-xs-12 cont-fecha-operaciones',
            'title'       => 'Fecha de Documentos Recibidos Departamento de Operaciones '
        ],
		'clientes_id' => [
			'type'        => 'select',
			'label'       => 'Clientes',
			'placeholder' => '- Seleccione un Clientes',
			'url'         => 'clientes'
		],
		'clientes_categorias_id' => [
			'type'        => 'select',
			'label'       => 'Clientes Categorias',
			'placeholder' => '- Seleccione un Clientes Categorias',
			'url'         => 'clientes_categorias'
		],
		'clientes_rutas_id' => [
			'type'        => 'select',
			'label'       => 'Clientes Rutas',
			'placeholder' => '- Seleccione un Clientes Rutas',
			'url'         => 'clientes_rutas'
		],
		'responsable_ruta' => [
			'type'        => 'select',
			'label'       => 'Responsable Ruta',
			'placeholder' => '- Seleccione un Responsable Ruta',
			'url'         => 'Agrega una URL Aqui!'
		],
		'supervisor' => [
			'type'        => 'select',
			'label'       => 'Supervisor',
			'placeholder' => '- Seleccione un Supervisor',
			'url'         => 'Agrega una URL Aqui!'
		],
		'jefe_departamento' => [
			'type'        => 'select',
			'label'       => 'Jefe Departamento',
			'placeholder' => '- Seleccione un Jefe Departamento',
			'url'         => 'Agrega una URL Aqui!'
		],
		'asistente' => [
			'type'        => 'select',
			'label'       => 'Asistente',
			'placeholder' => '- Seleccione un Asistente',
			'url'         => 'Agrega una URL Aqui!'
		]
	];

	public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);

		for ($i = date('Y'); $i >= 2016; $i--) { 
			$this->campos['ano']['options'][$i] = $i;
		}
		$this->campos['mes']['value'] = date('m');
		$this->campos['ano']['value'] = date('Y');

		$this->campos['clientes_id']['options'] = Clientes::pluck('nombre', 'id');
		$this->campos['clientes_categorias_id']['options'] = ClientesCategorias::pluck('nombre', 'id');
		$this->campos['clientes_rutas_id']['options'] = ClientesRutas::pluck('nombre', 'id');
	}

	public function clientes()
	{
		return $this->belongsTo('App\Modules\Clientes\Models\Clientes');
	}

	public function clientes_categorias()
	{
		return $this->belongsTo('App\Modules\Clientes\Models\ClientesCategorias');
	}

	public function clientes_rutas()
	{
		return $this->belongsTo('App\Modules\Clientes\Models\ClientesRutas');
	}

	public function servicios_contabilidad()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosContabilidad');
	}

	public function servicios_forma181()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma181');
	}

	public function servicios_forma182()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma182');
	}

	public function servicios_forma211()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma211');
	}

	public function servicios_forma212()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma212');
	}

	public function servicios_forma22()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma22');
	}

	public function servicios_forma30()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma30');
	}

	public function servicios_forma351()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma351');
	}

	public function servicios_forma352()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma352');
	}

	public function servicios_forma74()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosForma74');
	}

	public function servicios_inventario()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosInventario');
	}

	public function servicios_libros()
	{
		return $this->hasMany('App\Modules\Servicios\Models\ServiciosLibros');
	}
}