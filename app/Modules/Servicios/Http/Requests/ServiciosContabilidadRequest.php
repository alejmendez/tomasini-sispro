<?php

namespace App\Modules\Servicios\Http\Requests;

use App\Http\Requests\Request;

class ServiciosContabilidadRequest extends Request {
    protected $reglasArr = [
		'clientes_historico_id' => ['required', 'integer'], 
		'proceso' => ['required', 'date_format:"d/m/Y"'], 
		'revision' => ['required', 'date_format:"d/m/Y"'], 
		'envio' => ['required', 'date_format:"d/m/Y"'], 
		'entrega' => ['required', 'date_format:"d/m/Y"'], 
		'entrega_comunicacion' => ['required', 'date_format:"d/m/Y"']
	];
}