@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Clientes Rutas']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar ClientesRutas.',
        'columnas' => [
            'Nombre' => '50',
		'Responsable Ruta' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $ClientesRutas->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection