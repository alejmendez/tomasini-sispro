var aplicacion, tabla;

$(function() {
	tabla = $('#tabla').DataTable({
		ajax: {
			url: $url + "datatable",
			data: function (d) {
				d.mes = $('#mes').val();
				d.ano = $('#ano').val();
			}
		},
		"scrollX": true,
		columns: [
			{"data":"nombre", "name":"clientes.nombre"},
			{
				"data":"entrega", 
				"name":"servicios_documentos_entregados.entrega", 
				//"orderable" : false,
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_74", 
				"name":"servicios_documentos_entregados.forma_74", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_30", 
				"name":"servicios_documentos_entregados.forma_30", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_35_1", 
				"name":"servicios_documentos_entregados.forma_35_1", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_35_2", 
				"name":"servicios_documentos_entregados.forma_35_2", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_21_1", 
				"name":"servicios_documentos_entregados.forma_21_1", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_21_2", 
				"name":"servicios_documentos_entregados.forma_21_2", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_18_1", 
				"name":"servicios_documentos_entregados.forma_18_1", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_18_2", 
				"name":"servicios_documentos_entregados.forma_18_2", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"forma_22", 
				"name":"servicios_documentos_entregados.forma_22", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"inventario", 
				"name":"servicios_documentos_entregados.inventario", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"libros", 
				"name":"servicios_documentos_entregados.libros", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"libros_compras_ventas", 
				"name":"servicios_documentos_entregados.libros_compras_ventas", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			},
			{
				"data":"contabilidad", 
				"name":"servicios_documentos_entregados.contabilidad", 
				"render": function ( data, type, row ) {
					return render_col(data);
				}
			}
		]
	});


	$("#mes").on('change', function() {
		tabla.ajax.reload();
	});

	$("#ano").on('change', function() {
		tabla.ajax.reload();
	});
});

function render_col( str ) {
	console.log(str);
	if (str == 's') {
		var clase = 'success', str = 'Si';
	} else {
		var clase = 'danger', str = 'No';
	}
    return "<span class=\"label label-" + clase + "\">" + str + "</span>";
}