<?php

namespace App\Modules\Clientes\Http\Controllers;

//Controlador Padre
use App\Modules\Clientes\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Clientes\Http\Requests\ClientesRutasRequest;

//Modelos
use App\Modules\Clientes\Models\ClientesRutas;

class ClientesRutasController extends Controller
{
    protected $titulo = 'Clientes Rutas';

    public $js = [
        'ClientesRutas'
    ];
    
    public $css = [
        'ClientesRutas'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('clientes::ClientesRutas', [
            'ClientesRutas' => new ClientesRutas()
        ]);
    }

    public function nuevo()
    {
        $ClientesRutas = new ClientesRutas();
        return $this->view('clientes::ClientesRutas', [
            'layouts' => 'base::layouts.popup',
            'ClientesRutas' => $ClientesRutas
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $ClientesRutas = ClientesRutas::find($id);
        return $this->view('clientes::ClientesRutas', [
            'layouts' => 'base::layouts.popup',
            'ClientesRutas' => $ClientesRutas
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $ClientesRutas = ClientesRutas::withTrashed()->find($id);
        } else {
            $ClientesRutas = ClientesRutas::find($id);
        }

        if ($ClientesRutas) {
            return array_merge($ClientesRutas->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ClientesRutasRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $ClientesRutas = $id == 0 ? new ClientesRutas() : ClientesRutas::find($id);

            $ClientesRutas->fill($request->all());
            $ClientesRutas->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $ClientesRutas->id,
            'texto' => $ClientesRutas->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            ClientesRutas::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            ClientesRutas::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            ClientesRutas::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = ClientesRutas::select([
            'id', 'nombre', 'responsable_ruta', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}