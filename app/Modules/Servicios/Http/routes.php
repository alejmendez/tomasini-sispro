<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix') . '/servicios', 'namespace' => 'App\\Modules\Servicios\Http\Controllers'], function()
{
    Route::get('/', 				'ServiciosController@index');
	Route::get('nuevo', 			'ServiciosController@nuevo');
	Route::get('cambiar/{id}', 		'ServiciosController@cambiar');
	
	Route::get('buscar/{id}', 		'ServiciosController@buscar');

	Route::post('guardar',			'ServiciosController@guardar');
	Route::put('guardar/{id}', 		'ServiciosController@guardar');

	Route::delete('eliminar/{id}', 	'ServiciosController@eliminar');
	Route::post('restaurar/{id}', 	'ServiciosController@restaurar');
	Route::delete('destruir/{id}', 	'ServiciosController@destruir');

	Route::get('datatable', 		'ServiciosController@datatable');

	Route::group(['prefix' => 'trabajos'], function() {
		Route::get('/', 				  'TrabajosController@index');
		Route::get('buscar',	 		  'TrabajosController@buscar');
		Route::get('import',	 		  'TrabajosController@import');
		Route::get('buscar-cliente/{id}', 'TrabajosController@buscarCliente');
		#servicios/trabajos/import
		Route::get('import',              'TrabajosController@import');
		Route::post('guardar',			  'TrabajosController@guardar');
		Route::get('datatable', 	      'TrabajosController@datatable');
	});

	Route::group(['prefix' => 'reporte/documentos-entregados'], function() {
		Route::get('/', 				  'ReporteDocumentosController@index');
		Route::get('datatable', 		  'ReporteDocumentosController@datatable');
	});

    //{{route}}
});