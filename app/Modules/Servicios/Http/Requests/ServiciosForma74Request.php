<?php

namespace App\Modules\Servicios\Http\Requests;

use App\Http\Requests\Request;

class ServiciosForma74Request extends Request {
    protected $reglasArr = [
		'clientes_historico_id' => ['required', 'integer'], 
		'recibido_admon' => ['required', 'date_format:"d/m/Y"'], 
		'recibido_oper' => ['required', 'date_format:"d/m/Y"'], 
		'proceso' => ['required', 'date_format:"d/m/Y"'], 
		'revision_declarado' => ['required', 'date_format:"d/m/Y"'], 
		'envio' => ['required', 'date_format:"d/m/Y"'], 
		'tipo_servicio' => ['required', 'min:3', 'max:1'], 
		'numero_planilla' => ['required', 'min:3', 'max:10'], 
		'pago_planilla' => ['required', 'date_format:"d/m/Y"'], 
		'bancos_id' => ['required', 'integer'], 
		'monto_cancelado' => ['required'], 
		'entrega' => ['required', 'date_format:"d/m/Y"'], 
		'entrega_comunicacion' => ['required', 'date_format:"d/m/Y"']
	];
}