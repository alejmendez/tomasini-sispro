<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientesHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes_historico', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('mes')->unsigned();
            $table->integer('ano')->unsigned();

            $table->integer('clientes_id')->nullable()->unsigned();
            $table->integer('clientes_categorias_id')->nullable()->unsigned();
            $table->integer('clientes_rutas_id')->nullable()->unsigned();

            $table->string('responsable_ruta', 80)->nullable();

            $table->integer('supervisor')->nullable()->unsigned()->comment('Supervisor Responsable por Contribuyente');
            $table->integer('jefe_departamento')->nullable()->unsigned()->comment('Jefe de Departamento Responsable');
            $table->integer('asistente')->nullable()->unsigned()->comment('Asistente Responsable por Contribuyente');

            $table->date('recibido_admon')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Administración');
            $table->date('recibido_oper')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Operaciones');

            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('clientes_categorias_id')->references('id')->on('clientes_categorias')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('clientes_rutas_id')->references('id')->on('clientes_rutas')->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('supervisor')->references('id')->on('empresa_empleados')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('jefe_departamento')->references('id')->on('empresa_empleados')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('asistente')->references('id')->on('empresa_empleados')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes_historico');
    }
}
