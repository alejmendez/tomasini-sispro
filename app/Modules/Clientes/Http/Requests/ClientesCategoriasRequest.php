<?php

namespace App\Modules\Clientes\Http\Requests;

use App\Http\Requests\Request;

class ClientesCategoriasRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100']
	];
}