<?php

namespace App\Console\Commands;

use DB;
use Excel;
use Carbon\Carbon;
use Storage;

use App\Modules\Empresa\Models\EmpresaEmpleados;

use App\Modules\Clientes\Models\Clientes;
use App\Modules\Clientes\Models\ClientesCategorias;
use App\Modules\Clientes\Models\ClientesCondicion;
use App\Modules\Clientes\Models\ClientesCondicionContribuyente;
use App\Modules\Clientes\Models\ClientesRazonSocial;
use App\Modules\Clientes\Models\ClientesRutas;

use App\Modules\Servicios\Models\ClientesHistorico;

use App\Modules\Servicios\Models\Servicios;
use App\Modules\Servicios\Models\ServiciosClientes;

use App\Modules\Servicios\Models\ServiciosContabilidad;
use App\Modules\Servicios\Models\ServiciosForma181;
use App\Modules\Servicios\Models\ServiciosForma182;
use App\Modules\Servicios\Models\ServiciosForma211;
use App\Modules\Servicios\Models\ServiciosForma212;
use App\Modules\Servicios\Models\ServiciosForma22;
use App\Modules\Servicios\Models\ServiciosForma30;
use App\Modules\Servicios\Models\ServiciosForma351;
use App\Modules\Servicios\Models\ServiciosForma352;
use App\Modules\Servicios\Models\ServiciosForma74;
use App\Modules\Servicios\Models\ServiciosInventario;
use App\Modules\Servicios\Models\ServiciosLibrosLegales;

use App\Modules\Empresa\Models\EmpresaBancos;

use App\Modules\Servicios\Models\ServiciosDocumentosEntregados;

class importClass
{
    protected $errores = [];

    public $console = false;
    public $bar = false;
    protected $servicios = [
        [],
        [1, 2, 3, 4, 5, 6, 7, 9],
        [4, 7, 9],
        [1, 4, 7, 9],
        []
    ];

    protected $meses = [
        'ENERO'     => 1,
        'FEBRERO'   => 2,
        'MARZO'     => 3,
        'ABRIL'     => 4,
        'MAYO'      => 5,
        'JUNIO'     => 6,
        'JULIO'     => 7,
        'AGOSTO'    => 8,
        'SEPTIEMBRE'=> 9,
        'OCTUBRE'   => 10,
        'NOVIEMBRE' => 11,
        'DICIEMBRE' => 12
    ];

    protected $ClientesCategorias = [];
    protected $ClientesCondicionContribuyente = [];
    protected $ClientesRazonSocial = [];
    protected $ClientesRutas = [];
    protected $ClientesCondicion = [];
    protected $Empleados = [];

    protected $i = 0;

    public function progressBar($count) 
    {
        if ($this->console === false) {
            return false;
        }
        $this->bar = $this->console->getOutput()->createProgressBar($count);
        return $this->bar;
    }
    
    public function advance() 
    {
        if ($this->console === false) {
            return false;
        }
       
        return $this->bar->advance();
    }

    public function info($msj) 
    {
        if ($this->console === false) {
            echo $msj . "<br /> \n";
            return false;
        }
        return $this->console->info($msj);
    }

    public function warn($msj) 
    {
        if ($this->console === false) {
            echo $msj . "<br /> \n";
            return false;
        }
        return $this->console->warn($msj);
    }

    public function init($paso)
    {
        $inicio = Carbon::now();
        $this->info("Inicio: " . $inicio->format('d/m/Y H:i:s'));

        $this->_init();
        
        switch ($paso) {
            case '1':
                $this->procesar_clientes();
                break;
            
            case '2':
                $this->procesar_operaciones();
                break;
            
            case '3':
                $this->procesar_administracion();
                break;
            
            default:
                $this->info("Debe escoger una opcion del 1 al 3");
                break;
        }


        $fin = Carbon::now();
        $tiempo = $fin->diffForHumans($inicio);

        $this->info("");
        $this->info("Final: " . $fin->format('d/m/Y H:i:s'));
        $this->info("Tiempo del proceso " . $tiempo);
    }

    public function _init()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        
        //$start_row = $this->argument('start_row');
        $start_row = 11;

        config(['excel.import.startRow' => $start_row ]);

        $this->initVars();
    }

    public function initVars() 
    {
        if (!empty($this->ClientesCategorias)){
            return;
        }

        $this->ClientesCategorias             = $this->definicion(ClientesCategorias::all());
        $this->ClientesCondicionContribuyente = $this->definicion(ClientesCondicionContribuyente::all());
        $this->ClientesRazonSocial            = $this->definicion(ClientesRazonSocial::all());
        $this->ClientesRutas                  = $this->definicion(ClientesRutas::all());
        $this->ClientesCondicion              = $this->definicion(ClientesCondicion::all());
        $this->bancos                         = $this->definicion(EmpresaBancos::all());

        $this->Empleados = [];
        foreach (EmpresaEmpleados::all() as $ele) {
            $this->Empleados[str_slug($ele->nombre)] = $ele->id;
            $this->Empleados[str_slug($ele->nombre . ' ' . $ele->apellido)] = $ele->id;
        }

        $this->Empleados['carlos-vallee']     = 3;
        $this->Empleados['darian']            = 6;
        $this->Empleados['dayannerys']        = 7;
        $this->Empleados['freydimar']         = 10;
        $this->Empleados['jhonatan']          = 14;
        $this->Empleados['stefany-gutierrez'] = 25;
        $this->Empleados['sthefany']          = 25;
        $this->Empleados['stefany-guierrez']  = 25;
        $this->Empleados['yeniree']           = 29;
        $this->Empleados['ysbeth-tomasini']   = 30;
        $this->Empleados['acapulco']          = '';
        $this->Empleados['barbara']           = '';
        $this->Empleados['externo']           = '';
        $this->Empleados['rosa']              = '';
        $this->Empleados['carlos-javier']     = '';
        $this->Empleados['zulay']             = '';
        $this->Empleados['asesoria']          = '';
    }

    public function fecha($fecha, $empresa = null, $mes = null, $ano = null)
    {
        if (is_null($fecha)){
            return null;
        }
        
        $fecha = strtolower(trim($fecha));
        $array = [
            'no aplica',
            '1',
            '111',
            'n/a',
            'falta clave',
            'retirada',
            ',',
            '**',
            'cesada',
            'cliente',
            'correo',
            'borrador',
            'retirado',
            'sin operaciones',
            'sin opercaiones',
            'sin operación',
            'sin operaci�n',
            'sin operacion',
            's/operaciones',
            's/o',
        ];

        if (in_array($fecha, $array) || $fecha == ''){
            return null;
        }
        
        if (preg_match('/(\d{4}-\d{2}-\d{2})/', $fecha, $matchs)) {
            return $matchs[1];
        }

        if (preg_match('/(\d{1,2})[\,\/\-]*(\d{1,2})[\,\/\-]*(\d{2,4})/', $fecha, $matchs)) {
            if (strlen($matchs[3]) == 2) {
                $matchs[3] = '20' . $matchs[3];
            }

            $dia = $matchs[1] = intval($matchs[1]);
            $mes = $matchs[2] = intval($matchs[2]);
            $ano = $matchs[3] = intval($matchs[3]);

            if ($mes > 12 && $dia <= 12) {
                $dia = $matchs[2];
                $mes = $matchs[1];
            }

            if ($ano == 201 || $ano == 207 || $ano == 217){
                $ano = 2017;
            }
            
            if ($dia == 0 || $mes == 0 || $ano == 0 || 
                $ano > 2017 || $mes > 12 || $dia > 31){
                return null;
            }

            if ($ano < 2000) {
                $ano = 2017;
            }

            if ($dia < 10){
                $dia = '0' . $dia;
            }

            if ($mes < 10){
                $mes = '0' . $mes;
            }

            $fecha = $ano . '-' . $mes . '-' . $dia;
        } elseif (preg_match('/(\d{1,2})[\,\/\-]*(\d{1,2})[\,\/\-]*(\d{1,2})/', $fecha, $matchs)) {
            $this->info($matchs[0]);
            if ($matchs[3] == '17') {
                $matchs[3] = '2017';
            }

            $dia = $matchs[1] = intval($matchs[1]);
            $mes = $matchs[2] = intval($matchs[2]);
            $ano = $matchs[3] = intval($matchs[3]);

            if ($mes > 12 && $dia <= 12) {
                $dia = $matchs[2];
                $mes = $matchs[1];
            }

            if ($dia == 0 || $mes == 0 || $ano == 0 || 
                $ano > 2017 || $mes > 12 || $dia > 31){
                return null;
            }

            if ($ano < 2000) {
                $ano = 2017;
            }

            if ($dia < 10){
                $dia = '0' . $dia;
            }

            if ($mes < 10){
                $mes = '0' . $mes;
            }

            $fecha = $ano . '-' . $mes . '-' . $dia;
        } else {
            $this->errores[] = "Fecha: $fecha; Empresa: $empresa; Mes: $mes; Ano: $ano;\n";
            $fecha = null;
        }
        
        return $fecha;
    }

    protected function nombreEmpresa($nombre)
    {
        $nombre = trim($nombre);
        $nombre = trim($nombre, '.');
        $nombre = preg_replace('/\s+/', ' ', $nombre);
        
        if ($nombre === 'GIFT SHOP CESE DEFINITIVO AL 31/12/2016') {
            return null;
        }

        if($nombre == 'Buerger Guayana I, C.A'){
            $nombre = 'Burguer Guayana I, C.A.';
        }

        if($nombre == 'Buerger Guayana II, C.A'){
            $nombre = 'Burguer Guayana II, C.A.';
        }

        if($nombre == 'Buerger Guayana III, C.A'){
            $nombre = 'Burguer Guayana III, C.A.';
        }

        if($nombre == 'Calza Sport PuntoCom, C.A.' || $nombre == 'Calza Sport PuntoCom, C.A'){
            $nombre = 'Calza Sport.com, C.A.';
        }

        if($nombre == 'Extintores Continental,S.R.L'){
            $nombre = 'Extintores Continental, S.R.L.';
        }
        if($nombre == 'Creaciones e Inversiones, L $ L, C.A'){
            $nombre = 'Creaciones e Inversiones, L & L, C.A.';
        }

        return $nombre;
    }

    protected function truncar($tablas) 
    {
        //DB::statement('SET foreign_key_checks = 0;');
        foreach ($tablas as $tabla) {
            DB::statement("TRUNCATE $tabla CASCADE;");
        }
        //DB::statement('SET foreign_key_checks = 1;');
    }

    protected function definicion($lista)
    {
        $res = [];
        foreach ($lista as $ele) {
            $res[str_slug($ele->nombre)] = $ele->id;
        }

        return $res;
    }

    public function procesar_clientes()
    {
        $this->info("");
        $this->info("Proceso del Creacion de Clientes");
        
        $this->truncar([
            'clientes',
            'servicios_clientes'
        ]);

        //dd($ClientesCategorias, $ClientesCondicionContribuyente, $ClientesRazonSocial, $ClientesRutas);
        
        Excel::load('storage/app/base1.xlsx', function($reader) {
            $reader->setDateFormat('Y-m-d');
            foreach($reader as $sheet) {
                $this->i = 0;
                $this->progressBar($sheet->count());
                
                list($mes, $ano) = explode(' ', $sheet->getTitle());
                $mes_id = $this->meses[strtoupper($mes)];
                $ano = intval($ano);

                if ($mes_id < 5){
                    //continue;
                }

                $this->info("");
                $this->info("Procesando el mes de " . $mes);
                //$this->info("");

                DB::beginTransaction();
                foreach ($reader->get() as $linea) {
                    $this->guardar_clientes($linea);
                    $this->i++;
                    $this->advance();
                }

                DB::commit();
            }
        });
    }

    protected function guardar_clientes($linea)
    {
        try {
            $nombre_empresa = $linea['nombre_o_razon_social_del_cliente'];
            $nombre_empresa = $this->nombreEmpresa($nombre_empresa);

            if(is_null($nombre_empresa) || $nombre_empresa == ''){
                return;
            }
            
            $slug = str_slug($nombre_empresa);

            $Cliente = Clientes::where('slug', $slug)->first();
            
            $data = [
                'nombre'                              => $nombre_empresa,
                'slug'                                => $slug,
                'direccion'                           => is_null($linea['direccion_fiscal']) ? '' : $linea['direccion_fiscal'],
                'clientes_categorias_id'              => str_slug($linea['categoria']),
                'clientes_rutas_id'                   => str_slug($linea['ruta']),
                'clientes_condicion_contribuyente_id' => str_slug($linea['condicion_del_contribuyente']),
                'clientes_razon_social_id'            => str_slug($linea['razon_social']),
                'clientes_condicion_id'               => str_slug($linea['condicion_en_la_firma']),
                'supervisor'                          => str_slug($linea['socio_responsable_por_contador']),
                'jefe_departamento'                   => str_slug($linea['contador']),
                'asistente'                           => str_slug($linea['asistente_contable']),
            ];

            if (isset($this->ClientesCategorias[$data['clientes_categorias_id']])) {
                $data['clientes_categorias_id'] = $this->ClientesCategorias[$data['clientes_categorias_id']];
            } else {
                if ($data['clientes_categorias_id'] != ''){
                    $this->warn('clientes_categorias_id: ' . $data['clientes_categorias_id']);
                }
                $data['clientes_categorias_id'] = null;
            }

            if (isset($this->ClientesCondicionContribuyente[$data['clientes_condicion_contribuyente_id']])) {
                $data['clientes_condicion_contribuyente_id'] = $this->ClientesCondicionContribuyente[$data['clientes_condicion_contribuyente_id']];
            } else {
                if ($data['clientes_condicion_contribuyente_id'] != ''){
                    $this->warn('clientes_condicion_contribuyente_id: ' . $data['clientes_condicion_contribuyente_id']);
                }
                $data['clientes_condicion_contribuyente_id'] = null;
            }

            if (isset($this->ClientesRazonSocial[$data['clientes_razon_social_id']])) {
                $data['clientes_razon_social_id'] = $this->ClientesRazonSocial[$data['clientes_razon_social_id']];
            } else {
                if ($data['clientes_razon_social_id'] != ''){
                    $this->warn('clientes_razon_social_id: ' . $data['clientes_razon_social_id']);
                }
                $data['clientes_razon_social_id'] = null;
            }
            
            switch ($data['clientes_rutas_id']) {
                case 'centro':
                    $data['clientes_rutas_id'] = 'centro-1';
                    break;
                
                case 'paseo-meneses':
                    $data['clientes_rutas_id'] = 'paseo-meneses-1';
                    break;

                case 'avenida-nueva-granada':
                    $data['clientes_rutas_id'] = 'avenida-nueva-granada-1';
                    break;
            }

            if (isset($this->ClientesRutas[$data['clientes_rutas_id']])) {
                $data['clientes_rutas_id'] = $this->ClientesRutas[$data['clientes_rutas_id']];
            } else {
                if ($data['clientes_rutas_id'] != ''){
                    $this->warn('clientes_rutas_id: ' . $data['clientes_rutas_id']);
                }
                $data['clientes_rutas_id'] = null;
            }

            switch ($data['clientes_condicion_id']) {
                case 'asesoria':
                    $data['clientes_condicion_id'] = 'asesorias';
                    break;
                case 'soperaciones':
                    $data['clientes_condicion_id'] = '';
                    break;
            }

            $res = preg_match('/(\d{1,2})[\,\/\-]*(\d{1,2})/', $data['clientes_condicion_id']);
            if ($res) {
                $data['clientes_condicion_id'] = '';
            }
            
            if (isset($this->ClientesCondicion[$data['clientes_condicion_id']])) {
                $data['clientes_condicion_id'] = $this->ClientesCondicion[$data['clientes_condicion_id']];
            } else {
                if ($data['clientes_condicion_id'] != ''){
                    $this->warn('clientes_condicion_id: ' . $data['clientes_condicion_id']);
                }
                $data['clientes_condicion_id'] = null;
            }

            if (isset($this->Empleados[$data['supervisor']])) {
                $data['supervisor'] = $this->Empleados[$data['supervisor']];
            } else {
                if ($data['supervisor'] != ''){
                    $this->warn('supervisor: ' . $data['supervisor']);
                }
                $data['supervisor'] = null;
            }

            if (isset($this->Empleados[$data['jefe_departamento']])) {
                $data['jefe_departamento'] = $this->Empleados[$data['jefe_departamento']];
            } else {
                if ($data['jefe_departamento'] != ''){
                    $this->warn('jefe_departamento: ' . $data['jefe_departamento']);
                }
                $data['jefe_departamento'] = null;
            }

            if (isset($this->Empleados[$data['asistente']])) {
                $data['asistente'] = $this->Empleados[$data['asistente']];
            } else {
                if ($data['asistente'] != ''){
                    $this->warn('asistente: ' . $data['asistente']);
                }
                $data['asistente'] = null;
            }

            foreach ($data as $key => $value) {
                if ($key == 'direccion') {
                    continue;
                }
                if ($value == '') {
                    $data[$key] = null;
                }
            }

            $inventario = $linea['servicio_de_inventario'];
            if ($Cliente) {
                if ($Cliente->clientes_condicion_contribuyente_id != $data['clientes_condicion_contribuyente_id']) {
                    $this->proceso_servicios($Cliente, $inventario);
                }
                $Cliente->update($data);
            } else {
                $Cliente = Clientes::create($data);

                $this->proceso_servicios($Cliente, $inventario);
            }
        } catch (Illuminate\Database\QueryException $e) {
            dd($data);
            DB::rollback();
            $this->warn($e->getMessage());
        } catch (Exception $e) {
            dd($data);
            DB::rollback();
            $this->warn($e->errorInfo[2]);
        }

        return $Cliente;
    }

    protected function proceso_servicios($Cliente, $inventario)
    {
        ServiciosClientes::where('clientes_id', $Cliente->id)->delete();
        if (is_null($Cliente->clientes_condicion_contribuyente_id)){
            return;
        }
        
        foreach ($this->servicios[$Cliente->clientes_condicion_contribuyente_id] as $servicio_id) {
            ServiciosClientes::create([
                'servicios_id' => $servicio_id,
                'clientes_id' => $Cliente->id
            ]);
        }
        if ($inventario != 'NO APLICA' && $inventario != '') {
            ServiciosClientes::create([
                'servicios_id' => 8, // el id de inventario
                'clientes_id' => $Cliente->id
            ]);
        }
    }


    public function procesar_operaciones()
    {
        /*
        $this->truncar([
            'clientes_historico',
            'servicios_contabilidad',
            'servicios_forma_18_1',
            'servicios_forma_18_2',
            'servicios_forma_21_1',
            'servicios_forma_21_2',
            'servicios_forma_22',
            'servicios_forma_30',
            'servicios_forma_74',
            'servicios_forma_35_1',
            'servicios_forma_35_2',
            'servicios_inventario',
            'servicios_libros',
            'servicios_documentos_entregados'
        ]);
        */

        $campos_fecha = [
            'forma_74_recibido_admon',
            'forma_74_recibido_oper',
            'forma_74_proceso',
            'forma_74_revision_declarado',
            'forma_74_entrega_admon',

            'forma_35_1_recibido_admon',
            'forma_35_1_recibido_oper',
            'forma_35_1_proceso',
            'forma_35_1_revision_declarado',
            'forma_35_1_entrega_admon',

            'forma_35_2_recibido_admon',
            'forma_35_2_recibido_oper',
            'forma_35_2_proceso',
            'forma_35_2_revision_declarado',
            'forma_35_2_entrega_admon',

            'forma_18_1_recibido_admon',
            'forma_18_1_recibido_oper',
            'forma_18_1_proceso',
            'forma_18_1_revision_declarado',
            'forma_18_1_entrega_admon',
            
            'forma_18_2_recibido_admon',
            'forma_18_2_recibido_oper',
            'forma_18_2_procesoe',
            'forma_18_2_revision_declarado',
            'forma_18_2_entrega_admon',

            'forma_22_recibido_admon',
            'forma_22_recibido_oper',
            'forma_22_proceso',
            'forma_22_revision_declarado',
            'forma_22_entrega_admon',

            'forma_30_proceso',
            'forma_30_revision_declarado',
            'forma_30_registro',
            'forma_30_entrega_admon',

            'forma_21_1_recibido_admon',
            'forma_21_1_recibido_oper',
            'forma_21_1_proceso',
            'forma_21_1_revision_declarado',
            'forma_21_1_entrega_admon',

            'forma_21_2_recibido_admon',
            'forma_21_2_recibido_oper',
            'forma_21_2_proceso',
            'forma_21_2_revision_declarado',
            'forma_21_2_entrega_admon',

            'contabilidad_proceso',
            'contabilidad_revision',
            'contabilidad_entrega',
            'inventario_retorno_contabilidad',
            'inventario_proceso_inventario',
            'inventario_retorno_deposito_archivo',
            'inventario_entrega_admon'

        ];

        $this->info("");
        $this->info("Proceso del Archivo de operaciones");
        
        Excel::load('storage/app/base1.xlsx', function($reader) use ($campos_fecha) {
            $reader->formatDates(false);
            //$reader->setDateFormat('Y-m-d');
            
            foreach($reader->get() as $sheet) {
                $this->i = 0;
                $this->progressBar($sheet->count());
                
                list($mes, $ano) = explode(' ', $sheet->getTitle());
                $mes_id = $this->meses[strtoupper($mes)];
                $ano = intval($ano);

                if ($mes_id != 6) {
                    //continue;
                }  

                $this->info("");
                $this->info("Procesando el mes de " . $mes);
                $this->info("");

                DB::beginTransaction();
                foreach ($sheet as $linea) {
                    if(!isset($linea['nombre_o_razon_social_del_cliente']) || 
                        is_null($linea['nombre_o_razon_social_del_cliente'])){
                        $this->i++;
                        $this->advance();
                        continue;
                    }

                    $nombre_empresa = $linea['nombre_o_razon_social_del_cliente'];
                    $nombre_empresa = $this->nombreEmpresa($nombre_empresa);

                    if (is_null($nombre_empresa)) {
                        continue;
                    }

                    $cliente = $this->guardar_clientes($linea);

                    try {
                        foreach ($linea as $key => $value) {
                            if (in_array($key, $campos_fecha)) {
                                $linea[$key] = $this->fecha($value, $nombre_empresa, $mes_id, $ano);
                            }
                        }
                        //dd($linea);

                        //$contenido = json_encode($linea);
                        //Storage::disk('local')->append('import_log.txt', $contenido);
                        $ClientesHistorico = ClientesHistorico::updateOrCreate([
                                'mes' => $mes_id,
                                'ano' => $ano,
                                'clientes_id' => $cliente->id,
                            ],
                            [
                                'clientes_categorias_id'              => $cliente->clientes_categorias_id,
                                'clientes_rutas_id'                   => $cliente->clientes_rutas_id,
                                'supervisor'                          => $cliente->supervisor,
                                'jefe_departamento'                   => $cliente->jefe_departamento,
                                'asistente'                           => $cliente->asistente,
                            ]);

                        $documentos_entregados = [
                            'entrega'               => 'n',
                            'forma_74'              => @is_null($linea['forma_74_recibido_admon']) ? 'n':   's',
                            'forma_30'              => @is_null($linea['forma_30_recibido_admon']) ? 'n':   's',
                            'forma_35_1'            => @is_null($linea['forma_35_1_recibido_admon']) ? 'n': 's',
                            'forma_35_2'            => @is_null($linea['forma_35_2_recibido_admon']) ? 'n': 's',
                            'forma_21_1'            => @is_null($linea['forma_21_1_recibido_admon']) ? 'n': 's',
                            'forma_21_2'            => @is_null($linea['forma_21_2_recibido_admon']) ? 'n': 's',
                            'forma_18_1'            => @is_null($linea['forma_18_1_recibido_admon']) ? 'n': 's',
                            'forma_18_2'            => @is_null($linea['forma_18_2_recibido_admon']) ? 'n': 's',
                            'forma_22'              => @is_null($linea['forma_22_recibido_admon']) ? 'n':   's',
                            'inventario'            => 'n',
                            'libros'                => 'n',
                            'libros_compras_ventas' => 'n',
                            'contabilidad'          => 'n',
                        ];

                        $documentos_entregados['entrega'] = in_array('s', $documentos_entregados) ? 's' : 'n';
                        
                        ServiciosDocumentosEntregados::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            $documentos_entregados);
                        
                        ServiciosForma74::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'          => $linea['forma_74_recibido_admon'],
                                'recibido_oper'           => $linea['forma_74_recibido_oper'],
                                'proceso'                 => $linea['forma_74_proceso'],
                                'revision_declarado'      => $linea['forma_74_revision_declarado'],
                                'entrega_admon'           => $linea['forma_74_entrega_admon'],
                            ]);
                        
                        ServiciosForma30::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'          => $linea['forma_74_recibido_admon'],
                                'recibido_oper'           => $linea['forma_74_recibido_oper'],
                                'proceso'                 => $linea['forma_30_proceso'],
                                'revision_declarado'      => $linea['forma_30_revision_declarado'],
                                'entrega_admon'           => $linea['forma_30_entrega_admon'],
                                'registro'                => $linea['forma_30_registro'],
                            ]);


                        ServiciosForma351::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'        => $linea['forma_35_1_recibido_admon'],
                                'recibido_oper'         => $linea['forma_35_1_recibido_oper'],
                                'proceso'               => $linea['forma_35_1_proceso'],
                                'revision_declarado'    => $linea['forma_35_1_revision_declarado'],
                                'entrega_admon'         => $linea['forma_35_1_entrega_admon'],
                            ]);

                        ServiciosForma352::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'            => $linea['forma_35_2_recibido_admon'],
                                'recibido_oper'             => $linea['forma_35_2_recibido_oper'],
                                'proceso'                   => $linea['forma_35_2_proceso'],
                                'revision_declarado'        => $linea['forma_35_2_revision_declarado'],
                                'entrega_admon'             => $linea['forma_35_2_entrega_admon'],
                            ]);

                        ServiciosForma211::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'        => $linea['forma_21_1_recibido_admon'],
                                'recibido_oper'         => $linea['forma_21_1_recibido_oper'],
                                'proceso'               => $linea['forma_21_1_proceso'],
                                'revision_declarado'    => $linea['forma_21_1_revision_declarado'],
                                'entrega_admon'         => $linea['forma_21_1_entrega_admon'],
                            ]);

                        ServiciosForma212::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'        => $linea['forma_21_2_recibido_admon'],
                                'recibido_oper'         => $linea['forma_21_2_recibido_oper'],
                                'proceso'               => $linea['forma_21_2_proceso'],
                                'revision_declarado'    => $linea['forma_21_2_revision_declarado'],
                                'entrega_admon'         => $linea['forma_21_2_entrega_admon'],
                            ]);

                        ServiciosForma181::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'        => $linea['forma_18_1_recibido_admon'],
                                'recibido_oper'         => $linea['forma_18_1_recibido_oper'],
                                'proceso'               => $linea['forma_18_1_proceso'],
                                'revision_declarado'    => $linea['forma_18_1_revision_declarado'],
                                'entrega_admon'         => $linea['forma_18_1_entrega_admon'],
                            ]);

                        ServiciosForma182::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'        => $linea['forma_18_2_recibido_admon'],
                                'recibido_oper'         => $linea['forma_18_2_recibido_oper'],
                                'proceso'               => $linea['forma_18_2_procesoe'],
                                'revision_declarado'    => $linea['forma_18_2_revision_declarado'],
                                'entrega_admon'         => $linea['forma_18_2_entrega_admon'],
                            ]);

                        ServiciosForma22::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'recibido_admon'        => $linea['forma_22_recibido_admon'],
                                'recibido_oper'         => $linea['forma_22_recibido_oper'],
                                'proceso'               => $linea['forma_22_proceso'],
                                'revision_declarado'    => $linea['forma_22_revision_declarado'],
                                'entrega_admon'         => $linea['forma_22_entrega_admon'],
                            ]);

                        ServiciosContabilidad::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                            'proceso'               => $linea['contabilidad_proceso'],
                            'revision'              => $linea['contabilidad_revision'],
                            'entrega'               => $linea['contabilidad_entrega'],
                        ]);
                    } catch (Illuminate\Database\QueryException $e) {
                        //DB::rollback();
                        $this->warn($e->getMessage());
                    } catch (Exception $e) {
                        //DB::rollback();
                        $this->warn($e->errorInfo[2]);
                    }
                    $this->i++;
                    $this->advance();
                }
                
                DB::commit();
            }
        });

        foreach ($this->errores as $error) {
            //$this->warn($error);
        }
    }


    public function procesar_administracion()
    {
        $this->errores = [];

        $campos_fecha = [
            'forma_74_regular_pago_planilla',
            'forma_74_comp_pago_planilla',
            'forma_74_entrega',
            'forma_74_entrega_comunicacion',

            'forma_30_regular_pago_planilla',
            'forma_30_susti_pago_planilla',
            'forma_30_entrega',
            'forma_30_entrega_comunicacion',

            'forma_35_1_pago_planilla',
            'forma_35_1_entrega',
            'forma_35_1_entrega_comunicacion',

            'forma_35_2_pago_planilla',
            'forma_35_2_entrega',
            'forma_35_2_entrega_comunicacion',

            'forma_18_1_pago_planilla',
            'forma_18_1_entrega',
            'forma_18_1_entrega_comunicacion',

            'forma_18_2_pago_planilla',
            'forma_18_2_entrega',
            'forma_18_2_entrega_comunicacion',

            'forma_21_1_pago_planilla',
            'forma_21_1_entrega',
            'forma_21_1_entrega_comunicacion',

            'forma_21_2_pago_planilla',
            'forma_21_2_entrega',
            'forma_21_2_entrega_comunicacion',

            'forma_22_pago_planilla',

            'inventario_envio',
            'inventario_entrega',
            'inventario_entrega_comunicacion',

            'contabilidad_entrega',
            'contabilidad_entrega_comunicacion',

            'libros_legales_entrega_admon',
            'libros_legales_entrega',
            'libros_legales_entrega_comunicacion',

            'libros_entrega_admon',
            'libros_entrega',
            'libros_entrega_comunicacion',
            'inventario_entrega_admon',
        ];

        $campos_banco = [
            'forma_74_regular_bancos_id',
            'forma_74_comp_bancos_id',
            'forma_30_regular_bancos_id',
            'forma_30_susti_bancos_id',
            'forma_35_1_bancos_id',
            'forma_35_2_bancos_id',
            'forma_21_1_bancos_id',
            'forma_21_2_bancos_id',
            'forma_18_1_bancos_id',
            'forma_18_2_bancos_id',
            'forma_22_bancos_id',
        ];

        $this->info("");
        $this->info("Proceso del Archivo de Administración");
        
        Excel::load('storage/app/base2.xlsx', function($reader) use ($campos_fecha,  $campos_banco) {
            $reader->formatDates(false);
            //$reader->setDateFormat('Y-m-d');
            
            foreach($reader->get() as $sheet) {
                //$sheet->protect('8876489');
                $this->i = 0;
                $this->progressBar($sheet->count());
                
                list($mes, $ano) = explode(' ', $sheet->getTitle());
                $mes_id = $this->meses[strtoupper($mes)];
                $ano = intval($ano);

                if ($mes_id != 1) {
                    //continue;
                }  

                $this->info("");
                $this->info("Procesando el mes de " . $mes);
                $this->info("");

                DB::beginTransaction();
                foreach ($sheet as $linea) {
                    /*
                    if ($mes_id <= 4 || $this->i < 285) {
                        $this->i++;
                        $this->advance();
                        continue;
                    }
                    */

                    if(!isset($linea['nombre_o_razon_social_del_cliente']) || 
                        is_null($linea['nombre_o_razon_social_del_cliente'])){
                        $this->i++;
                        $this->advance();
                        continue;
                    }

                    $nombre_empresa = $linea['nombre_o_razon_social_del_cliente'];
                    $nombre_empresa = $this->nombreEmpresa($nombre_empresa);

                    if (is_null($nombre_empresa)) {
                        continue;
                    }

                    try {
                        $cliente = Clientes::select([
                            'id',
                            'nombre'
                        ])
                        ->where('slug', str_slug($nombre_empresa))
                        ->first();

                        if (!$cliente) {
                            $cliente = $this->guardar_clientes($linea);
                        }

                        $ClientesHistorico = ClientesHistorico::updateOrCreate(
                            [
                                'mes' => $mes_id,
                                'ano' => $ano,
                                'clientes_id' => $cliente->id,
                            ],
                            [
                                'clientes_categorias_id'              => $cliente->clientes_categorias_id,
                                'clientes_rutas_id'                   => $cliente->clientes_rutas_id,
                                'supervisor'                          => $cliente->supervisor,
                                'jefe_departamento'                   => $cliente->jefe_departamento,
                                'asistente'                           => $cliente->asistente,
                            ]);

                        $linea_ini = $linea;

                        //$this->info($linea['nombre_o_razon_social_del_cliente']);

                        foreach ($linea as $key => $campo) {
                            if (in_array($key, $campos_fecha)) {
                                $linea[$key] = $this->fecha($campo, $nombre_empresa, $mes_id, $ano);
                                continue;
                            }

                            $campo = trim(strtolower($campo));
                            if ($campo == '' || $campo == 'no aplica') {
                                $linea[$key] = null;
                                continue;
                            }
                            
                            $linea[$key] = $campo;

                            if (in_array($key, $campos_banco)) {
                                if (is_null($linea[$key])){
                                    continue;
                                }

                                $res = preg_match('/(\d{1,2})[\,\/\-]*(\d{1,2})/', $linea[$key]);
                                if ($res) {
                                    $linea[$key] = '';
                                }

                                // debe ser sin operaciones
                                
                                if ($linea[$key] == 's/operaciones' || 
                                    str_slug($linea[$key]) == 'sin-operaciones' || 
                                    $linea[$key] == 'retirado' || 
                                    $linea[$key] == 'cesada') {
                                    $linea[$key] = null;
                                } elseif ($linea[$key] == 'certificado' || 
                                    $linea[$key] == 'ceretificado' || 
                                    $linea[$key] == 'certficado' || 
                                    $linea[$key] == 'c ertificado' || 
                                    $linea[$key] == 'certifivcado' || 
                                    $linea[$key] == 'cert' || 
                                    $linea[$key] == 'cert/cliente') {
                                    $linea[$key] = 'Certificado';
                                } elseif ($linea[$key] == 'on line') {
                                    $linea[$key] = 'online';
                                } elseif ($linea[$key] == 'cliente' || $linea[$key] == 'cleinte' || $linea[$key] == 'clinte') {
                                    $linea[$key] = 'Cliente';
                                } elseif ($linea[$key] == 'tesor0' || $linea[$key] == 'tesoro' || $linea[$key] == 'cert/tesoro' || $linea[$key] == 'cert-tesoro') {
                                    $linea[$key] = 'Banco del Tesoro, C.A. Banco Universal';
                                } elseif ($linea[$key] == 'bnc') {
                                    $linea[$key] = 'Banco Nacional Crédito, C.A. Banco Universal';
                                } elseif ($linea[$key] == 'exterior') {
                                    $linea[$key] = 'Banco Exterior, C.A. Banco Universal';
                                } elseif ($linea[$key] == 'mercantil' || $linea[$key] == 'mercantil.') {
                                    $linea[$key] = 'Banco Mercantil, C.A. S.A.C.A. Banco Universal';
                                } elseif ($linea[$key] == 'provincial') {
                                    $linea[$key] = 'Banco Provincial, S.A. Banco Universal';
                                } elseif ($linea[$key] == 'venezuela') {
                                    $linea[$key] = 'Banco Provincial, S.A. Banco Universal';
                                } elseif ($linea[$key] == 'banesco') {
                                    $linea[$key] = 'Banesco Banco Universal S.A.C.A.';
                                }

                                if (str_slug($linea[$key]) == '') {
                                    $linea[$key] = null;
                                }

                                if (is_null($linea[$key])) {
                                    continue;
                                }

                                if (isset($this->bancos[str_slug($linea[$key])])) {
                                    $linea[$key] = $this->bancos[str_slug($linea[$key])];
                                } else {
                                    $this->info("");
                                    $this->warn('No existe el banco ' . $linea[$key] . ' en ' . $key . ' slug: ' . str_slug($linea[$key]));
                                    $linea[$key] = null;
                                }
                                continue;
                            }

                            if (ends_with($key, 'numero_planilla') || $key == 'numero_planilla') {
                                if ($campo == 's/operaciones') {
                                    $linea[$key] = null;
                                }
                            }
                            if (ends_with($key, 'monto_cancelado') || $key == 'monto_cancelado') {
                                if (is_null($campo)) {
                                    $linea[$key] = '0.00';
                                    continue;
                                } else {
                                    $campo = trim($campo, '.');
                                    $campo = trim($campo, ',');
                                }


                                $res = preg_match('/^([\d\,\.]+)[\,\.](\d+)$/', $campo, $matchs);

                                if ($res) {
                                    $linea[$key] = str_replace(['.', ','], '', $matchs[1]) . '.' . $matchs[2];
                                } else {
                                    $res = preg_match('/^\d+$/', $campo);
                                    if (!$res) {
                                        $linea[$key] = '0.00';
                                    }
                                }
                            }
                        }
                        

                        $serv = ServiciosForma74::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'regular_numero_planilla' => $linea['forma_74_regular_numero_planilla'],
                                'regular_pago_planilla'   => $linea['forma_74_regular_pago_planilla'],
                                'regular_bancos_id'       => $linea['forma_74_regular_bancos_id'],
                                'regular_monto_cancelado' => $linea['forma_74_regular_monto_cancelado'],
                                'comp_numero_planilla'    => $linea['forma_74_comp_numero_planilla'],
                                'comp_pago_planilla'      => $linea['forma_74_comp_pago_planilla'],
                                'comp_bancos_id'          => $linea['forma_74_comp_bancos_id'],
                                'comp_monto_cancelado'    => $linea['forma_74_comp_monto_cancelado'],
                                'entrega'                 => $linea['forma_74_entrega'],
                                'entrega_comunicacion'    => $linea['forma_74_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosForma30::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'regular_numero_planilla' => $linea['forma_30_regular_numero_planilla'],
                                'regular_pago_planilla'   => $linea['forma_30_regular_pago_planilla'],
                                'regular_bancos_id'       => $linea['forma_30_regular_bancos_id'],
                                'regular_monto_cancelado' => $linea['forma_30_regular_monto_cancelado'],
                                'susti_numero_planilla'   => $linea['forma_30_susti_numero_planilla'],
                                'susti_pago_planilla'     => $linea['forma_30_susti_pago_planilla'],
                                'susti_bancos_id'         => $linea['forma_30_susti_bancos_id'],
                                'susti_monto_cancelado'   => $linea['forma_30_susti_monto_cancelado'],
                                'entrega'                 => $linea['forma_30_entrega'],
                                'entrega_comunicacion'    => $linea['forma_30_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosForma351::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'numero_planilla'      => $linea['forma_35_1_numero_planilla'],
                                'pago_planilla'        => $linea['forma_35_1_pago_planilla'],
                                'bancos_id'            => $linea['forma_35_1_bancos_id'],
                                'monto_cancelado'      => $linea['forma_35_1_monto_cancelado'],
                                'entrega'              => $linea['forma_35_1_entrega'],
                                'entrega_comunicacion' => $linea['forma_35_1_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosForma352::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'numero_planilla'      => $linea['forma_35_2_numero_planilla'],
                                'pago_planilla'        => $linea['forma_35_2_pago_planilla'],
                                'bancos_id'            => $linea['forma_35_2_bancos_id'],
                                'monto_cancelado'      => $linea['forma_35_2_monto_cancelado'],
                                'entrega'              => $linea['forma_35_2_entrega'],
                                'entrega_comunicacion' => $linea['forma_35_2_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosForma211::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'numero_planilla'      => $linea['forma_21_1_numero_planilla'],
                                'pago_planilla'        => $linea['forma_21_1_pago_planilla'],
                                'bancos_id'            => $linea['forma_21_1_bancos_id'],
                                'monto_cancelado'      => $linea['forma_21_1_monto_cancelado'],
                                'entrega'              => $linea['forma_21_1_entrega'],
                                'entrega_comunicacion' => $linea['forma_21_1_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosForma212::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'numero_planilla'      => $linea['forma_21_2_numero_planilla'],
                                'pago_planilla'        => $linea['forma_21_2_pago_planilla'],
                                'bancos_id'            => $linea['forma_21_2_bancos_id'],
                                'monto_cancelado'      => $linea['forma_21_2_monto_cancelado'],
                                'entrega'              => $linea['forma_21_2_entrega'],
                                'entrega_comunicacion' => $linea['forma_21_2_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosForma181::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'numero_planilla'      => $linea['forma_18_1_numero_planilla'],
                                'pago_planilla'        => $linea['forma_18_1_pago_planilla'],
                                'bancos_id'            => $linea['forma_18_1_bancos_id'],
                                'monto_cancelado'      => $linea['forma_18_1_monto_cancelado'],
                                'entrega'              => $linea['forma_18_1_entrega'],
                                'entrega_comunicacion' => $linea['forma_18_1_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosForma182::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'numero_planilla'      => $linea['forma_18_2_numero_planilla'],
                                'pago_planilla'        => $linea['forma_18_2_pago_planilla'],
                                'bancos_id'            => $linea['forma_18_2_bancos_id'],
                                'monto_cancelado'      => $linea['forma_18_2_monto_cancelado'],
                                'entrega'              => $linea['forma_18_2_entrega'],
                                'entrega_comunicacion' => $linea['forma_18_2_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosForma22::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'numero_planilla' => $linea['forma_22_numero_planilla'],
                                'pago_planilla'   => $linea['forma_22_pago_planilla'],
                                'bancos_id'       => $linea['forma_22_bancos_id'],
                                'monto_cancelado' => $linea['forma_22_monto_cancelado'],
                            ]);
                        
                        $serv = ServiciosInventario::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'entrega_admon'        => $linea['inventario_entrega_admon'],
                                'entrega'              => $linea['inventario_entrega'],
                                'entrega_comunicacion' => $linea['inventario_entrega_comunicacion'],
                            ]);
                            
                        $serv = ServiciosLibrosLegales::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'entrega_admon'        => $linea['libros_legales_entrega_admon'],
                                'entrega'              => $linea['libros_legales_entrega'],
                                'entrega_comunicacion' => $linea['libros_legales_entrega_comunicacion'],
                            ]);
                        
                        $serv = ServiciosContabilidad::updateOrCreate(
                            ['clientes_historico_id' => $ClientesHistorico->id],
                            [
                                'entrega'              => $linea['contabilidad_entrega'],
                                'entrega_comunicacion' => $linea['contabilidad_entrega_comunicacion'],
                            ]);
                    } catch (\InvalidArgumentException $e) {
                        //dd($e, $serv);
                        //DB::rollback();
                        $this->warn($e->getMessage());
                    } catch (Illuminate\Database\QueryException $e) {
                        //DB::rollback();
                        $this->warn($e->getMessage());
                    } catch (Exception $e) {
                        //DB::rollback();
                        $this->warn($e->errorInfo[2]);
                    }
                    
                    $this->i++;
                    $this->advance();
                }
                
                DB::commit();
            }
        });

        foreach ($this->errores as $error) {
            //$this->warn($error);
        }
    }
}
