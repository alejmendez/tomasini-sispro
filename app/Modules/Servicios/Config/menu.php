<?php

$menu['servicios'] = [
	[
		'nombre' 	=> 'Servicios',
		'direccion' => '#Servicios',
		'icono' 	=> 'fa fa-wrench',
		'menu' 		=> [
			[
				'nombre' 	=> 'Servicios',
				'direccion' => 'servicios',
				'icono' 	=> 'fa fa-server'
			],
			[
				'nombre' 	=> 'Trabajos',
				'direccion' => 'servicios/trabajos',
				'icono' 	=> 'fa fa-wrench'
			],
			[
				'nombre' 	=> 'Reportes',
				'direccion' => '#ServiciosReportes',
				'icono' 	=> 'fa fa-file-text-o',
				'menu' 		=> [
					[
						'nombre' 	=> 'Documentos Entregados',
						'direccion' => 'servicios/reporte/documentos-entregados',
						'icono' 	=> 'fa fa-file-text-o'
					]
				]
			]
		]
	]
];