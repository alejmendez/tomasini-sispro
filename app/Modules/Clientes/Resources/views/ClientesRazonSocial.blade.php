@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Clientes Razon Social']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar ClientesRazonSocial.',
        'columnas' => [
            'Nombre' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $ClientesRazonSocial->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection