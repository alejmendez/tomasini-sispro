<?php

namespace App\Modules\Servicios\Models;

use Illuminate\Database\Eloquent\Model;

class ServiciosClientes extends Model
{
    protected $table = 'servicios_clientes';
    protected $fillable = ['servicios_id','clientes_id'];

    protected $primaryKey = null;
    public $incrementing = false;

    public $timestamps = false;

    public function servicios()
	{
		return $this->belongsTo('App\Modules\Servicios\Models\Servicios');
	}

	public function clientes()
	{
		return $this->belongsTo('App\Modules\Clientes\Models\Clientes');
	}
}