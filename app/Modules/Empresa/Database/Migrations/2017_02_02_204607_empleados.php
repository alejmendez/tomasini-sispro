<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Empleados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_empleados', function(Blueprint $table){
            $table->increments('id');
            
            $table->char('nacionalidad', 1);
            $table->integer('dni')->unsigned()->unique();

            $table->string('nombre', 100);
            $table->string('apellido', 100)->nullable();

            $table->string('correo', 50)->nullable()->unique();
            $table->string('telefono', 15)->nullable();
            $table->string('telefono_movil', 15)->nullable();
            $table->date('nacimiento')->nullable();

            $table->char('genero', 1)->nullable();
            $table->string('edo_civil',2)->nullable();  
            $table->string('domicilio', 200)->nullable();
            
            $table->integer('empresa_estructura_id')->unsigned();
            $table->integer('empresa_cargos_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('empresa_cargos_id')
                ->references('id')->on('empresa_cargos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('empresa_estructura_id')
                ->references('id')->on('empresa_estructura')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_empleados');
    }
}
