<?php

namespace App\Modules\Servicios\Http\Controllers;

//Controlador Padre
use App\Modules\Servicios\Http\Controllers\Controller;

//Dependencias
use DB;
use Carbon\Carbon;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Servicios\Http\Requests\TrabajosRequest;

//Modelos

use App\Modules\Empresa\Models\EmpresaEmpleados;

use App\Modules\Clientes\Models\Clientes;

use App\Modules\Servicios\Models\ClientesHistorico;

use App\Modules\Servicios\Models\ServiciosDocumentosEntregados;


class ReporteDocumentosController extends Controller
{
    protected $titulo = 'Trabajos';

    public $js = [
        'ReporteDocumentos'
    ];
    
    public $css = [
        'ReporteDocumentos'
    ];

    public $librerias = [
        'datatables',
        'bootstrap-datepicker'
    ];

    public function index()
    {
        return $this->view('servicios::ReporteDocumentos');
    }

    public function meses()
    {
        return [
            1 => 'ENERO',
            'FEBRERO',
            'MARZO',
            'ABRIL',
            'MAYO',
            'JUNIO',
            'JULIO',
            'AGOSTO',
            'SEPTIEMBRE',
            'OCTUBRE',
            'NOVIEMBRE',
            'DICIEMBRE'
        ];
    }

    public function datatable(Request $request){
        $sql = Clientes::select([
            'clientes.nombre', 
            'servicios_documentos_entregados.entrega',
            'servicios_documentos_entregados.forma_74',
            'servicios_documentos_entregados.forma_30',
            'servicios_documentos_entregados.forma_35_1',
            'servicios_documentos_entregados.forma_35_2',
            'servicios_documentos_entregados.forma_21_1',
            'servicios_documentos_entregados.forma_21_2',
            'servicios_documentos_entregados.forma_18_1',
            'servicios_documentos_entregados.forma_18_2',
            'servicios_documentos_entregados.forma_22',
            'servicios_documentos_entregados.inventario',
            'servicios_documentos_entregados.libros',
            'servicios_documentos_entregados.libros_compras_ventas',
            'servicios_documentos_entregados.contabilidad'
        ])
        ->leftJoin('clientes_condicion_contribuyente', 'clientes_condicion_contribuyente.id', '=', 'clientes.clientes_condicion_contribuyente_id')
        ->leftJoin('clientes_historico', [
            ['clientes.id', '=', 'clientes_historico.clientes_id'],
            ['clientes_historico.mes', '=', DB::raw($request->get('mes'))],
            ['clientes_historico.ano', '=', DB::raw($request->get('ano'))]
        ])
        ->leftJoin('servicios_documentos_entregados', 'servicios_documentos_entregados.clientes_historico_id', '=', 'clientes_historico.id');

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}