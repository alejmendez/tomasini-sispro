<?php

namespace App\Modules\Servicios\Http\Requests;

use App\Http\Requests\Request;

class ServiciosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100']
	];
}