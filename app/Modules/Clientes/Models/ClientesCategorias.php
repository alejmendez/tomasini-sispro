<?php

namespace App\Modules\Clientes\Models;

use App\Modules\Base\Models\Modelo;

class ClientesCategorias extends modelo
{
    protected $table = 'clientes_categorias';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Clientes Categorias'
        ]
    ];
}