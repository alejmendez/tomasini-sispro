<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocumentosEntregados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_documentos_entregados', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->char('entrega', 1)->nullable();
            $table->char('forma_74', 1)->nullable();
            $table->char('forma_30', 1)->nullable();
            $table->char('forma_22', 1)->nullable();
            $table->char('forma_18_1', 1)->nullable();
            $table->char('forma_18_2', 1)->nullable();
            $table->char('forma_21_1', 1)->nullable();
            $table->char('forma_21_2', 1)->nullable();
            $table->char('forma_35_1', 1)->nullable();
            $table->char('forma_35_2', 1)->nullable();
            $table->char('inventario', 1)->nullable();
            $table->char('libros', 1)->nullable();
            $table->char('libros_compras_ventas', 1)->nullable();
            $table->char('contabilidad', 1)->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_documentos_entregados');
    }
}
