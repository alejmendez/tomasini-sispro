<?php

namespace App\Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class EmpresaCargosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100']
	];
}