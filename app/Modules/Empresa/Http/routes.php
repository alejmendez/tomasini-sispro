<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix') . '/empresa', 'namespace' => 'App\\Modules\Empresa\Http\Controllers'], function()
{
    Route::group(['prefix' => 'bancos'], function() {
		Route::get('/', 				'EmpresaBancosController@index');
		Route::get('nuevo', 			'EmpresaBancosController@nuevo');
		Route::get('cambiar/{id}', 		'EmpresaBancosController@cambiar');
		
		Route::get('buscar/{id}', 		'EmpresaBancosController@buscar');

		Route::post('guardar',			'EmpresaBancosController@guardar');
		Route::put('guardar/{id}', 		'EmpresaBancosController@guardar');

		Route::delete('eliminar/{id}', 	'EmpresaBancosController@eliminar');
		Route::post('restaurar/{id}', 	'EmpresaBancosController@restaurar');
		Route::delete('destruir/{id}', 	'EmpresaBancosController@destruir');

		Route::get('datatable', 		'EmpresaBancosController@datatable');
	});


	Route::group(['prefix' => 'cargos'], function() {
		Route::get('/', 				'EmpresaCargosController@index');
		Route::get('nuevo', 			'EmpresaCargosController@nuevo');
		Route::get('cambiar/{id}', 		'EmpresaCargosController@cambiar');
		
		Route::get('buscar/{id}', 		'EmpresaCargosController@buscar');

		Route::post('guardar',			'EmpresaCargosController@guardar');
		Route::put('guardar/{id}', 		'EmpresaCargosController@guardar');

		Route::delete('eliminar/{id}', 	'EmpresaCargosController@eliminar');
		Route::post('restaurar/{id}', 	'EmpresaCargosController@restaurar');
		Route::delete('destruir/{id}', 	'EmpresaCargosController@destruir');

		Route::get('datatable', 		'EmpresaCargosController@datatable');
	});


	Route::group(['prefix' => 'empleados'], function() {
		Route::get('/', 				'EmpresaEmpleadosController@index');
		Route::get('nuevo', 			'EmpresaEmpleadosController@nuevo');
		Route::get('cambiar/{id}', 		'EmpresaEmpleadosController@cambiar');
		
		Route::get('buscar/{id}', 		'EmpresaEmpleadosController@buscar');

		Route::post('guardar',			'EmpresaEmpleadosController@guardar');
		Route::put('guardar/{id}', 		'EmpresaEmpleadosController@guardar');

		Route::delete('eliminar/{id}', 	'EmpresaEmpleadosController@eliminar');
		Route::post('restaurar/{id}', 	'EmpresaEmpleadosController@restaurar');
		Route::delete('destruir/{id}', 	'EmpresaEmpleadosController@destruir');

		Route::get('arbol', 			'EmpresaEmpleadosController@arbol');

		Route::get('datatable', 		'EmpresaEmpleadosController@datatable');
	});


	Route::group(['prefix' => 'estructura'], function() {
		Route::get('/', 				'EmpresaEstructuraController@index');
		Route::get('nuevo', 			'EmpresaEstructuraController@nuevo');
		Route::get('cambiar/{id}', 		'EmpresaEstructuraController@cambiar');
		
		Route::get('buscar/{id}', 		'EmpresaEstructuraController@buscar');

		Route::post('guardar',			'EmpresaEstructuraController@guardar');
		Route::put('guardar/{id}', 		'EmpresaEstructuraController@guardar');

		Route::delete('eliminar/{id}', 	'EmpresaEstructuraController@eliminar');
		Route::post('restaurar/{id}', 	'EmpresaEstructuraController@restaurar');
		Route::delete('destruir/{id}', 	'EmpresaEstructuraController@destruir');

		Route::get('arbol', 			'EmpresaEstructuraController@arbol');

		Route::get('datatable', 		'EmpresaEstructuraController@datatable');
	});

    //{{route}}
});
