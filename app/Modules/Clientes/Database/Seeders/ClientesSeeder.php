<?php

namespace App\Modules\Clientes\Database\Seeders;


use DB;
use Illuminate\Database\Seeder;


use App\Modules\Clientes\Models\Clientes;
use App\Modules\Clientes\Models\ClientesCondicion;
use App\Modules\Clientes\Models\ClientesCondicionContribuyente;
use App\Modules\Clientes\Models\ClientesRazonSocial;
use App\Modules\Clientes\Models\ClientesRutas;
use App\Modules\Clientes\Models\CondicionContribuyente;
use App\Modules\Clientes\Models\ClientesCategorias;

use App\Modules\Empresa\Models\EmpresaEmpleados;

use App\Modules\Servicios\Models\ServiciosClientes;


class ClientesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$clientes = [
			['4 Ever, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Cuyuni Edif. Diamante, Piso PB, Loca 1 Urb. Canaima', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['45, C.A.', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['54, C.A.', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['Abasto y Licoreria El Gran Cacique, S.R.L.', '', 'Activos', 'S.R.L.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Inces', 'Avenida Libertador', 'Freidymar', 'Rubén'],
			['Abastos y Charcutería La Ideal Crespo, F.P.', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Casacoima, Edifico Nasser, PB, Local 1, Sector Centurión', 'Paseo Meneses', 'Freidymar', 'Rubén'],
			['Acosta Villarroel Alfredo Jose', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Avenida 17 de Diciembre Clinica Santa Ana', 'Oficina', 'Yenire', 'Dayannery'],
			['Adela Maria Marcano de Malave', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Clinica La Milagrosa', 'Avenida Jesus Soto', 'Mileibys', 'Maite'],
			['Adiktos, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'CC Abou Center', 'Centro', 'Externo', 'Erick'],
			['Administradora Las Banderas, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'CC Las Bnaderas', 'Las Banderas', 'Yenire', 'Rosa'],
			['Agencia de Loterias Marivi, F.P.(Omaira Vasquez)', '', 'Activos', 'F.P.', 'D', 'No Sujeta', 'NO APLICA', 'NO APLICA', '', 'Oficina', 'Oficina', 'Mileibys', 'Jonathan'],
			['Agostinho Silvino Dos Passos', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Stephany', 'Raifer'],
			['Agricola Caño Nuevo 2425, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida República', 'Las Banderas', 'Ninibeth', 'Carlos Javier'],
			['Agropecuaria Canaima 1, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Periferico', 'Periferico', 'Externo', 'Yenire'],
			['Agropecuaria Los Andes 2050, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Av. Paseo Orinoco, Edificio San Antonio, P/B, Local 01, Sector Plaza Las Banderas.', 'Centro', 'Erick', 'Jaqueline'],
			['Aladino Brunch, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Traki', 'La Redoma', 'Iris', 'Stephany Gutierrez'],
			['Alberto Jose De Pacce Luces', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Clinica La Milagrosa', 'Oficina', 'Yenire', 'Dayannery'],
			['Alf, C.A.', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['Alimentos 24, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'CC Las Bnaderas', 'Las Banderas', 'Ninibeth', 'Carlos Javier'],
			['Almacen San Antonio Ciudad Bolívar, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Centro', 'Centro', 'Externo', 'Erick'],
			['Almacen Time Store, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseo Orinico', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Almacenes  Acapulco, C.A', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['Almacenes Olimpico, C.A. ', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Alney\'s, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro', 'Centro', 'Externo', 'Mileibys'],
			['Alta peluqueria Leticia, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Libertad', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Amar Center, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Avenida Sucre', 'Terminal', 'Ninibeth', 'Carlos Javier'],
			['AMS Celular, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'CC Hnos. Liberto', 'Avenida Jesus Soto', 'Mileibys', 'Jonathan'],
			['Amwar David Chehayeb Souki', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Externo', 'Yenire'],
			['Angel Armando Granado Cova.', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Oficina', 'Oficina', 'Mileibys', 'Maite'],
			['Arti - Hogar, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Periferico', 'Periferico', 'Erick', 'Jaqueline'],
			['Asociacion Civil Casa D\' Italia', '', 'Activos', 'A.C.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida San Vicente de Paul', 'Avenida 17 de Diciembre', 'Mileibys', 'Jonathan'],
			['Asociacion Cooperativa Las Banderas Snacks Shop, R.L. ', '', 'Activos', 'R.L.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Las Banderas', 'Las Banderas', 'Yenire', 'Rosa'],
			['Asociacion Cooperativa Transporte El Poder de Dios, R.L.', '', 'Activos', 'R.L.', 'C', 'Formal', 'Mensual', 'NO APLICA', '', 'Avenida Germania', 'Hospital', 'Yenire', 'Dayannery'],
			['Asociacion Co-Propietarios de Residencias Rio Aro II', '', 'Zulay', 'A.C.', 'D', 'No Sujeta', 'NO APLICA', 'NO APLICA', '', 'Avenida San Vicente de Paul', 'Oficina', 'Zulay ', 'Zulay '],
			['Auto Colores Bolivar, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Paseo Gaspari', 'Avenida República', 'Ninibeth', 'Carlos Javier'],
			['Auto Motores Mundial, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'UDO', 'La Sabanita', 'Mileibys', 'Jonathan'],
			['Autocristales, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Republica', 'Avenida República', 'Yenire', 'Rosa'],
			['Autorepuestos Le-Cars C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Nueva Granada', 'Estadio Herés', 'Stephany', 'Raifer'],
			['Autos Principal, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', '', '', 'Freidymar', 'Rubén'],
			['Autos Stefano, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Periferico', 'Periferico', 'Externo', 'Yenire'],
			['Bante Com, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Iris', 'Stephany Gutierrez'],
			['Belkis Tomasini Guillén', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Ninibeth', 'Carlos Javier'],
			['Bio Piel Spa Medico, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Puerto Ordaz', 'Puerto Ordaz', 'Mileibys', 'Jonathan'],
			['Bodegon de Jim, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'La Redoma', 'La Redoma', 'Freidymar', 'Rubén'],
			['Bodegón El Andinito, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Vista Hermosa', 'Estadio Herés', 'Mileibys', 'Jonathan'],
			['Bodegon Lusitano, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Avenida La paragua', 'Avenida Libertador', 'Freidymar', 'Rubén'],
			['Bodegon Sport Players, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Sieger', 'Paseo Meneses', 'Freidymar', 'Rubén'],
			['Bodegon y Festejos JL 2602, C.A.', '', 'Afectado', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenidad Menca de Leoni', 'La Sabanita', 'Ninibeth', 'Carlos Javier'],
			['Brasil Papeles C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Puerto Ordaz', 'Puerto Ordaz', 'Freidymar', 'Rubén'],
			['Buerger Guayana I, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Vista Hermosa, Carrera 4, entre calle 3 y 4, Edf. Angela, Local N° 1 ', 'Estadio Herés', 'Yenire', 'Rosa'],
			['Buerger Guayana II, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Germania Cruce C/Andre Bello, Edf. Don Marco, Local N° 7.', 'Hospital', 'Ninibeth', 'Carlos Javier'],
			['Buerger Guayana III, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseo Moreno de Mendoza, N° 73, Sector Centurion', 'Periferico', 'Stephany', 'Raifer'],
			['Calza Sport.com, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'Erick', 'Jaqueline'],
			['Calzados DO RE MI, C.A', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Avenida Republica', 'Avenida República', 'Freidymar', 'Rubén'],
			['Car´s Center, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'UDO', 'La Sabanita', 'Erick', 'Jaqueline'],
			['Carmen Lucia Piñango Latuff', '', 'Activos', 'PN', 'C', 'Formal', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Yenire', 'Dayannery'],
			['Carnes JB, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Paseo Moreno de Mendoza, Local, N° 1, ', 'Periferico ', 'Yenire', 'Rosa'],
			['Carniceria y Distribuidora El Dividivi, C.A.', '', 'Afectado', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida España', 'La Sabanita', 'Stephany', 'Raifer'],
			['Carniceria, Charcuteria y Viveres La Cumbre, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida San Vicente de Paul', 'Avenida 17 de Diciembre', 'Mileibys', 'Jonathan'],
			['Casa TV, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Paseo Orinico', 'Centro', 'Erick', 'Jaqueline'],
			['Cauchos Cumana, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Cumana', 'Centro', 'Erick', 'Jaqueline'],
			['Centro Comercial Lu Lu, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Jesus Soto', 'Avenida Jesus Soto', 'Externo', 'Mileibys'],
			['Centro de Apuestas All Win, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Colón', 'La Sabanita', 'Ninibeth', 'Carlos Javier'],
			['Centro de Llamadas El Seguro Hamad, F.P.(Imad Nasser)', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Paseo Meneses', 'Paseo Meneses', 'Externo', 'Yenire'],
			['Ceramica Guayana, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Upata', 'La Redoma', 'Stephany', 'Raifer'],
			['Ceramicas Uraima, C.A', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Periferico', 'Periferico', 'Mileibys', 'Jonathan'],
			['Cesar Andres Bello', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Clinica La Milagrosa', 'Avenida Jesus Soto', 'Mileibys', 'Maite'],
			['Charcuteria Angostura II, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Carrera 5, Edificio Minguetti, Planta Baja, Local 3, Vista Hermosa', 'Estadio Herés', 'Freidymar', 'Rubén'],
			['Charcuteria Angostura, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Andres Bello C.C. Naim', 'Hospital', 'Freidymar', 'Rubén'],
			['Chehayeb Souki Faddy Lee', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro', 'Centro', 'Externo', 'Yenire'],
			['Childrens, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Avenida 17 de Diciembre', 'Avenida 17 de Diciembre', 'Externo', 'Erick'],
			['Cinco Hermanos Cars, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Cumana', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Cindusur, S.A.', '', 'Activos', 'S.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Puerto Ordaz', 'Puerto Ordaz', 'Mileibys', 'Jonathan'],
			['Coco\'s Shop, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'Erick', 'Jaqueline'],
			['Comercial Amar, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Erick', 'Jaqueline'],
			['Comercial Gran Caribe, S.R.L', '', 'Activos', 'S.R.L.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Grupo Merida', 'Centro', 'Erick', 'Jaqueline'],
			['Comercial La Nueva Perla, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Comercial Los 7 Hermanos, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Pasaje Triviño', 'Centro', 'Externo', 'Erick'],
			['Comercial Pellegrino, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Cruz Verde', 'Cruz Verde', 'Iris', 'Stephany Gutierrez'],
			['Comercializadora  Chance Tres Bonne, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Las Banderas', 'Las Banderas', 'Yenire', 'Rosa'],
			['Comercializadora Ferpeca, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Periferico', 'Periferico', 'Ninibeth', 'Carlos Javier'],
			['Comercializadora Ferremundo, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'La Vuela del Cacho', 'La sabanita', 'Yenire', 'Rosa'],
			['Comercializadora Frio Master, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Periferico', 'Periferico', 'Erick', 'Jaqueline'],
			['Comercializadora JGR, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Av. Upata, Edificio SARACUSA, P/B. Local 07, Parroquia Catedral, Cd. Bolívar', 'Hospital', 'Iris', 'Stephany Gutierrez'],
			['Construcasa, C.A.', '', 'Afectado', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Los Proceres', 'Los Procesos', 'Yenire', 'Rosa'],
			['Construcciones LMC y Serv. Generales, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Externo', 'Yenire'],
			['Construhierros Bolivar, C.A.', '', 'Asesorias', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Perimetral', 'Oficina', 'Asesorias', 'Asesorias'],
			['Constru-Inversiones Machico, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Terminal', 'Terminal', 'Ninibeth', 'Carlos Javier'],
			['Corporacion Alakay, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Periferico', 'Periferico', 'Externo', 'Mileibys'],
			['Corporacion Caribe, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Cruz Verde', 'Cruz Verde', 'Erick', 'Jaqueline'],
			['Creaciones e Inversiones, L $ L, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Vennezuela CC Bolívar Plaza, Nivel 1, Local 13', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Crystal Cell, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro Comercial Crytal Shoping', 'Paseo Meneses', 'Mileibys', 'Jonathan'],
			['Cyber Plus, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro', 'Centro', 'Erick', 'Jaqueline'],
			['Dali, F.P.(Bitar De Al Dali Naziha).', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Paseo Meneses', 'Paseo Meneses', 'Mileibys', 'Jonathan'],
			['De Colores Boutique C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Centro Comercial Las Banderas', 'Las Banderas', 'Ninibeth', 'Carlos Javier'],
			['De Colores, C.A. ', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Centro Comercial Florida', 'Avenida 17 de Diciembre', 'Mileibys', 'Jonathan'],
			['De Pan y Melao, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle San Miguel, Local N° 40 Urb., Santa Fe Munic. Herés, Parroquia Vta. Hermosa.', 'Vista Hermosa', 'Stephany', 'Raifer'],
			['Decoraciones Orel, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Desvio, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Abboud Center', 'Centro', 'Externo', 'Mileibys'],
			['Distribuidora De Productos Virgen del Valle, C.A', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Sucre', 'Periferico', 'Freidymar', 'Rubén'],
			['Distribuidora Eurocom, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'La Paragua', 'Oficina', 'Freidymar', 'Rubén'],
			['Distribuidora Gran Caribe, F.P (Al Rumhein Mayaleh Hussam)', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Grupo Merida', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Distribuidora Moreno-Blanco, C.A.', '', '', '', '', '', '', '', '', '', '', '', ''],
			['Distribuidora Prasan, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Coca Cola', 'La Sabanita', 'Yenire', 'Rosa'],
			['Distribuidora Pumori, C,A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Paseo Heres', 'Paseo Meneses', 'Freidymar', 'Rubén'],
			['Distribuidora Raiders, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Agua Salada ', 'Oficina', 'Externo', 'Mileibys'],
			['Distribuidora S&P, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Coca Cola', 'La Sabanita', 'Yenire', 'Rosa'],
			['Distribuidora Sur-Oriente JT, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Vista hermosa', 'Estadio Herés', 'Stephany', 'Raifer'],
			['Distribuidora y Variedades Publi B.M., C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Final del Paseo', 'Centro', 'Mileibys', 'Jonathan'],
			['Drymaca, C.A.', '', 'S/Operaciones', '', 'A', 'Especial', 'Mensual', 'NO APLICA', '', '', '', 'Freidymar', 'Rubén'],
			['El Andinito Expres, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Las Americas', 'Centro', 'Erick', 'Jaqueline'],
			['El Bodegon D\'Naight, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Avenida puente Gomez', 'La Sabanita', 'Ninibeth', 'Carlos Javier'],
			['El Emir,C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseo Gaspari', 'Periferico', 'Freidymar', 'Rubén'],
			['El Marash II, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Av. Rotaria Edif Minghetti, PB, Urb. Vta. Hermosa', 'Estadio Herés', 'Mileibys', 'Jonathan'],
			['El Marash, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Vidal', 'Paseo Meneses', 'Freidymar', 'Rubén'],
			['El Rincon del Arabito, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Cruz Verde', 'Cruz Verde', 'Iris', 'Stephany Gutierrez'],
			['El Rincon del Guaro, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'C.C. La Piedra del Medio', 'Avenida 17 de Diciembre', 'Stephany', 'Raifer'],
			['Electrocom w8, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Erick', 'Jaqueline'],
			['Envidiosas, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Abboud Center', 'Centro', 'Externo', 'Erick'],
			['Especialidades Opticas Bensayan, F.P.', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Medico Orinoco', 'Paseo Meneses', 'Freidymar', 'Rubén'],
			['Estacion de Servicios Las Banderas, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Agua Salada', 'Las Banderas', 'Stephany', 'Raifer'],
			['Estacion de Servicios Tacoma, C.A. ', '', 'Activos', 'C.A.', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Avenida puente Gomez', 'La sabanita', 'Mileibys', 'Maite'],
			['Exclusiva Boutique, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Samara', 'Avenida 17 de Diciembre', 'Externo', 'Erick'],
			['Extintores Continental,S.R.L', '', 'Activos', 'S.R.L.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Caracas', 'Centro', 'Erick', 'Jaqueline'],
			['Facontex, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Jesus Soto', 'Avenida Jesus Soto', 'Yenire', 'Rosa'],
			['Farmacia 701 C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida 17 de Diciembre', 'Avenida 17 de Diciembre', 'Mileibys', 'Jonathan'],
			['Farmacia España, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida España', 'La sabanita', 'Yenire', 'Rosa'],
			['Farmacia Mundo Salud Express, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Andres Eloy blanco', 'Avenida Andres Eloy Blanco', 'Stephany', 'Raifer'],
			['Fashion Girls, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Bolivar Plaza', 'Centro', 'Externo', 'Mileibys'],
			['Fashion Moda Boutique, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Samara', 'Avenida 17 de Diciembre', 'Externo', 'Yenire'],
			['Ferreteria Fertamanaco 2014, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida España', 'La sabanita', 'Ninibeth', 'Carlos Javier'],
			['Ferretería Lusitano C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida La paragua', 'Avenida Libertador', 'Stephany', 'Raifer'],
			['Festejos Tropical,C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida 17 de Diciembre', 'Avenida 17 de Diciembre', 'Externo', 'Erick'],
			['Fina Modas, S.R.L.', '', 'Activos', 'S.R.L.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Sucre', 'Periferico', 'Externo', 'Erick'],
			['Floristeria Marizuly, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Cruz Verde', 'Cruz Verde', 'Erick', 'Jaqueline'],
			['Forest, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Puerto Ordaz', 'Puerto Ordaz', 'Stephany', 'Raifer'],
			['Francisco Ambrosino Morabito', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Clinica La MIlagrosa', 'Avenida Jesus Soto', 'Yenire', 'Dayannery'],
			['Frango\'s Broaster C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Las Banderas', 'Las Banderas', 'Ninibeth', 'Carlos Javier'],
			['Full Fuegos Bolivar, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Paseo Gaspari', 'Periferico', 'Yenire', 'Rosa'],
			['Fundacion Universitaria Iberoamericana', '', 'Externo', 'A.C.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida 17 de Diciembre', 'Avenida 17 de Diciembre', 'Mileibys', 'Fatima'],
			['Funerarias del Sur, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Grupo Merida', 'Centro', 'Erick', 'Jaqueline'],
			['Funesur Servicios, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Grupo Merida', 'Centro', 'Yenire', 'Rosa'],
			['Ghazy Nasser Salah El Dine', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Nasser', 'Centro', 'Externo', 'Yenire'],
			['Gifts Shop, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Crytal Shoping', 'Paseo Meneses', 'Externo', 'Erick'],
			['Giga 88, C.A.', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['Giuseppe Modello D´Amenico', '', 'Activos', 'PN', 'D', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Clinica La milagrosa', 'Avenida Jesus Soto', '', ''],
			['Godzilla Shop, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'Yenire', 'Rosa'],
			['Gouberry', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Paseo Meneses', 'Paseo Meneses', 'Externo', 'Erick'],
			['Gran Moda, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseo Orinoco', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Gran Remate 2050, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseo Orinico', 'Centro', 'Stephany', 'Raifer'],
			['Greal, C.A', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida La paragua', 'Avenida Libertador', 'Freidymar', 'Rubén'],
			['GT Auto Shop, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'E/S La Redoma', 'La Redoma', 'Iris', 'Stephany Gutierrez'],
			['H. Kassem Mundo Fashion, F.P (Hndi Saman)', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Centro Comercial Nasser', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Harim, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Centro Comercial Plaza', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Hernan Espinoza', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Avenida Jesus Soto', 'Externo', 'Yenire'],
			['Herreria Catania, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Agua Salada', 'Las Banderas', 'Yenire', 'Rosa'],
			['Hielos La Roca, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Freidymar', 'Rubén'],
			['Hierrotechos Guayana, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Puerto Ordaz', 'Puerto Ordaz', 'Erick', 'Jaqueline'],
			['Hotel Turistico Libertador, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Libertador', 'Avenida Libertador', 'Stephany', 'Raifer'],
			['Importaciones Lu Lu, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Jesus Soto', 'Avenida Jesus Soto', 'Externo', 'Mileibys'],
			['Importadora Free, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Coca Cola', 'La sabanita', 'Ninibeth', 'Carlos Javier'],
			['Inmobiliaria America 2006, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Las Americas', 'Centro', 'Erick', 'Jaqueline'],
			['Intuición, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', '', '', 'Ninibeth', 'Carlos Javier'],
			['Inversiones 1958, C.A.', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['Inversiones 888, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Crytal Shoping', 'Paseo Meneses', 'Stephany', 'Raifer'],
			['Inversiones Autoval, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida España', 'La sabanita', 'Stephany', 'Raifer'],
			['Inversiones Chimata Tepuy, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida 17 de Diciembre', 'Oficina', 'Ninibeth', 'Carlos Javier'],
			['Inversiones Ferreven, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Jesus Soto', 'Avenida Jesus Soto', 'Ninibeth', 'Carlos Javier'],
			['Inversiones H Y R B, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Pasaje Bolivar', 'Centro', 'Ninibeth', 'Carlos Javier'],
			['Inversiones M.G.P.O, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Agua Salada', 'Las Banderas', 'Ninibeth', 'Carlos Javier'],
			['Inversiones Naturalisimas Glaybemar, S.R.L.', '', 'Activos', 'S.R.L.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Ele Terminal', 'Terminal', 'Yenire', 'Rosa'],
			['Inversiones Orinoco Electronic, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Paseo Orinoco', 'Centro', 'Freidymar', 'Rubén'],
			['Inversiones Palese, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Mileibys', 'Jonathan'],
			['Inversiones Super Maravilla, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'EricK', 'Jaqueline'],
			['Inversiones Yomi, C.A', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida 17 de Diciembre', 'Avenida 17 de Diciembre', 'Mileibys', 'Jonathan'],
			['Isma, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Ninibeth', 'Carlos Javier'],
			['Jhonny Rafael Barreto Arias', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Clinica La Milagrosa', 'Avenida Jesus Soto', 'Yenire', 'Dayannery'],
			['Jorge Rabat', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Clinica La Milagrosa', 'Avenida Jesus Soto', 'Mileibys', 'Maite'],
			['Josefina Peluqueria Unisex, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Hermano Liberto', 'Avenida Jesus Soto', 'Externo', 'Erick'],
			['Joyeria Diamante Cristal, S.R.L.', '', 'Activos', 'S.R.L.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Pasaje Bolivar', 'Centro', 'Externo', 'Yenire'],
			['Joyeria Los Angeles, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Pasaje Bolivar', 'Centro', 'Externo', 'Mileibys'],
			['k\' Ofertas, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venzuela', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Kamina, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Keddy´s, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Crytal Shoping', 'Paseo Meneses', 'Externo', 'Yenire'],
			['La Belleza, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseo Orinoco', 'Centro', 'Stephany', 'Raifer'],
			['La Bodega de Renato, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Avenida Jesus Soto', 'La Redoma', 'Yenire', 'Rosa'],
			['La Boutique del Cuero, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro Comercial Samara', 'Avenida 17 de Diciembre', 'Freidymar', 'Rubén'],
			['La Boveda, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle La Urbina', 'Avenida 17 de Diciembre', 'Stephany', 'Raifer'],
			['La Esmeraldha Eventos, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro Comercial Madeirense', 'Avenida Andres Eloy Blanco', 'Freidymar', 'Rubén'],
			['La Gran Feria del Calzado, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'EricK', 'Jaqueline'],
			['Laboexpress Meneses, C.A.', '', 'Activos', 'C.A.', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Paseo Meneses', 'Paseo Meneses', 'Yenire', 'Dayannery'],
			['Labomedica Guayana, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Republica', 'Oficina', 'Iris', 'Stephany Gutierrez'],
			['Laboratorio Clinico Bacteriologico Siegart, C.A.', '', 'Activos', 'C.A.', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Paseo Meneses', 'Periferico', 'Mileibys', 'Maite'],
			['Laboratorio Clinico La Chinita, 2015, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Paseo Meneses', 'Paseo Meneses', 'Freidymar', 'Rubén'],
			['Laboratorio Clinico Roybis & CIA. ', '', 'Activos', 'C.I.A', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida 17 de Diciembre', 'Avenida 17 de Diciembre', 'Stephany', 'Raifer'],
			['Laboratorio de Especialidades del Bioanalisis,C.A.(L.E.B.C.A.)', '', 'Activos', 'C.A.', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Avenida Germania', 'Hospital', 'Yenire', 'Dayannery'],
			['Leon Al Rumhein Al Mayaleh', '', 'Activos', 'PN', 'C', 'Formal', 'Mensual', 'NO APLICA', '', 'Av. Andres Bello, Edif. Clinica Andres Bello, Piso PB, Of. 4, Sector Aeropuerto Parroquia Catedral, Ciudad Bolivar, Edo.Bolivar', 'Hospital', 'Iris', 'Stephany Gutierrez'],
			['Leudys Guillermina Bermudez de Santodomingo', '', '', '', '', '', '', '', '', '', '', '', ''],
			['Liberto & Asociados, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Hermano Liberto', 'Avenida Jesus Soto', 'EricK', 'Jaqueline'],
			['Librolandia, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Piar', 'Centro', 'EricK', 'Jaqueline'],
			['Licoreria Isa, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'La Paragua', 'La Paragua', 'EricK', 'Jaqueline'],
			['Licoreria Nativisaias,C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Parque Ruiz Pineda', 'Avenida Libertador', 'Mileibys', 'Jonathan'],
			['Licoreria Perimetral, C.A.', '', 'Afectado', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Avenida España', 'La sabanita', 'Freidymar', 'Rubén'],
			['Licoreria Rio Caura, F.P (Jose Armas)', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Maripa', 'Oficina', 'Externo', 'Mileibys'],
			['Licoreria y Charcuteria San Vicente, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Avenida 17 de Diciembre', 'Avenida 17 de Diciembre', 'Externo', 'Mileibys'],
			['Licores MH Perimetral, C.A.', '', 'Afectado', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Avenida España', 'La sabanita', 'Yenire', 'Rosa'],
			['Loterias Khaled, F.P.', '', 'Activos', 'F.P.', 'D', 'No Sujeta', 'NO APLICA', 'NO APLICA', '', 'Pasaje Triviño', 'Oficina', 'Stephany', 'Raifer'],
			['Lovely Shoes Moda, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Pasaje Triviño', 'Centro', 'Externo', 'Mileibys'],
			['Lubricantes El Poder de Dios, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Germania', 'Hospital', 'Freidymar', 'Rubén'],
			['Luis Armando Bello Vargas (Servicios y Asistencia Gerencial Luis Bello, F.P.)', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle El Llaner, enre Av Manapire y Calle 6, Casa 3, Ub Cristo Rey. Vallee de la Pascua. Edo Guarico.', 'Las Banderas', 'Ninibeth', 'Carlos Javier'],
			['Maderera Caroni, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Jhon Bolívar', 'Paseo Meneses', 'Mileibys', 'Jonathan'],
			['Manar Motos, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Avenida Upata', 'Hospital', 'Freidymar', 'Rubén'],
			['Manuel Ozon Peña', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Grupo Merida', 'Centro', 'EricK', 'Jaqueline'],
			['Manupol, C.A.', '', 'Asesorias', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Asesorias', 'Asesorias'],
			['Maritza del Valle Martinez', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Clinica La Milagrosa', 'Avenida Jesus Soto', 'Externo', 'Mileibys'],
			['Materiales El Rey, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Upata', 'Oficina', 'Yenire', 'Rosa'],
			['Materiales Mara, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Marhuanta', 'La Redoma', 'EricK', 'Jaqueline'],
			['Materiales S.H. El Rey, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Final Calle Colon ', 'La sabanita', 'Ninibeth', 'Carlos Javier'],
			['Maxicell W8, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Crytal Shoping', 'Paseo Meneses', 'Stephany', 'Raifer'],
			['Maximodas, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'EricK', 'Jaqueline'],
			['McD Pollo, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Cetro Comercial Hermanos Liberto', 'Avenida Jesus Soto', 'Mileibys', 'Jonathan'],
			['Mega-Salud C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Germania', 'Hospital', 'Mileibys', 'Jonathan'],
			['Metal Cell C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro Comercial Las Banderas', 'Las Banderas', 'Ninibeth', 'Carlos Javier'],
			['Mey Cream Chan, F.P. (Lai Chan)', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Crytal Shoping', 'Paseo Meneses', 'Externo', 'Yenire'],
			['Mia\'s Studio, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Centro Comercia Phenicia', 'Hospital', 'Externo', 'Erick'],
			['Moises Bensayan Lopez', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Medico Orinoco', 'Paseo Meneses', 'Freidymar', 'Rubén'],
			['Motel Angostura & CIA, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Los Proceres', 'Los Procesos', 'Yenire', 'Rosa'],
			['Motel Turistico Venus Suite, C.A', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Marhuanta', 'Oficina', 'Iris', 'Stephany Gutierrez'],
			['Movil Line, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseje Triviño', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Multicauchos Nueva Ideal, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'La Sabanita', 'La sabanita', 'Stephany', 'Raifer'],
			['Multiservicios 5B&V, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Pto. La Cruz', 'Oficina', 'Mileibys', 'Jonathan'],
			['Multiservicios de Refrigeracion y Radiadores Stefano, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Periferico', 'Periferico', 'Yenire', 'Rosa'],
			['Multiservicios San Miguel RL, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'La Sabanita', 'La sabanita', 'Mileibys', 'Jonathan'],
			['Mundo Cel, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro Comercial Las Americas', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Mundo Toyota, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Av. Republica', 'Avenida República', 'Yenire', 'Rosa'],
			['Nasser N. Yhmed', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Externo', 'Yenire'],
			['Nasser Salah El Dine Akram', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Sucre', 'Terminal', 'Yenire', 'Rosa'],
			['Nastasi Medina Salvador Carlos', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Centro Medico Orinoco', 'Paseo Meneses', 'Mileibys', 'Maite'],
			['Nenela Variedades y Regalos, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro', 'Centro', 'Externo', 'Mileibys'],
			['Novedades Italia, C.A.', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['Nuccia Bisignano Morfortese', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Avenida 17 de Diciembre', 'Avenida 17 de Diciembre', 'Mileibys', 'Maite'],
			['Optica Meneses, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Centro Comercial Crytal Shoping', 'Paseo Meneses', 'Mileibys', 'Jonathan'],
			['Orilet, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Libertador', 'Avenida Libertador', 'Stephany', 'Raifer'],
			['Orinoco  Color C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Yenire', 'Rosa'],
			['Pablo Valles', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Oficina', 'Oficina', 'Mileibys', 'Maite'],
			['Panadería y Pastelería Gourmet Santa Cruz C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Sucre', 'Terminal', 'Yenire', 'Rosa'],
			['Panaderia y Pasteleria Republica, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Repúbilca', 'Avenida República', 'Ninibeth', 'Carlos Javier'],
			['Pasarela Shoes, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Centro Comercial Los Proceres', 'Hospital', 'Externo', 'Mileibys'],
			['Periferico Loterias, S.R.L.', '', 'Activos', 'S.R.L.', 'D', 'No Sujeta', 'NO APLICA', 'NO APLICA', '', 'Periferico', 'Periferico', 'Yenire', 'Rosa'],
			['Pimpones Kids, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Nasser', 'Centro', 'EricK', 'Jaqueline'],
			['Pits Automotriz, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Maracay', 'Avenida Jesus Soto', 'Mileibys', 'Jonathan'],
			['Productos de La Madera Promaca C.A.', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Mileibys', 'Dayannery'],
			['Punto Casual, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Paseo Orinioco', 'Centro', 'Externo', 'Yenire'],
			['Raiza Vallee', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Jesus Soto', 'Avenida Jesus Soto', 'Externo', 'Erick'],
			['Rectimundial, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'La Sabanita', 'La sabanita', 'Ninibeth', 'Carlos Javier'],
			['Refinca, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'La Sabanita', 'La sabanita', 'Yenire', 'Rosa'],
			['Remberto Jose Hernandez Da Silva', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Meneses', 'Paseo Meneses', 'Externo', 'Yenire'],
			['Representaciones SZ, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'La Sabanita', 'Oficina', 'Ninibeth', 'Carlos Javier'],
			['Repuestos Continental, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'La Sabanita', 'La Sabanita', 'Yenire', 'Rosa'],
			['Repuestos El Criollito, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Nueva Granada', 'Avenida Nueva Granada', 'Mileibys', 'Jonathan'],
			['Repuestos y Accesorios Bici-Moto Don Rafa, C.A.', '', 'Afectado', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida españa', 'La sabanita', 'Freidymar', 'Rubén'],
			['Restaurant Servi Express, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Mileibys', 'Jonathan'],
			['Restaurant y Loncheria Xuan Hung Xing, F.P.', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Grupo Merida', 'Centro', 'Externo', 'Erick'],
			['Restaurant, Pizzeria Café Las Nieves, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Clinica Las Nieves', 'Hospital', 'Stephany', 'Raifer'],
			['Rios Grill 2014, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Las Banderas', 'Las Banderas', 'Ninibeth', 'Carlos Javier'],
			['Rivas Gomez Jesus Rafael ', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Avenida Jesus Soto', 'Externo', 'Yenire'],
			['Robert Arquimedes Garcia Palacios.', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Avenida Jesus Soto. Centro Comercial Hermanos Liberto', 'Oficina', 'Yenire', 'Dayannery'],
			['Romans Beauty Center, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Liebertador, Edificio TRAKI, Piso PB, Local L-21 Sector Medina Anagarita.', 'Avenida Libertador', 'Mileibys', 'Jonathan'],
			['Ros Comunicaciones, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Colón', 'La Sabanita', 'Ninibeth', 'Carlos Javier'],
			['Roximar Salazar 11, F.P.', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Republica C/C Avenida Sucre', 'Terminal', 'Ninibeth', 'Carlos Javier'],
			['Ruiz Rondon Carielys Johanny', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Andres Eloy blanco', 'Avenida Nueva Granada', 'Externo', 'Erick'],
			['Ruiz Rondon Carilys Johanny', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Andres Eloy blanco', 'Avenida Nueva Granada', 'Externo', 'Erick'],
			['Ruiz Rondon Carlizbeth Johanna', '', 'Activos', 'PN', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Andres Eloy blanco', 'Avenida Nueva Granada', 'Externo', 'Erick'],
			['Sadher Laboratorio Clinico, C.A.', '', 'Activos', 'C.A.', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Guri', 'Oficina', 'Yenire', 'Dayannery'],
			['Selecta Guayana, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseo Meneses', 'Paseo Meneses', 'Mileibys', 'Jonathan'],
			['Serenos Eudorina, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Freidymar', 'Rubén'],
			['Sergio Parrilla Restaurante, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Vista hermosa', 'Avenida 17 de Diciembre', 'Stephany', 'Raifer'],
			['Service Center II, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Paseo Heres', 'Avenida Jesus Soto', 'EricK', 'Jaqueline'],
			['Silmai, C.A', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['Sirop Bistro Café C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Av. Jesús Soto Cruce con Calle Paseo Heres C.C.Lulu Nivel 1 Local A2-1 Sector Aeropuerto', 'Avenida Jesus Soto', 'EricK', 'Jaqueline'],
			['Sky Fashion, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Puerto Ordaz', 'Puerto Ordaz', 'Mileibys', 'Jonathan'],
			['Sonia Contreras', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Clinica La Milagrosa', 'Avenida Jesus Soto', 'Mileibys', 'Maite'],
			['Sport X\'Tremos, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro Comercial Plaza', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Sporting I, C.A.', '', 'Afectado', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Las Banderas', 'Las Banderas', 'Stephany', 'Raifer'],
			['Sumequi, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Avenida Germania', 'Hospital', 'Stephany', 'Raifer'],
			['Suministro y Mantenimiento Toyo 1000, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Avenida puente Gomez', 'La sabanita', 'Ninibeth', 'Carlos Javier'],
			['Suministros Medicos Quirurgicos Zailis, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Savoy', 'Hospital', 'Stephany', 'Raifer'],
			['Supermercado Periferico, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Periferico', 'Periferico', 'Stephany', 'Raifer'],
			['Taller Latoneria y Pintura 2 JJ, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'La Sabanita', 'La sabanita', 'Externo', 'Yenire'],
			['Tecnica de Mantos, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Las Banderas', 'Las Banderas', 'Yenire', 'Rosa'],
			['Telecomunicaciones Nass Center, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro Comercial Nasser', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Telecomunicaciones New Generation, C.A. ', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Centro Comercial Las Americas', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Teresa Emperatriz Anciany de Tovar (New Fashion Anciany, F.P.)', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Crytal Shoping', 'Paseo Meneses', 'Externo', 'Erick'],
			['Tienda Naturista Natura Michell, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Vista hermosa', 'Avenida 17 de Diciembre', 'Freidymar', 'Rubén'],
			['Tomasi Almeida Jesus Felix', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Oficina', 'Oficina', 'Yenire', 'Dayannery'],
			['Torni-Frio Bolívar, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Paseo Meneses', 'Paseo Meneses', 'Stephany', 'Raifer'],
			['Trovato Snack Sport Bar, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Andres Eloy blanco', 'Oficina', 'Freidymar', 'Rubén'],
			['Tubo Riego, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'La Sabanita', 'La sabanita', 'Ninibeth', 'Carlos Javier'],
			['Vallee Tomasini y Asociados, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Oficina', 'Oficina', 'Carolina', 'Carolina'],
			['Vanidades, Estilos y Uñas, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Pasaje triviño', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Variedades Nastasi, F.P (Gian Franco Nastasi Lisa)', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Andres Bello', 'Hospital', 'Freidymar', 'Rubén'],
			['Variedades Omarcasoca Lugo, F.P.(Omaira Lugo).', '', 'Activos', 'F.P.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Oficina', 'Avenida Jesus Soto', 'Externo', 'Mileibys'],
			['Vida Joven, C.A.', '', 'Externo', 'C.A.', 'A', 'Especial', 'Mensual', 'NO APLICA', '', 'Calle Venezuela', 'Centro', 'Acapulco', 'Barbara'],
			['Wago\'s Sport, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Centro Comercial Abboud Center', 'Centro', 'Externo', 'Mileibys'],
			['Xio Computer, C.A', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Oficina', 'Avenida Jesus Soto', 'Ninibeth', 'Carlos Javier'],
			['Yingrimar Elena Lopez Yanez', '', 'Activos', 'PN', 'C', 'Formal', 'Trimestral', 'NO APLICA', '', 'Clinica ', 'Oficina', 'Mileibys', 'Maite'],
			['Zapateria la Maravilla, C.A.', '', 'Activos', 'C.A.', 'A', 'Especial', 'Mensual', 'YAMILET', '', 'Calle Venezuela', 'Centro', 'Erick', 'Jaqueline'],
			['Zapateria La Reyna, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'YAMILET', '', 'Avenida Cumana', 'Centro', 'Iris', 'Stephany Gutierrez'],
			['Zero S21 C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Calle Callejón Urbina Local Galpón S/N Sector Negro Primero', 'Avenida 17 de Diciembre', 'Stephany', 'Raifer'],
			['Inversiones N.A., C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Upata CC Phenicia, Nivel Planta Baja ', 'Hospital', 'Yenire', 'Rosa'],
			['Inversiones Phenicia, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Avenida Upata CC Phenicia, Nivel Planta Baja ', 'Hospital', 'Yenire', 'Rosa'],
			['Luis Alonso Navarrete D\' Marco', '', 'Activos', 'PN', 'D', 'Ordinario', 'Mensual', 'NO APLICA', '', 'Av Libertador Edif. Dorado Piso 1, Apt 02, Barrio Ajuro', 'Oficina', 'Iris', 'Stephany Gutierrez'],
			['Supermercado Quality, C.A.', '', 'Activos', 'C.A.', 'B', 'Ordinario', 'Mensual', 'NO APLICA', '', 'La Paragua', 'Oficina', 'Freidymar', 'Rubén'],
		];

		$condicion = ['' => null];
		foreach (ClientesCondicion::all() as $val) {
			$condicion[str_slug($val->nombre)] = $val->id;
		}

		$condicionContribuyente = ['' => null];
		foreach (ClientesCondicionContribuyente::all() as $val) {
			$condicionContribuyente[str_slug($val->nombre)] = $val->id;
		}

		$razonSocial = ['' => null];
		foreach (ClientesRazonSocial::all() as $val) {
			$razonSocial[str_slug($val->nombre)] = $val->id;
		}

		$rutas = ['' => null];
		foreach (ClientesRutas::all() as $val) {
			$rutas[str_slug($val->nombre)] = $val->id;
		}

		$categorias = ['' => null];
		foreach (ClientesCategorias::all() as $val) {
			$categorias[str_slug($val->nombre)] = $val->id;
		}

		$empleados = ['' => null];
		foreach (EmpresaEmpleados::all() as $val) {
			$empleados[str_slug($val->nombre . ' ' . $val->apellido)] = $val->id;
			$empleados[str_slug($val->nombre)] = $val->id;
		}

		$servicios = [
			[],
			[1, 2, 3, 4, 5, 6, 7, 9],
			[4, 7, 9],
			[1, 4, 7, 8, 9],
			[]
		];

		foreach ($clientes as $cliente) {
			//break;
			$cliente[2] = str_slug($cliente[2]);
			$cliente[3] = str_slug($cliente[3]);
			$cliente[4] = str_slug($cliente[4]);
			$cliente[5] = str_slug($cliente[5]);
			$cliente[10] = str_slug($cliente[10]);
			$cliente[11] = str_slug($cliente[11]);
			$cliente[12] = str_slug($cliente[12]);

			if ($cliente[11] == 'externo'){
				$cliente[11] = '';
			}

			if ($cliente[10] == 'centro'){
				$cliente[10] = 'centro-1';
			}

			if ($cliente[2] == 'soperaciones'){
				$cliente[2] = '';
			}
			
			if ($cliente[10] == 'paseo-meneses'){
				$cliente[10] = 'paseo-meneses-1';
			}

			if ($cliente[10] == 'avenida-nueva-granada'){
				$cliente[10] = 'avenida-nueva-granada-1';
			}

			if ($cliente[11] == 'acapulco' || 
				$cliente[11] == 'asesorias' ||
				$cliente[11] == 'barbara' ||
				$cliente[11] == 'zulay' ||
				$cliente[11] == 'carlos-javier'
				){
				$cliente[11] = '';
			}

			if ($cliente[12] == 'acapulco' || 
				$cliente[12] == 'barbara' ||
				$cliente[12] == 'asesorias' ||
				$cliente[12] == 'carlos-javier' ||
				$cliente[12] == 'zulay' ||
				$cliente[12] == 'rosa' ) {
				$cliente[12] = '';
			}

			if (
				!isset($rutas[$cliente[10]])
				){
				//dd("Falta ruta", $cliente);
			}

			if (
				!isset($empleados[$cliente[11]]) || 
				!isset($empleados[$cliente[12]])
				){
				//
				//dd($empleados, $cliente);
			}

			$data = [
				'nombre'                              => $cliente[0],
				'direccion'                           => $cliente[8],
				'clientes_categorias_id'              => $categorias[$cliente[4]],
				'clientes_rutas_id'                   => $rutas[$cliente[10]],
				'clientes_condicion_contribuyente_id' => $condicionContribuyente[$cliente[5]],
				'clientes_razon_social_id'            => $razonSocial[$cliente[3]],
				'clientes_condicion_id'               => $condicion[$cliente[2]],
				//'jefe_departamento'                   => $empleados[$cliente[11]],
				'supervisor'                          => $empleados[$cliente[11]],
				'asistente'                           => $empleados[$cliente[12]],
			];

			$_cliente = Clientes::create($data);

			//ServiciosClientes::where('clientes_id', $_cliente->id)->delete();
			if ($cliente[5] != ''){
				foreach ($servicios[$condicionContribuyente[$cliente[5]]] as $servicio_id) {
					ServiciosClientes::create([
						'servicios_id' => $servicio_id,
						'clientes_id'  => $_cliente->id
					]);
				}
			}
		}
	}
}
