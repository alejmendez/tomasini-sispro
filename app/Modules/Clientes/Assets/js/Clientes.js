var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'buscar' : function(r) {
			$servicios = $('.servicios');
			$('input', $servicios).prop('checked', false);
			for (var i in r.servicios) {
				$("#servicios_" + r.servicios[i], $servicios).prop('checked', true);
			}
		},
		'limpiar' : function(){
			tabla.ajax.reload();
			$('.servicios input').prop('checked', false);
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{"data":"nombre","name":"clientes.nombre"},
			{"data":"razon_social","name":"clientes_condicion_contribuyente.nombre"}
		]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$("#clientes_condicion_contribuyente_id").change(function() {
		var condicion = $(this).val(),
			servicios = [
				[],
				[1, 2, 3, 4, 5, 6, 7, 9],
				[4, 7, 9],
				[1, 4, 7, 8, 9],
				[]
			],
			$servicios = $('.servicios');

		$('input', $servicios).prop('checked', false);
		for (var i in servicios[condicion]) {
			$("#servicios_" + servicios[condicion][i], $servicios).prop('checked', true);
		}
	});
});