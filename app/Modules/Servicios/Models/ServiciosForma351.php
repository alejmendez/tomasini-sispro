<?php

namespace App\Modules\Servicios\Models;

use Carbon\Carbon;
use App\Modules\Base\Models\Modelo;

use App\Modules\Empresa\Models\EmpresaBancos;

class ServiciosForma351 extends modelo
{
    protected $table = 'servicios_forma_35_1';
    protected $fillable = [
        'clientes_historico_id',
        'recibido_admon',
        'recibido_oper',
        'proceso',
        'revision_declarado',
        'envio',
        'certificado',
        'numero_planilla',
        'pago_planilla',
        'bancos_id',
        'monto_cancelado',
        'entrega_admon',
        'entrega',
        'entrega_comunicacion',
        'entrega_documentos',
        'comentario'
    ];
    
    public $campos = [
        'clientes_historico_id' => [],
        'recibido_admon' => [
            'type'        => 'date',
            'label'       => 'Recibido a Administración',
            'placeholder' => 'Recibido a Administración',
            'title'       => 'Fecha de Recepción de las Retenciones de la 1era. Quincena'
        ],
        'recibido_oper' => [
            'type'        => 'date',
            'label'       => 'Recibido a Operaciones',
            'placeholder' => 'Recibido a Operaciones',
            'cont_class'  => 'col-lg-3 col-md-4 col-sm-6 col-xs-12 cont-fecha-operaciones',
            'title'       => 'Fecha de Documentos Recibidos Departamento de Operaciones '
        ],
        'proceso' => [
            'type'        => 'date',
            'label'       => 'Proceso',
            'placeholder' => 'Proceso'
        ],
        'revision_declarado' => [
            'type'        => 'date',
            'label'       => 'Revisión Declarado',
            'placeholder' => 'Revisión Declarado'
        ],
        'entrega_admon' => [
            'type'        => 'date',
            'label'       => 'Envio a Administración',
            'placeholder' => 'Envio a Administración',
            'title'       => 'Fecha de envio al departamento de Administración'
        ],
        'certificado' => [
            'type'        => 'select',
            'label'       => 'Certificado',
            'options'     => ['No', 'Si'],
            'value'       => 0,
            'placeholder' => false
        ],
        'numero_planilla' => [
            'type'        => 'text',
            'label'       => 'Numero Planilla',
            'placeholder' => 'Numero Planilla'
        ],
        'pago_planilla' => [
            'type'        => 'date',
            'label'       => 'Pago Planilla',
            'placeholder' => 'Pago Planilla'
        ],
        'bancos_id' => [
            'type'        => 'select',
            'label'       => 'Bancos',
            'placeholder' => '- Seleccione un Bancos'
        ],
        'monto_cancelado' => [
            'type'        => 'text',
            'label'       => 'Monto Cancelado',
            'placeholder' => 'Monto Cancelado'
        ],
        'entrega' => [
            'type'        => 'date',
            'label'       => 'Entrega al Personal Externo',
            'placeholder' => 'Entrega al Personal Externo',
            'title'       => 'Fecha de Entrega al Personal Externo'
        ],
        'entrega_comunicacion' => [
            'type'        => 'date',
            'label'       => 'Entrega de Comunicacion al Cliente',
            'placeholder' => 'Entrega de Comunicacion al Cliente',
            'title'       => 'Fecha de Entrega de la Comunicación al Cliente'
        ],
        'entrega_documentos' => [
            'type'        => 'select',
            'label'       => 'Entrega a',
            'placeholder' => '- Entrega a...',
            'options'     => [
                'c' => 'Cliente',
                'i' => 'Inventario',
            ]
        ],
        'comentario' => [
            'type'        => 'textarea',
            'label'       => 'Comentario',
            'placeholder' => 'Comentario',
            'cont_class'  => 'col-sm-12'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['bancos_id']['options'] = EmpresaBancos::pluck('nombre', 'id');
    }

    public function setRecibidoAdmonAttribute($value)
    {
        $this->attributes['recibido_admon'] = $this->formatoFecha($value);
    }

    public function getRecibidoAdmonAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setRecibidoOperAttribute($value)
    {
        $this->attributes['recibido_oper'] = $this->formatoFecha($value);
    }

    public function getRecibidoOperAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setProcesoAttribute($value)
    {
        $this->attributes['proceso'] = $this->formatoFecha($value);
    }

    public function getProcesoAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setRevisionDeclaradoAttribute($value)
    {
        $this->attributes['revision_declarado'] = $this->formatoFecha($value);
    }

    public function getRevisionDeclaradoAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEnvioAttribute($value)
    {
        $this->attributes['envio'] = $this->formatoFecha($value);
    }

    public function getEnvioAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setPagoPlanillaAttribute($value)
    {
        $this->attributes['pago_planilla'] = $this->formatoFecha($value);
    }

    public function getPagoPlanillaAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEntregaAttribute($value)
    {
        $this->attributes['entrega'] = $this->formatoFecha($value);
    }

    public function getEntregaAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEntregaComunicacionAttribute($value)
    {
        $this->attributes['entrega_comunicacion'] = $this->formatoFecha($value);
    }

    public function getEntregaComunicacionAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
}