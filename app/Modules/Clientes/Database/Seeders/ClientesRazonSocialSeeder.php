<?php

namespace App\Modules\Clientes\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use App\Modules\Clientes\Models\ClientesRazonSocial;

class ClientesRazonSocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$razones = [
            'A.C.',
            'C.A.',
            'C.I.A.',
            'F.P.',
            'P.N.',
            'R.L.',
            'S.A.',
            'S.R.L.',
        ];

    	DB::beginTransaction();
        try{
            foreach ($razones as $razon) {
                ClientesRazonSocial::create([
                    'nombre' => $razon
                ]);
            }
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();
    }
}
