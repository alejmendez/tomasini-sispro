<?php

namespace App\Modules\Servicios\Models;

use Carbon\Carbon;
use App\Modules\Base\Models\Modelo;

use App\Modules\Empresa\Models\EmpresaBancos;

class ServiciosForma30 extends modelo
{
    protected $table = 'servicios_forma_30';
    protected $fillable = [
        'clientes_historico_id',
        'recibido_admon',
        'recibido_oper',
        'proceso',
        'revision_declarado',
        'registro',
        'envio',
        'certificado',
        'regular_numero_planilla',
        'regular_pago_planilla',
        'regular_bancos_id',
        'regular_monto_cancelado',
        'susti_numero_planilla',
        'susti_pago_planilla',
        'susti_bancos_id',
        'susti_monto_cancelado',
        'entrega_admon',
        'entrega',
        'entrega_comunicacion',
        'comentario'
    ];
    
    public $campos = [
        'clientes_historico_id' => [],
        'recibido_admon' => [
            'type'        => 'date',
            'label'       => 'Recibido a Administración',
            'placeholder' => 'Recibido a Administración',
            'title'       => 'Fecha de Documentos Recibidos Departamento de Administración'
        ],
        'recibido_oper' => [
            'type'        => 'date',
            'label'       => 'Recibido a Operaciones',
            'placeholder' => 'Recibido a Operaciones',
            'cont_class'  => 'col-lg-3 col-md-4 col-sm-6 col-xs-12 cont-fecha-operaciones',
            'title'       => 'Fecha de Documentos Recibidos Departamento de Operaciones '
        ],
        'proceso' => [
            'type'        => 'date',
            'label'       => 'Proceso',
            'placeholder' => 'Proceso'
        ],
        'revision_declarado' => [
            'type'        => 'date',
            'label'       => 'Revisión Declarado',
            'placeholder' => 'Revisión Declarado'
        ],
        'registro' => [
            'type'        => 'date',
            'label'       => 'Registro',
            'placeholder' => 'Registro'
        ],
        'entrega_admon' => [
            'type'        => 'date',
            'label'       => 'Envio a Administración',
            'placeholder' => 'Envio a Administración',
            'title'       => 'Fecha de envio al departamento de Administración'
        ],
        'certificado' => [
            'type'        => 'select',
            'label'       => 'Certificado',
            'options'     => ['No', 'Si'],
            'value'       => 0,
            'placeholder' => false
        ],
        'regular_numero_planilla' => [
            'type'        => 'text',
            'label'       => 'Numero Planilla Regular',
            'placeholder' => 'Numero Planilla Regular'
        ],
        'regular_pago_planilla' => [
            'type'        => 'date',
            'label'       => 'Pago Planilla Regular',
            'placeholder' => 'Pago Planilla Regular'
        ],
        'regular_bancos_id' => [
            'type'        => 'select',
            'label'       => 'Bancos',
            'placeholder' => '- Seleccione un Bancos'
        ],
        'regular_monto_cancelado' => [
            'type'        => 'text',
            'label'       => 'Monto Cancelado Regular',
            'placeholder' => 'Monto Cancelado Regular'
        ],
        'susti_numero_planilla' => [
            'type'        => 'text',
            'label'       => 'Numero Planilla Sustitutiva',
            'placeholder' => 'Numero Planilla Sustitutiva'
        ],
        'susti_pago_planilla' => [
            'type'        => 'date',
            'label'       => 'Pago Planilla Sustitutiva',
            'placeholder' => 'Pago Planilla Sustitutiva'
        ],
        'susti_bancos_id' => [
            'type'        => 'select',
            'label'       => 'Bancos',
            'placeholder' => '- Seleccione un Bancos Sustitutiva'
        ],
        'susti_monto_cancelado' => [
            'type'        => 'text',
            'label'       => 'Monto Cancelado Sustitutiva',
            'placeholder' => 'Monto Cancelado Sustitutiva'
        ],
        'entrega' => [
            'type'        => 'date',
            'label'       => 'Entrega al Personal Externo',
            'placeholder' => 'Entrega al Personal Externo',
            'title'       => 'Fecha de Entrega al Personal Externo'
        ],
        'entrega_comunicacion' => [
            'type'        => 'date',
            'label'       => 'Entrega de Comunicacion al Cliente',
            'placeholder' => 'Entrega de Comunicacion al Cliente',
            'title'       => 'Fecha de Entrega de la Comunicación al Cliente'
        ],
        'comentario' => [
            'type'        => 'textarea',
            'label'       => 'Comentario',
            'placeholder' => 'Comentario',
            'cont_class'  => 'col-sm-12'
        ],
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['regular_bancos_id']['options'] = EmpresaBancos::pluck('nombre', 'id');
        $this->campos['susti_bancos_id']['options'] = EmpresaBancos::pluck('nombre', 'id');
    }

    public function setRecibidoAdmonAttribute($value)
    {
        $this->attributes['recibido_admon'] = $this->formatoFecha($value);
    }

    public function getRecibidoAdmonAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setRecibidoOperAttribute($value)
    {
        $this->attributes['recibido_oper'] = $this->formatoFecha($value);
    }

    public function getRecibidoOperAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setProcesoAttribute($value)
    {
        $this->attributes['proceso'] = $this->formatoFecha($value);
    }

    public function getProcesoAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setRevisionDeclaradoAttribute($value)
    {
        $this->attributes['revision_declarado'] = $this->formatoFecha($value);
    }

    public function getRevisionDeclaradoAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }

    public function setRegistroAttribute($value)
    {
        $this->attributes['registro'] = $this->formatoFecha($value);
    }

    public function getRegistroAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEnvioAttribute($value)
    {
        $this->attributes['envio'] = $this->formatoFecha($value);
    }

    public function getEnvioAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setPagoPlanillaAttribute($value)
    {
        $this->attributes['pago_planilla'] = $this->formatoFecha($value);
    }

    public function getPagoPlanillaAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEntregaAttribute($value)
    {
        $this->attributes['entrega'] = $this->formatoFecha($value);
    }

    public function getEntregaAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
    
    public function setEntregaComunicacionAttribute($value)
    {
        $this->attributes['entrega_comunicacion'] = $this->formatoFecha($value);
    }

    public function getEntregaComunicacionAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
}