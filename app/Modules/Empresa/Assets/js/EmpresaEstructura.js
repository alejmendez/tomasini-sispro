var aplicacion, $form, tabla, $arbol;
$(function() {
	aplicacion = new app('formulario', {
		'buscar' : function(r){
			$arbol.deselect_all();
			$arbol.select_node(r.empresa_estructura_id);
		},
		'limpiar' : function(){
			tabla.ajax.reload();

			arbol();
		}
	});

	arbol();

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{"data":"nombre","name":"nombre"},
			{"data":"empresa_estructura_id","name":"empresa_estructura_id"}
		]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});

function arbol(){
	$.ajax({
		url : $url + 'arbol',
		success : function(r){
			$('#arbol')
			.html('')
			.jstree('destroy')
			.on('changed.jstree', function (e, data) {
				$("#empresa_estructura_id").val(data.instance.get_node(data.selected[0]).id);
				//aplicacion.id = data.instance.get_node(data.selected[0]).id;
			})

			.jstree({
				'core' : {
					//"animation" : 0,
				    "check_callback" : true,
				    "themes" : { "stripes" : true },
				    'force_text' : true,
					"multiple" : true,
					'data' : r
				},
				"types" : {
					"default" : {
						"icon" : "fa fa-folder-o",
						"valid_children" : ["default","file"]
					},
					"todo" : {
						"icon" : "fa fa-sitemap",
						"valid_children" : []
					},
					"arch" : {
						"icon" : "fa fa-file-text-o",
						"valid_children" : []
					},
					"metodo" : {
						"icon" : "fa fa-gears",
						"valid_children" : []
					}
				},
				"checkbox" : {
					//"keep_selected_style" : false
			    },
				"plugins" : [
					"search", "types"
				]
			});

			$arbol = $('#arbol').jstree(true);
		}
	});
}