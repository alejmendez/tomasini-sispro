<?php

namespace App\Modules\Clientes\Models;

use App\Modules\Base\Models\Modelo;

use App\Modules\Clientes\Models\ClientesCategorias;
use App\Modules\Clientes\Models\ClientesRutas;
use App\Modules\Clientes\Models\ClientesCondicionContribuyente;
use App\Modules\Clientes\Models\ClientesRazonSocial;
use App\Modules\Clientes\Models\ClientesCondicion;
use App\Modules\Empresa\Models\EmpresaEmpleados;

class Clientes extends modelo
{
    protected $table = 'clientes';
    protected $fillable = [
        "nombre", 
        "slug", 
        "direccion", 
        "clientes_categorias_id", 
        "clientes_rutas_id", 
        "clientes_condicion_contribuyente_id",
        "clientes_razon_social_id",
        "clientes_condicion_id",
        "supervisor",
        "jefe_departamento",
        "asistente"
    ];
    
    public $campos = [
        'nombre' => [
            'type'        => 'text',
            'label'       => 'Nombre',
            'placeholder' => 'Nombre del Clientes'
        ],
        'clientes_razon_social_id' => [
            'type'        => 'select',
            'label'       => 'Razón Social',
            'placeholder' => '- Razón Social'
        ],
        'clientes_condicion_contribuyente_id' => [
            'type'        => 'select',
            'label'       => 'Condición del Contribuyente',
            'placeholder' => '- Condición del Contribuyente'
        ],
        'clientes_categorias_id' => [
            'type'        => 'select',
            'label'       => 'Categoria',
            'placeholder' => '- Categoria del Cliente'
        ],
        'clientes_rutas_id' => [
            'type'        => 'select',
            'label'       => 'Ruta',
            'placeholder' => '- Rutas del Clientes'
        ],
        'clientes_condicion_id' => [
            'type'        => 'select',
            'label'       => 'Condicion',
            'placeholder' => '- Condicion del Cliente'
        ],
        'supervisor' => [
            'type'        => 'select',
            'label'       => 'Supervisor',
            'placeholder' => '- Supervisor Responsable'
        ],
        'jefe_departamento' => [
            'type'        => 'select',
            'label'       => 'Jefe de Departamento',
            'placeholder' => '- Jefe de Departamento'
        ],
        'asistente' => [
            'type'        => 'select',
            'label'       => 'Asistente',
            'placeholder' => '- Asistente Responsable'
        ],
        'direccion' => [
            'type'        => 'textarea',
            'label'       => 'Dirección',
            'placeholder' => 'Dirección del Clientes',
            'cont_class'  => 'col-sm-12 col-xs-12'
        ],
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['clientes_categorias_id']['options'] = ClientesCategorias::orderBy('nombre')->pluck('nombre', 'id');
        $this->campos['clientes_rutas_id']['options'] = ClientesRutas::orderBy('nombre')->pluck('nombre', 'id');
        $this->campos['clientes_condicion_id']['options'] = ClientesCondicion::orderBy('nombre')->pluck('nombre', 'id');
        $this->campos['clientes_condicion_contribuyente_id']['options'] = ClientesCondicionContribuyente::orderBy('nombre')->pluck('nombre', 'id');
        $this->campos['clientes_razon_social_id']['options'] = ClientesRazonSocial::orderBy('nombre')->pluck('nombre', 'id');

        $empleados = EmpresaEmpleados::orderBy('nombre')->get();
        $this->campos['supervisor']['options'] = [];
        foreach ($empleados as $empleado) {
            $this->campos['supervisor']['options'][$empleado->id] = $empleado->nombre . ' ' . $empleado->apellido;
        }
        
        $this->campos['jefe_departamento']['options'] = $this->campos['supervisor']['options'];
        $this->campos['asistente']['options'] = $this->campos['supervisor']['options'];
    }

    public function getNacimientoAttribute($value){
        return is_null($value) ? $value : $this->formatoFecha($value)->format('d/m/Y');
    }

    public function setCorreoAttribute($value){
        $this->attributes['correo'] = strtolower($value);
    }

    public function categoria(){
        return $this->hasOne('App\Modules\Clientes\Models\ClientesCategorias', 'id', 'clientes_categorias_id');
    }

    public function ruta(){
        return $this->hasOne('App\Modules\Clientes\Models\ClientesRutas', 'id', 'clientes_rutas_id');
    }
    
    public function empleado(){
        return $this->hasOne('App\Modules\Empresa\Models\EmpresaEmpleados', 'id', 'responsable_ruta');
    }

    public function servicios()
    {
        return $this->belongsToMany('App\Modules\Servicios\Models\Servicios', 'servicios_clientes');
    }

    public function condicion_contribuyente()
    {
        return $this->hasOne('App\Modules\Clientes\Models\ClientesCondicionContribuyente', 'id', 'clientes_condicion_contribuyente_id');
    }
}