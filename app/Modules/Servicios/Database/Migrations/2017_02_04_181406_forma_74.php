<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Forma74 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        FORMA 74 (ISLR)
            CONTROL DE PROCESO DECLARACION FORMA 74 (ISLR)
                fecha de proceso de retenciones islr (xml) por el asistente
                fecha revisado y declarado en el portal por el supervisor (xml)
                fecha de entrega al departamento de administracion

                MÓDULO DE SERVICIO BANCARIO (REGULAR) (COMPLEMENTARIA)
                    NUMERO DE LA PLANILLA
                    FECHA DE PAGO DE LA PLANILLA
                    ENTIDAD BANCARIA
                    MONTO CANCELADO

                MÓDULO DE CONTROL DE ENTREGA DE LOS DOCUMENTOS
                    Fecha de Entrega al Personal Externo
                    Fecha de Entrega de la Comunicación al Cliente
        */
        
        Schema::create('servicios_forma_74', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->date('recibido_admon')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Administración');
            $table->date('recibido_oper')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Operaciones');

            $table->date('proceso')->nullable()->comment('Fecha de proceso de retenciones islr (xml) por el asistente');
            $table->date('revision_declarado')->nullable()->comment('Fecha revisado y declarado en el portal por el supervisor (xml)');
            $table->date('entrega_admon')->nullable()->comment('Fecha de envio al departamento de Administración');

            $table->string('certificado', 1)->nullable()->comment('Habilita o Desabilita la informacion del banco');
            $table->string('regular_numero_planilla', 40)->nullable()->comment('Numero de la planilla regular');
            $table->date('regular_pago_planilla')->nullable()->comment('Fecha de pago de la planilla regular');
            $table->integer('regular_bancos_id')->nullable()->unsigned()->comment('Entidad bancaria regular');
            $table->decimal('regular_monto_cancelado', 14, 2)->nullable()->comment('Monto cancelado regular');

            $table->string('comp_numero_planilla', 40)->nullable()->comment('Complementario de la planilla regular');
            $table->date('comp_pago_planilla')->nullable()->comment('Complementario Fecha de pago de la planilla regular');
            $table->integer('comp_bancos_id')->nullable()->unsigned()->comment('Complementaria Entidad bancaria regular');
            $table->decimal('comp_monto_cancelado', 14, 2)->nullable()->comment('Complementario Monto cancelado regular');

            $table->date('entrega')->nullable()->comment('Fecha de Entrega al Personal Externo');
            $table->date('entrega_comunicacion')->nullable()->comment('Fecha de Entrega de la Comunicación al Cliente');

            $table->string('comentario', 200)->nullable()->comment('Comentario');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('regular_bancos_id')->references('id')->on('empresa_bancos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('comp_bancos_id')->references('id')->on('empresa_bancos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_forma_74');
    }
}
