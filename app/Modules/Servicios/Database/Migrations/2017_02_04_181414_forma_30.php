<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Forma30 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*
        FORMA 30 (IVA)
            fecha de proceso del libro de compras y ventas por el asistente
            fecha revision e impresión del libro de compras y ventas por el supervisor
            fecha de registro de la planilla forma 30 (iva) en el portal por el gerente
            fecha de entrega al departamento de administracion

            MÓDULO DE SERVICIO BANCARIO (REGULAR) (SUSTITUTIVA)
                NUMERO DE LA PLANILLA
                FECHA DE PAGO DE LA PLANILLA
                ENTIDAD BANCARIA
                MONTO CANCELADO

            MÓDULO DE CONTROL DE ENTREGA DE LOS DOCUMENTOS
                Fecha de Entrega al Personal Externo
                Fecha de Entrega de la Comunicación al Cliente
        */

        Schema::create('servicios_forma_30', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('clientes_historico_id')->unsigned();

            $table->date('recibido_admon')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Administración');
            $table->date('recibido_oper')->nullable()->comment('Fecha de Documentos Recibidos Departamento de Operaciones');

            $table->date('proceso')->nullable()->comment('Fecha de proceso del libro de compras y ventas por el asistente');
            $table->date('revision_declarado')->nullable()->comment('Fecha revision e impresión del libro de compras y ventas por el supervisor');
            $table->date('registro')->nullable()->comment('Fecha de registro de la planilla forma 30 (iva) en el portal por el gerente');
            $table->date('entrega_admon')->nullable()->comment('Fecha de envio al departamento de Administración');

            $table->string('certificado', 1)->nullable()->comment('Habilita o Desabilita la informacion del banco');

            $table->string('regular_numero_planilla', 40)->nullable()->comment('Regular Numero de la planilla');
            $table->date('regular_pago_planilla')->nullable()->comment('Regular Fecha de pago de la planilla');
            $table->integer('regular_bancos_id')->nullable()->unsigned()->comment('Regular Entidad bancaria');
            $table->decimal('regular_monto_cancelado', 14, 2)->nullable()->comment('Regular Monto cancelado');

            $table->string('susti_numero_planilla', 40)->nullable()->comment('Sustitutiva Numero de la planilla');
            $table->date('susti_pago_planilla')->nullable()->comment('Sustitutiva Fecha de pago de la planilla');
            $table->integer('susti_bancos_id')->nullable()->unsigned()->comment('Sustitutiva Entidad bancaria');
            $table->decimal('susti_monto_cancelado', 14, 2)->nullable()->comment('Sustitutiva Monto cancelado');

            $table->date('entrega')->nullable()->comment('Fecha de Entrega al Personal Externo');
            $table->date('entrega_comunicacion')->nullable()->comment('Fecha de Entrega de la Comunicación al Cliente');
            
            $table->string('comentario', 200)->nullable()->comment('Comentario');
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clientes_historico_id')->references('id')->on('clientes_historico')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('regular_bancos_id')->references('id')->on('empresa_bancos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('susti_bancos_id')->references('id')->on('empresa_bancos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_forma_30');
    }
}
