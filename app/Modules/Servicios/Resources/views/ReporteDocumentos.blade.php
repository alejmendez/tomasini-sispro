@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    <div id="botonera">
        <div class="msj-botonera bg-red bg-font-red text-left" style="display: none;"></div>
        <div class="btn-group btn-group-solid">
            <button id="limpiar" class="btn default tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.clean.title') }}">
                <i class="fa fa-file-o" aria-hidden="true"></i>
                <span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.clean.btn') }}</span>
            </button>

            <button id="guardar" class="btn blue tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.save.title') }}">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                <span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.save.btn') }}</span>
            </button>
        </div>
    </div>
    @include('base::partials.ubicacion', ['ubicacion' => ['Servicios', 'Trabajo']])
@endsection

@section('content')


<div class="row">
    {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <label class="requerido">Mes</label>
        {{ Form::select('mes', $controller->meses(), date('m'), [
            'id'          => 'mes',
            'class'       => 'form-control'
        ]) }} 
    </div>

    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <label class="requerido">A&ntilde;o</label>
        {{ Form::selectRange('ano', 2017, 2016, date('Y'), [
            'id'          => 'ano',
            'class'       => 'form-control'
        ]) }} 
    </div>
    {!! Form::close() !!}
</div>

<div class="portlet box blue portlet-lista-clientes">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-list"></i> Lista de clientes.
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="javascript:;" class="reload"> </a>
            <a href="javascript:;" class="fullscreen"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <table id="tabla" class="table table-striped table-hover table-bordered" style="width: 2000px;">
            <thead>
                <tr>
                    <th style="width: 200px;">Empresa</th>
                    <th style="width: 100px;">Entrega</th>
                    <th style="width: 100px;">Forma 74</th>
                    <th style="width: 100px;">Forma 30</th>
                    <th style="width: 100px;">Forma 35 1qc</th>
                    <th style="width: 100px;">Forma 35 2qc</th>
                    <th style="width: 100px;">Forma 21 1qc</th>
                    <th style="width: 100px;">Forma 21 2qc</th>
                    <th style="width: 100px;">Forma 18 1qc</th>
                    <th style="width: 100px;">Forma 18 2qc</th>
                    <th style="width: 100px;">Forma 22</th>
                    <th style="width: 100px;">Inventario</th>
                    <th style="width: 100px;">Libros</th>
                    <th style="width: 200px;">Libros Compras Ventas</th>
                    <th style="width: 100px;">Contabilidad</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection