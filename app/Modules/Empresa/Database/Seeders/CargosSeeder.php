<?php

namespace App\Modules\Empresa\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Modules\Empresa\Models\EmpresaCargos;

class CargosSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$cargos = [
			'Presidente',
			'Vicepresidente',
			'Gerente de Auditoria e Impuestos',
			'Administradora',
			'Gerente de Operaciones',
			'Coordinador de Cuentas Por Cobrar',
			'Asistente de Cobranzas',
			'Jefe de Mercadeo',
			'Supervisor de Contabilidad',
			'Contador I',
			'Contador II',
			'Contador III',
			'Analista de Sistemas',
			'Analista de Recursos Humanos I',
			'Asistente de Contabilidad I',
			'Asistente de Contabilidad II',
			'Asistente Administrativo I',
			'Asistente Administrativo II',
			'Asistente de Contribuyentes I',
			'Asistente de Contribuyentes II',
			'Personal Externo',
		];

		foreach ($cargos as $cargo) {
			EmpresaCargos::create([
				'nombre' => $cargo
			]);
		}
	}
}
