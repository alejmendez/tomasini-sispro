<?php

namespace App\Modules\Clientes\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use App\Modules\Clientes\Models\ClientesCategorias;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$categorias = [
    		'A',
    		'B',
            'C',
    		'D',
    	];

        DB::beginTransaction();
        try{
            foreach ($categorias as $categoria) {
                ClientesCategorias::create([
                    'nombre' => $categoria
                ]);
            }
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();
    }
}
