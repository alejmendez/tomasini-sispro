<?php

$menu['empresa'] = [
	[
		'nombre' 	=> 'Empresa',
		'direccion' => '#Empresa',
		'icono' 	=> 'fa fa-sitemap',
		'menu' 		=> [
			[
				'nombre' 	=> 'Bancos',
				'direccion' => 'empresa/bancos',
				'icono' 	=> 'fa fa-bank'
			],
			[
				'nombre' 	=> 'Cargos',
				'direccion' => 'empresa/cargos',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Empleados',
				'direccion' => 'empresa/empleados',
				'icono' 	=> 'fa fa-user-circle'
			],
			[
				'nombre' 	=> 'Estructura',
				'direccion' => 'empresa/estructura',
				'icono' 	=> 'fa fa-sitemap'
			]
		]
	]
];