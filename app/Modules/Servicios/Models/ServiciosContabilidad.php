<?php

namespace App\Modules\Servicios\Models;

use Carbon\Carbon;
use App\Modules\Base\Models\Modelo;

use App\Modules\Servicios\Models\ClientesHistorico;

class ServiciosContabilidad extends modelo
{
    protected $table = 'servicios_contabilidad';
    protected $fillable = [
        'clientes_historico_id',
        'proceso',
        'revision',
        'envio',
        'entrega',
        'entrega_comunicacion',
        'entrega_documentos',
        'comentario'
    ];
    public $campos = [
        'proceso' => [
            'type'        => 'date',
            'label'       => 'Proceso',
            'placeholder' => 'Proceso'
        ],
        'revision' => [
            'type'        => 'date',
            'label'       => 'Revisión',
            'placeholder' => 'Revisión'
        ],
        'entrega' => [
            'type'        => 'date',
            'label'       => 'Entrega al Personal Externo',
            'placeholder' => 'Entrega al Personal Externo',
            'title'       => 'Fecha de Entrega al Personal Externo'
        ],
        'entrega_comunicacion' => [
            'type'        => 'date',
            'label'       => 'Entrega de Comunicacion al Cliente',
            'placeholder' => 'Entrega de Comunicacion al Cliente',
            'title'       => 'Fecha de Entrega de la Comunicación al Cliente'
        ],
        'entrega_documentos' => [
            'type'        => 'select',
            'label'       => 'Entrega a',
            'placeholder' => '- Entrega a...',
            'options'     => [
                'c' => 'Cliente',
                'i' => 'Inventario',
            ]
        ],
        'comentario' => [
            'type'        => 'textarea',
            'label'       => 'Comentario',
            'placeholder' => 'Comentario',
            'cont_class'  => 'col-sm-12'
        ]
    ];

    public function setProcesoAttribute($value)
    {
        $this->attributes['proceso'] = $this->formatoFecha($value);
    }

    public function getProcesoAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }

    public function setRevisionAttribute($value)
    {
        $this->attributes['revision'] = $this->formatoFecha($value);
    }

    public function getRevisionAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }

    public function setEnvioAttribute($value)
    {
        $this->attributes['envio'] = $this->formatoFecha($value);
    }

    public function getEnvioAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }

    public function setEntregaAttribute($value)
    {
        $this->attributes['entrega'] = $this->formatoFecha($value);
    }

    public function getEntregaAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }

    public function setEntregaComunicacionAttribute($value)
    {
        $this->attributes['entrega_comunicacion'] = $this->formatoFecha($value);
    }

    public function getEntregaComunicacionAttribute($value)
    {
        return  Carbon::parse($value)->format('d/m/Y');
    }
}