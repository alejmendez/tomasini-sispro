<?php

namespace App\Modules\Empresa\Models;

use App\Modules\Base\Models\Modelo;

class EmpresaEstructura extends modelo
{
    protected $table = 'empresa_estructura';
    protected $fillable = ["nombre","empresa_estructura_id"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Empresa Estructura'
        ],
        'empresa_estructura_id' => [
            'type' => 'hidden'
        ]
    ];

    public function estructura()
    {
        return $this->hasOne('App\Modules\Empresa\Models\EmpresaEstructura', 'empresa_estructura_id');
    }

    public static function arbol($padre = 0)
    {
        $data = [];
        $elem = self::where('empresa_estructura_id', $padre)->get();

        foreach ($elem as $ele) {
            $dato = [
                'id'   => $ele->id,
                'text' => $ele->nombre,
                'icon' => 'fa fa-sitemap'
            ];

            if ($padre == 0) {
                $dato['state'] = [
                    'opened' => true
                ];
            }

            $hijos = self::arbol($ele->id);

            if (!empty($hijos)) {
                $dato['children'] = $hijos;
            }

            $data[] = $dato;
        }
        
        return $data;
    }
}