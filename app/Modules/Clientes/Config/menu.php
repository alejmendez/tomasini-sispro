<?php

$menu['clientes'] = [
	[
		'nombre' 	=> 'Clientes',
		'direccion' => '#Clientes',
		'icono' 	=> 'fa fa-building-o',
		'menu' 		=> [
			[
				'nombre' 	=> 'Clientes',
				'direccion' => 'clientes',
				'icono' 	=> 'fa fa-building-o'
			],
			[
				'nombre' 	=> 'Categorias',
				'direccion' => 'clientes/categorias',
				'icono' 	=> 'fa fa-list-ul'
			],
			[
				'nombre' 	=> 'Rutas',
				'direccion' => 'clientes/rutas',
				'icono' 	=> 'fa fa-map-o'
			]
		]
	]
];