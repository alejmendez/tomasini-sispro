@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Empresa', 'Empleados']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar EmpresaEmpleados.',
        'columnas' => [
    		'Cédula' => '10',
    		'Nombre' => '21',
    		'Apellido' => '21',
    		'Correo' => '23',
    		'Telefono Movil' => '15',
    		'Género' => '10'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $EmpresaEmpleados->generate() !!}
        {!! Form::close() !!}

        <div class="col-md-12">
            <h3>Ubicaci&oacute;n Administrativa</h3>
            <div id="arbol"></div>
        </div>
    </div>
@endsection